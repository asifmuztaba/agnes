<?php

include('../../../../../../../wp-load.php');

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

// STEP 1: Read POST data
// reading posted data from directly from $_POST causes serialization 
// issues with array data in POST
// reading raw POST data from input stream instead. 
$raw_post_data  = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost         = array();
foreach ($raw_post_array as $keyval) {
    $keyval             = explode('=', $keyval);
    if (count($keyval) == 2)
        $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if (function_exists('get_magic_quotes_gpc')) {
    $get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
    if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
        $value = urlencode(stripslashes($value));
    }
    else {
        $value = urlencode($value);
    }
    $req .= "&$key=$value";
}


// STEP 2: Post IPN data back to paypal to validate
if (get_option('paypal_mode') == "live") {
$ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
}
else{
$ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
}
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// In wamp like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
// of the certificate as shown below.
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if (!($res = curl_exec($ch))) {
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
curl_close($ch);


// STEP 3: Inspect IPN validation result and act accordingly

if (strcmp($res, "VERIFIED") == 0) {
    // check whether the payment_status is Completed
    // check that txn_id has not been previously processed
    // check that receiver_email is your Primary PayPal email
    // check that payment_amount/payment_currency are correct
    // process payment
    // assign posted variables to local variables
    $item_number      = $_POST['item_number'];
    $payment_status   = $_POST['payment_status'];
    $payment_amount   = $_POST['mc_gross'];
    $payment_currency = $_POST['mc_currency'];
    $txn_id           = $_POST['txn_id'];
    $receiver_email   = $_POST['receiver_email'];
    $payer_email      = $_POST['payer_email'];
    $payer_id         = $_POST['payer_id'];
    $amt              = $_POST['amt'];

$myfile = fopen("before.txt", "w") or die("Unable to open file!");
fwrite($myfile, $_POST);
fclose($myfile);

    if ($_POST['payment_status'] == 'Completed') {
        
        

        $returnatt = serialize($_POST);

        $book_id  = $_POST['custom'];
        

    // Update Booking Post to Publish
      $my_post = array();
      $my_post['ID'] = $book_id;
      $my_post['post_status'] = 'publish';

    // Update the post into the database
       wp_update_post( $my_post );

        $author_id = get_post_field ('post_author', $book_id);
        if(!$author_id){
            $author_id = 1;
        }

        $f_name = get_field("book_first_name",$book_id);
        $l_name = get_field("book_last_name",$book_id);
        $order_title = $f_name ." ".$l_name. " ( Book )";

        $my_post = array(
            'post_title'   => $order_title,
            'post_status'  => 'publish',
            'post_author'  => $author_id,
            'post_type'    => 'payment',
            'post_content' => $returnatt
        );

        // Insert the post into the database
        $insert_post = wp_insert_post($my_post);
        
        
        $url = admin_url( 'post.php?post='.$insert_post.'&action=edit');
        
        //$url = get_edit_post_link( $insert_post );
        
        update_post_meta($book_id, 'payment_status', $url);
		       
        
        update_post_meta($insert_post, 'payment_for', "Book Request");

        update_post_meta($insert_post, 'payment_date', $_POST['payment_date']);
        update_post_meta($insert_post, 'mc_currency', $_POST['mc_currency']);
        update_post_meta($insert_post, 'payment_status', $_POST['payment_status']);

        update_post_meta($insert_post, 'mc_fee', $_POST['mc_fee']);
        update_post_meta($insert_post, 'business', $_POST['business']);
        update_post_meta($insert_post, 'verify_sign', $_POST['verify_sign']);
        update_post_meta($insert_post, 'receiver_email', $_POST['receiver_email']);
        update_post_meta($insert_post, 'payment_fee', $_POST['payment_fee']);
        update_post_meta($insert_post, 'receiver_id', $_POST['receiver_id']);
        
        update_post_meta($insert_post, 'payment_given', $_POST['payment_gross']);
        update_post_meta($insert_post, 'payment_gross', $pieces['package_amount']);
        
        update_post_meta($insert_post, 'address_zip', $_POST['address_zip']);
        update_post_meta($insert_post, 'address_street', $_POST['address_street']);
        update_post_meta($insert_post, 'address_country_code', $_POST['address_country_code']);
        update_post_meta($insert_post, 'address_country', $_POST['address_country']);
        update_post_meta($insert_post, 'address_city', $_POST['address_city']);
        update_post_meta($insert_post, 'address_state', $_POST['address_state']);

//payer detail
        update_post_meta($insert_post, 'payer_id', $_POST['payer_id']);
        update_post_meta($insert_post, 'payer_email', $_POST['payer_email']);
        update_post_meta($insert_post, 'first_name', $_POST['first_name']);
        update_post_meta($insert_post, 'last_name', $_POST['last_name']);
        
        $usr_email = get_field("book_user_email",$book_id);
        
        $mail_content_top = get_option('mymail_buybook_header');
        $mail_content_footer = get_option('mymail_buybook_footer');
        
        /*start of send user payment notification*/
        $from    = get_option( 'admin_email' );
        $to      = $usr_email;
        $subject = "Book Payment";
        $maildata='
                            <table style="border-collapse: collapse; width: 100%;font-size: 14px; line-height: 18px; color:#535353; margin:0; padding:0; text-align: left;background:#EFEFEF;">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:90%; float:left; margin:5%">
                                    <tr>
                                        <td style="font-size:16px; font-weight:bold;">Hello '.$f_name.',</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px 0;" width="150px;"> '.$mail_content_top.'</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width:100%; margin:10px 0; font-size:14px; border-collapse:collapse;" cellpadding="0" cellspacing="0">		
                                                <tr>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">Name</td>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">'.$f_name.' '.$l_name.'</td>
                                                </tr>	
                                                <tr>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">Payment Given</td>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">'.$_POST['payment_gross'].'</td>
                                                </tr>					
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px 0; width:150px;">'.$mail_content_footer.'</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                        </tr>
                    </table>';

        send_my_mail($from, $to, $subject, $maildata);
        /*End of sending user payment notification*/
        	
  
		//start of send Admin payment notification
        $from    = get_option( 'admin_email' );
        $to      = get_option( 'admin_email' );
        $subject = "Booking Payment Info";
        $message = '
                            <table style="border-collapse: collapse; width: 100%;font-size: 14px; line-height: 18px; color:#535353; margin:0; padding:0; text-align: left;background:#EFEFEF;">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:90%; float:left; margin:5%">
                                    <tr>
                                        <td style="font-size:16px; font-weight:bold;">Hello Admin,</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px 0;" width="150px;"> New Payment has been done regarding buying the Book. The details are as below :</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table style="width:100%; margin:10px 0; font-size:14px; border-collapse:collapse;" cellpadding="0" cellspacing="0">		
                                                <tr>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">Name</td>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">'.$f_name.' '.$l_name.'</td>
                                                </tr>	
                                                <tr>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">Email</td>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">'.$usr_email.'</td>
                                                </tr>	
                                                <tr>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">Payment Given</td>
                                                    <td style="border:1px solid #6C6C6C; padding:5px;">'.$_POST['payment_gross'].'</td>
                                                </tr>					
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px 0;" width="150px;"> For more details <a href="'.$url.'">Click Here</a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                        </tr>
                    </table>';

        send_my_mail($from, $to, $subject, $message);
		

/* end of Booking update process */
		
    }
}
else if (strcmp($res, "INVALID") == 0) {
    // log for manual investigation
}
?>
