<?php
/*
  Template Name: Permission To Prosper - Thank you
 */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Money Mindset Mentor & Wealth Therapist">
		<meta name="author" content="Agnes Kowalski">
		<meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<title>Thank You</title>
		<!-- Bootstrap core CSS -->
		<link href="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/thankyou-css/bootstrap.min.css" rel="stylesheet">
		<link href="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/thankyou-css/style.css" rel="stylesheet">
		<script src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/thankyou-js/jquery-1.12.4.min.js"></script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '242423366230929');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=242423366230929&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
	</head>
	<body>
		<div class="main_div">
			<!-- thank you start -->
			<section>
				<div class="seaction5">
					<div class="sec5 sec6 sec7">
						<div class="container">
							<div class="top-buffer3"></div>
							<div class="top-buffer1"></div>
							<div class="ruls">
								<div class="rule1 thankyou">
									<p>You did it. You gave yourself <span class="Permission">Permission to Prosper.</span></p>
									<div class="yourself5">
										<h2 class="yourself">Hug yourself,</h2>
										<h2 class="yourself">this is HUGE!</h2>
									</div>
								</div>
								<div class="top-buffer3"></div>
								<div class="rule1 thankyou thankyou5">
									<h5 class="process">The process of co-creation starts <span>NOW.</span></h5>
									<!--<div class="top-buffer1"></div>-->
									<!--<p>An email is going to be sent to you</p>
									<p>shortly with details of your purchase.</p>-->
									<p style="font-size: 22px !important;">
										YOU WILL RECEIVE YOUR LOGIN WITHIN 2-12HRS DEPENDING ON HOW FAST PAYPAL PROCESSES, WHATEVER EMAIL IS ASSOCIATED WITH</p>
									<p style="font-size: 22px !important;">                                          YOUR PAYPAL ACCOUNT IS THE EMAIL YOUR LOGIN WILL BE SENT TO. FOR ANY ISSUES CONTACT <a style="color: #c8485f;" href="mailto:support@agneskowalski.com" target="_top">SUPPORT@AGNESKOWALSKI.COM</a>
									</p>
									<div class="top-buffer3"></div>
									<h3>Thank you </h3>
									<h4>Aggie K</h4>
									<h2>Agnes Kowalski</h2>
								</div>
								<div class="top-buffer3"></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- thank you end -->
			<footer>
				<div class="container">
					<div class="footer">
						<p>© 2018 ALL RIGHTS RESERVED</p>
					</div>
				</div>
			</footer>
		</div>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106731453-1');
</script>		
	</body>
</html>