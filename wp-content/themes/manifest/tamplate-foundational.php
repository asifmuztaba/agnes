<?php
/*
  Template Name: Foundational 
 */

if (!session_id()) {
    session_start();
}

if (is_user_logged_in()) {
    ?>
    <?php
    get_header();
    ?>
<style type="text/css">
   .pb-4, .py-4 {
   padding-bottom: 1.5rem;
   }
   .pt-4, .py-4 {
   padding-top: 1.5rem;
   }
   .mt-4, .my-4 {
   margin-top: 1.5rem;
   }
   .main-content > div {
   padding: 20px 0 50px;
   font-family: Roboto-Regular;
   }
   .down-arrow {
   padding: 0 0 25px;
   text-align: center;
   }
   .down-arrow .fa.fa-arrow-down {
   font-size: 52px;
   color: #67a9a5;
   }
   .pdf {
   display: inline-block;
   width: 100%;    
   }
   .pdf a {
   background-color: rgb(247, 247, 247);
   border: 1px solid rgb(237, 237, 237);
   display: table-cell;
   height: 170px;
   margin: 0 auto;
   padding: 15px;  
   position: relative;
   vertical-align: middle;
   width: 1000px;
   }
   .pdf .pdf-name {
   text-align: center;
   color: #67a9a5;
   font-family: Roboto-Regular;
   }
   .pdf img {
   position: absolute;
   bottom: 10px;
   right: 10px;
   }
    .vedio {
      height: 100%;
      max-height: 200px;
      overflow: hidden;
      position: relative;
    }
   .vedio img {
   bottom: auto;
   height: 100%;
   position: inherit;
   right: auto;
   width: 100%;
   }
   .vedio .icon {
   height: 40px;
   left: 0;
   margin: 0 auto;
   position: absolute;
   right: 0;
   top: 50%;
   transform: translateY(-50%);
   width: 60px;
   }
   .continue, .preview, .next-sect {
   background-color: rgb(200, 72, 95) !important;
   border: 0 none;
   border-radius: 0;
   letter-spacing: 1px;
   padding: 10px 40px;
   text-transform: uppercase;
   transition: 0.5s;
   color: #fff !important;
   }
   .continue:hover, .next-sect:hover, .preview {
   background-color: #000 !important;
   }
   .preview:hover {
   background-color: rgb(200, 72, 95) !important;
   }
   .disabled, :disabled {
   background-color: rgba(0,0,0,0.3) !important;
   color: rgba(255,255,255) !important;
   }
   .homework {
      font-size: 20px;
      color: rgb(103, 169, 165);
      letter-spacing: 1px;
      padding-bottom: 10px;
      text-align: center;
      font-family: Roboto-Regular;
    }
   .full-w {
       display: inline-block;
       width: 100%;
       padding: 12px 0;
       }
    .titlev {
      color: rgb(103, 169, 165);
      font-family: Roboto-Regular;
      font-size: 20px;
      height: 70px;
      padding-top: 10px;
      text-align: center;
    }
    .btn.continue, .btn.next-sect {
      float: right;
    }
</style>
    <?php if (have_posts()) : ?>  
        <?php while (have_posts()) : the_post(); ?>

            <!---login---->
            <section class="login-page">
                <div class="top-buffer6"></div>
                <div class="container">
                    <div class="dashboard">
                        <div class="title sec4">
                            <h3 class="text-center"><?php the_title(); ?></h3>
                            <a class="back_btn_dash" href="<?php echo get_permalink(get_option('dash_page_id')); ?>"> &#60;&#60; Back To Dashboard </a>
                        </div>
                        <div class="top-buffer1"></div>

                        <?php                        
                        $current_user = wp_get_current_user();
						$lesson_status = $current_user->lesson_status;
	
                        $subscribed_program = get_field('subscribed_program', 'user_' . $current_user->ID);
                        
                        if (!empty($subscribed_program)) {
                            ?>
                            <?php
                            for($j=0;$j<count($subscribed_program);$j++){
                                $post_id = $subscribed_program[$j]["subscribed_program"]->ID;
                                $program_video_rep = get_field("program_video_rep", $post_id);
                                $pro_pdf_rep = get_field("pro_pdf_rep",$post_id);
                                $pdfs_transcripts = get_field("pdfs_transcripts",$post_id);
                                ?>
                                <?php
                                /*
                                  echo "<pre>";
                                  print_r($program_video_rep);
                                  echo "</pre>";
                                 */
                                ?>
<!-- 
                                <div class="program1 videos">
                                    <h4><?php echo $subscribed_program[$j]["subscribed_program"]->post_title; ?></h4>
                                    <?php foreach ($program_video_rep as $program_video) { ?>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="dash-box">
                                                <a class="video"  title="<?php echo $program_video["pro_vide_title"]; ?>" href="<?php echo $program_video["pro_vid_video"]; ?>"><img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png"/><img src="<?php echo $program_video["pro_vid_img"]; ?>"/></a>
                                            </div>

                                            <p class="text-center"><?php echo $program_video["pro_vide_title"]; ?></p>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="display-buffer"></div> -->
                                         <section class="main-content">
                                            <div id="vedioRow_1" class="row py-4 vedioRow">
                                               <div class="col-xs-12">
                                                  <h2>Welcome</h2>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[0]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[0]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[0]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[0]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[0]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[0]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[0]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[0]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[1]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[1]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[1]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[1]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[1]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[1]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[1]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[1]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                               </div>
                                               <div class="col-xs-6 mt-4">
                                                  <button id="preview_1" disabled="" class="btn preview" style="opacity: 0;">Back</button>
                                               </div>
                                               <div  class="col-xs-6 mt-4 text-right">

                                                  <?php //if ('lessonstaus =< 2') { ?>

                                                     <button id="nextsect_1" class="btn next-sect">Next</button>

                                                  <?php //}else{ ?>

                                                     <button id="continue_1" data-lession_id="1" class="btn continue">Complete</button>
                                                    
                                                  <?php //} ?>

                                               </div>
                                            </div>
                                            <div id="vedioRow_2" class="row py-4 vedioRow">
                                               <div class="col-xs-12">
                                                  <h2>Week 1</h2>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[2]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[2]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[2]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[2]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[2]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[2]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[2]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[2]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[3]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[3]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[3]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[3]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[3]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[3]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[3]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[3]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[4]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[4]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[4]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[4]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[4]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[4]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[4]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[4]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[39]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[39]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[39]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[39]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="col-xs-6 mt-4">
                                                  <button id="preview_2" class="btn preview">Back</button>
                                               </div>
                                               <div class="col-xs-6 mt-4 text-right">

                                                  <?php //if ('lessonstaus =< 2') { ?>

                                                     <button id="nextsect_2" class="btn next-sect">Next</button>

                                                  <?php //}else{ ?>

                                                     <button id="continue_2" data-lession_id="2" class="btn continue">Complete</button>
                                                    
                                                  <?php //} ?>
                                                  
                                               </div>
                                            </div>
                                            <div id="vedioRow_3" class="row py-4 vedioRow">
                                               <div class="col-xs-12">
                                                  <h2>Week 2</h2>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[5]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[5]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[5]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[5]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[5]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[5]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[5]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[5]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[6]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[6]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[6]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[6]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[6]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[6]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[6]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[6]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[7]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[7]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[7]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[7]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[7]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[7]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[7]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[7]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                               </div>
                                               <div class="col-xs-6 mt-4">
                                                  <button id="preview_3" class="btn preview">Back</button>
                                               </div>
                                               <div class="col-xs-6 mt-4 text-right">

                                                  <?php //if ('lessonstaus =< 2') { ?>

                                                     <button id="nextsect_3" class="btn next-sect">Next</button>

                                                  <?php //}else{ ?>

                                                     <button id="continue_3" data-lession_id="3" class="btn continue">Complete</button>
                                                    
                                                  <?php //} ?>
                                                  
                                               </div>
                                            </div>
                                            <div id="vedioRow_4" class="row py-4 vedioRow">
                                               <div class="col-xs-12">
                                                  <h2>Week 3</h2>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[8]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[8]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[8]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[8]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[8]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[8]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[8]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[8]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[9]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[9]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[9]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[9]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[9]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[9]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[9]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[9]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[10]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[10]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[10]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[10]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[10]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[10]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[10]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[10]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                               </div>
                                               <div class="col-xs-6 mt-4">
                                                  <button id="preview_4" class="btn preview">Back</button>
                                               </div>
                                               <div class="col-xs-6 mt-4 text-right">

                                                  <?php //if ('lessonstaus =< 2') { ?>

                                                     <button id="nextsect_4" class="btn next-sect">Next</button>

                                                  <?php //}else{ ?>

                                                     <button id="continue_4" data-lession_id="4" class="btn continue">Complete</button>
                                                    
                                                  <?php //} ?>
                                                  
                                               </div>
                                            </div>
                                            <div id="vedioRow_5" class="row py-4 vedioRow">
                                               <div class="col-xs-12">
                                                  <h2>Week 4</h2>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[11]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[11]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[11]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[11]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[11]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[11]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[11]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[11]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[12]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[12]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[12]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[12]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-6 mt-4">
                                                     <button id="preview_5" class="btn preview">Back</button>
                                                  </div>
                                                  <div class="col-xs-6 mt-4 text-right">

                                                  <?php //if ('lessonstaus =< 2') { ?>

                                                     <button id="nextsect_5" class="btn next-sect">Next</button>

                                                  <?php //}else{ ?>

                                                     <button id="continue_5" data-lession_id="5" class="btn continue">Complete</button>
                                                    
                                                  <?php //} ?>
                                                  
                                                  </div>
                                               </div>
                                            </div>
                                            <div id="vedioRow_6" class="row py-4 vedioRow">
                                               <div class="col-xs-12">
                                                  <h2>ALUMNI SECTION</h2>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[13]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[13]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[13]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[13]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[13]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[13]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[12]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[12]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[14]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[14]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[14]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[14]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[14]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[14]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[13]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[13]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[15]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[15]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[15]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[15]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[15]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[15]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[14]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[14]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[16]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[16]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[16]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[16]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[16]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[16]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[15]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[15]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[17]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[17]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[17]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[17]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[17]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[17]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[16]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[16]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[18]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[18]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[18]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[18]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[18]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[18]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[17]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[17]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[19]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[19]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[19]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[19]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[19]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[19]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[18]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[18]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[19]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[19]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[20]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[20]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[20]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[20]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[20]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[20]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[20]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[20]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[21]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[21]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[21]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[21]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[21]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[21]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[21]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[21]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[22]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[22]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[22]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[22]['pro_vide_title']); ?></div>
                                                     <div class="pdf" style="margin-bottom: 10px;">
                                                        <a target="_blank" href="<?php print_r($pdfs_transcripts[22]['transcripts_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pdfs_transcripts[22]['transcripts_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                     <div class="down-arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                                                     <div class="homework">HOMEWORK</div>
                                                     <div class="pdf">
                                                        <a target="_blank" href="<?php print_r($pro_pdf_rep[22]['pro_pdf_dwnld']); ?>" title="Click to Download PDF" >
                                                           <h3 class="pdf-name"><?php print_r($pro_pdf_rep[22]['pro_pdf_title']); ?></h3>
                                                           <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive">
                                                           <!-- <u>Download Now</u> -->
                                                        </a>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[23]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[23]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[23]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[23]['pro_vide_title']); ?></div>                                 
                                                 </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[24]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[24]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[24]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[24]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[25]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[25]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[25]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[25]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[26]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[26]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[26]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[26]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[27]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[27]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[27]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[27]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[28]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[28]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[28]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[28]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[29]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[29]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[29]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[29]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[30]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[30]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[30]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[30]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[31]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[31]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[31]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[31]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[32]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[32]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[32]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[32]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[33]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[33]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[33]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[33]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[34]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[34]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[34]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[34]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[35]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[35]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[35]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[35]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[36]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[36]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[36]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[36]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[37]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[37]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[37]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[37]['pro_vide_title']); ?></div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-4">
                                                     <div class="vedio">
                                                        <a class="video" title="<?php print_r($program_video_rep[38]['pro_vide_title']); ?>" href="<?php print_r($program_video_rep[38]['pro_vid_video']); ?>">
                                                        <img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png">
                                                        <img src="<?php print_r($program_video_rep[38]['pro_vid_img']); ?>"></a>
                                                     </div>
                                                     <div class="titlev"><?php print_r($program_video_rep[38]['pro_vide_title']); ?></div>
                                                  </div>
                                               </div>
                                               <div class="full-w">
                                                  <div class="col-xs-6 mt-4">
                                                     <button id="preview_6" class="btn preview">Back</button>
                                                  </div>
                                                  <div class="col-xs-6 mt-4 text-right">

                                                  <?php //if ('lessonstaus =< 2') { ?>

                                                     <button id="nextsect_6" class="btn next-sect">Next</button>

                                                  <?php //}else{ ?>

                                                     <button id="continue_6" data-lession_id="6" class="btn continue">Finish</button>
                                                    
                                                  <?php //} ?>
                                                  
                                                  </div>
                                               </div>
                                            </div>
                                            <div id="vedioRow_7" class="row py-4 vedioRow">
                                               <div class="col-xs-12">
                                                  <h2>PTP 2.0 Section</h2>
                                               </div>
                                                <div class="col-xs-12">Coming Soon...</div>    
                                                <div class="full-w">
                                                  <div class="col-xs-6 mt-4">
                                                     <button id="preview_7" class="btn preview">Back</button>
                                                  </div>
                                               </div>
                                            </div>
                                         </section>
                                         <!-- main-content -->

                            <?php } ?>
                        <?php }else{ ?>
                                <p>No subscription is done.</p>
                        <?php }?>
                    </div>
                </div>
                <div class="top-buffer100"></div>
            </section>
            <!--- end Login--->
        <?php endwhile; ?>
    <?php else: ?> 
        <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
    <?php endif; ?>

    <?php
    get_footer();
}
else {
    wp_safe_redirect(get_permalink(get_option('login_page_id'))); /* Redirect To Login page */
    die();
}?>


<script type="text/javascript">
   $(document).ready(function(){
   
     $('.vedioRow').css('display','none');
     $('.next-sect').css('display','none');
   
     $('.preview').on('click',function(){
         var id = $(this).attr('id');
         var res = id.split("_");
         var rowNumber = res[1];
         $('#vedioRow_'+rowNumber).css('display','none');
   
         var previwNumber = parseInt(res[1]) - 1;
         $('#vedioRow_'+previwNumber).css('display','block');
       });
   
     $('.next-sect').on('click',function(){
       var id = $(this).attr('id');
         var res = id.split("_");
         var rowNumber = res[1];
         $('#vedioRow_'+rowNumber).css('display','none');
   
         var nextNumber = parseInt(res[1]) + 1;
         $('#vedioRow_'+nextNumber).css('display','block');
       }); 

 



       var videoId = <?=$lesson_status; ?>;
       var displayNumber = (videoId)+1;
       $('#vedioRow_'+displayNumber).css('display','block');

		while(videoId > 0){

			$('#continue_'+videoId).css('display','block');
			$('#continue_'+videoId).css('display','none');
			$('#nextsect_'+videoId).css('display','none');
			$('#nextsect_'+videoId).css('display','block');
			--videoId;
		}


 
   });
     
</script>
