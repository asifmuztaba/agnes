<?php
/*
 * Template Name: Money Mindset Guide Page
 */
get_header();
?>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
<div class="top-buffer3"></div>
<div class="top-buffer1"></div>
<div class="container money-mindset-class">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-sm-9">
                    <div class="section1">
                        <div class="sec1-body">
                            <div class="sec-dis">
                                <div class="buffer-top2"></div>
                                <div class="buffer-top2"></div>
                                <p class="s16 white titles"><?php echo get_field('section_title'); ?></p>
                                <div class="buffer-top1"></div>
                                <p class="s16 white"><?php echo get_field('section_sub_title'); ?></p>
                                <div class="buffer-top05"></div>
                                <div class="buffer-top1"></div>
                                <div class="contents">
                                    <div class="buffer-top05" style="display:inline-block;"></div>
                                    <?php
                                        $substrings = get_field('section_strings');
                                    ?>
                                    <p class="white mo-bold"><b><i><?php echo $substrings[0]['string_text']; ?></i></b></p>
                                    <h2 class="s30 orange"><i><?php echo $substrings[1]['string_text']; ?></i></h2>
                                    <p class="s14 white mo-light"><i><?php echo $substrings[2]['string_text']; ?></i></p>
                                    <div class="buffer-top05"></div>
                                    <h2 class="s30 orange"><i><?php echo $substrings[3]['string_text']; ?></i></h2>
                                    <h2 class="s30 orange"><i><?php echo $substrings[4]['string_text']; ?></i></h2>
                                    <div class="sign">
                                        <img src="<?php echo get_field('sign_image'); ?>" alt="<?php echo basename(get_field('sign_image')); ?>" />
                                    </div>
                                    <div class="buffer-top05" style="display:inline-block;"></div>
                                </div>
                            </div>
                            <div class="back-img">
                               <img src="<?php echo get_field('back_image'); ?>" alt="<?php echo basename(get_field('back_image')); ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="top-buffer2"></div>
            <p class="s35"><?php the_title(); ?></p>
            <div class="top-buffer2"></div>

            <div class="b22 metasinglemeta">
                <span class="s22 a22">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    &nbsp; <?php the_time(get_option('date_format')); ?> I 
                </span>
            </div>
            <div class="b23 metasinglemeta">
                <span class="a22">
                    <i class="fa fa-comment" aria-hidden="true"></i>
                    &nbsp; <a href="<?php the_permalink(); ?>#comment" class="go_to_comment">LEAVE A COMMENT</a> I 
                </span>
            </div>
            <div class="b24">
                <span class="a22">

                    SHARE ON &nbsp; 
                    <a class="fb-xfbml-parse-ignore" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>&amp;src=sdkpreparse" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" /></a>
                    <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>&amp;tw_p=tweetbutton&amp;url=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a>
                    <a href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $pimage[0]; ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/pinterest.png" /></a>
                    <a href="mailto:?&subject=<?php echo urlencode(get_the_title()); ?>&body=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/email.png" /></a>

                    </span>
                </div>

                    <div class="section2">
                      <div class="buffer-top2"></div>
                          <div class="buffer-top05"></div>
                          <p class="s15 mo-light"><?php echo get_field('content_firstline'); ?></p>
                           <div class="buffer-top2"></div>
                      <p class="s18 popins-light"><i><?php echo get_field('content_secondline'); ?></i></p>
                           <div class="buffer-top1"></div>
                          <div class="topic">
                                <?php echo get_field('content_points'); ?>
                          </div>
                          <div class="buffer-top2"></div>
                          <div class="buffer-top1"></div>
                          <p class="s18 mo-light"><?php echo get_field('content_end_first_line'); ?></p>
                          <div class="buffer-top05"></div>
                          <p class="s18 popins-bold"><i><?php echo get_field('content_end_second_line'); ?></i></p>
                          <div class="buffer-top2"></div>
                          <div class="buffer-top1"></div>
                          <h2><i><?php echo get_field('middle_second_section_title'); ?></i></h2>
                          <p class="s18 popins-reg space"><?php echo get_field('middle_second_section_sub_title'); ?></p>
                          <div class="buffer-top2"></div>
                          <div class="pera">
                                <?php echo get_field('middle_second_section_content'); ?>
                          </div>
                          <div class="buffer-top2"></div>
                          <div class="buffer-top2"></div>
                          <p class="s20 mo-bols"><?php echo get_field('middle_second_section_string1'); ?></p>
                          <div class="buffer-top1"></div><div class="buffer-top05"></div>
                          <p class="s18 popins-bold upper"><i><?php echo get_field('middle_second_section_string2'); ?></i></p>
                          <div class="buffer-top2"></div>
                          <div class="pera">
                              <?php echo get_field('middle_second_section_content2'); ?>
                          </div>
                           <div class="buffer-top2"></div>
                          <p class="s19 Popins-light text-center"><?php echo get_field('middle_second_section_string3'); ?></p>
                          <p class="s25 popins-bold text-center"><?php echo get_field('middle_second_section_string4'); ?></p>
                          <h2 class="text-center"><?php echo get_field('middle_section_third_title'); ?></h2>
                           <div class="buffer-top1"></div>
                           <p class="s13 text-center mo-light"><?php echo get_field('middle_section_third_sub_title'); ?></p>
                            <div class="buffer-top2"></div>
                          <div class="pera">
                                  <?php echo get_field('middle_section_third_content'); ?>
                          </div>
                           <div class="buffer-top2"></div> <div class="buffer-top1"></div>
                          <div class="pera">
                             <?php echo get_field('middle_question_1'); ?> 
                          </div>
                          <div class="buffer-top5"></div>
                          <div class="pera">
                              <?php echo get_field('middle_question_2'); ?> 
                          </div>
                          <div class="buffer-top5"></div>
                          <div class="pera">
                             <?php echo get_field('middle_question_3'); ?> 
                          </div>
                          <div class="buffer-top5"></div>
                          <div class="pera">
                              <?php echo get_field('middle_question_4'); ?> 
                          </div>
                             <div class="buffer-top2"></div>
                             <div class="buffer-top2"></div>
                          <div class="notes text-center upper">
                               <p class="mo-light"><?php echo get_field('middle_question_string_1'); ?></p>
                                   <p class="s18s mo-bols"><?php echo get_field('middle_question_string_2'); ?></p>
                          </div>
                               <p class="s14 mo-light text-center"><?php echo get_field('middle_question_string_3'); ?></p> 
                                   <div class="buffer-top1"></div>
                              <p class="s18 mo-light upper text-center"><?php echo get_field('middle_question_string_4'); ?></p>

                    </div>
                    <div class="buffer-top2"></div>
                    <div class="buffer-top2"></div>
                    <div class="section3">
                        <div class="post">
                             <div class="buffer-top5" style="display:inline-block;"></div>
                             <div class="buffer-top2"></div>
                                 <div class="box">
                                    <h4 class="s18 popins-bold"><i><?php echo get_field('bottom_section_title'); ?></i></h4>
                                    <p class="Popins-light s15"><?php echo get_field('bottom_section_content1'); ?></p>
                                    <p class="Popins-light s15"><?php echo get_field('bottom_section_content2'); ?></p>
                                 </div>
                             <div class="buffer-top5"></div>
                             <div class="buffer-top2 buffer"></div>
                        </div>							
                    </div>
                             
                  <?php /*  <div class="button5">
                        $pagelist = get_posts('sort_column=menu_order&sort_order=asc');
                        $pages = array();
                        foreach ($pagelist as $page) {
                            $pages[] += $page->ID;
                        }
                        $current = array_search(get_the_ID(), $pages);
                        $prevID = $pages[$current - 1];
                        $nextID = $pages[$current + 1];
                        ?>
                        <?php if (!empty($prevID)) { ?>
                        <a href="<?php echo get_permalink($prevID); ?>" title="<?php echo get_the_title($prevID); ?>"><input value="&lt; Prev" type="button" /></a>
                        <?php } else { ?>
                        <?php } ?>
                        <?php if (!empty($nextID)) { ?>
                        <a href="<?php echo get_permalink($nextID); ?>" title="<?php echo get_the_title($nextID); ?>"><input value="Next &gt;" class="button6" type="button" /></a>
                        <?php } else { ?>
                        <?php } ?>
                        <!-- Single Post Navigation Ends -->
                    </div>  */ ?>

                    <div class="top-buffer3"></div>
                    <div class="top-buffer2"></div>


                    <?php
                    if (comments_open() || get_comments_number()) {
                        comments_template();
                    }
                    ?>

                    <div class="top-buffer3"></div>
                    <div class="top-buffer2"></div>
                    <p class="s35 metasinglemeta">You may also like to read</p>
                    <div class="top-buffer2"></div>
                    <div class="row">

                        <div class="row p-right">
                            <?php
                            $count = 0;
                            $args = array(
                                'post_type' => 'post',
                                'orderby' => 'rand',
                                'post_status' => 'publish',
                                'category_name' => $catname,
                                'post__not_in' => array($post->ID),
                                'showposts' => 3,
                                );
                            $posts = query_posts($args);
                            if (have_posts()) : while (have_posts()) : the_post();
                            $pimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 responsive2">
                                <a href="<?php the_permalink(); ?>">
                                    <?php if ($pimage[0] != "") { ?><img src="<?php echo $pimage[0]; ?>" class="img-responsive" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php } ?>
                                    <div class="top-buffer"></div>
                                    <p class="s35 a36"><?php the_title(); ?></p>
                                </a>
                            </div>
                            <?php
                            $count++;
                            endwhile;
                            endif;
                            wp_reset_query();
                            ?>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 textsatting">
                    <?php if (is_active_sidebar('blog_sidebar')) { ?>
                    <?php dynamic_sidebar('blog_sidebar'); ?>
                    <?php } ?>
                </div>
            </div>                                
        </div>
    </div>
</div>
	<?php endwhile; ?>
<?php endif; ?>

<div class="top-buffer3"></div>
<div class="top-buffer2"></div>
<?php get_footer(); ?>