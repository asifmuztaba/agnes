<?php
/*
  Template Name: Subconsicous Smackdown
 */
?>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Subconsicous Smackdown</title>
      <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/css/bootstrap.css">
      <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/fonts/stylesheet.css">
      <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/css/font-awesome.css">
      <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/css/style.css">
   </head>
   <body>
      <div class="main">
      <div class="banner py-3">
         <div class="continer">
            <div class="subcon">
               <img class="py-4 px-2 banner-image" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/logo_icon.png" alt="logo" >
            </div>
            <div class="banner-text p-3">
               <span class="header-text pb-1 d-inline-block w-100">
                  <p>HEY <span class="mindset">MINDSET </span> NERD!</p>
               </span>
               <span class="header-text1 pb-3 d-inline-block w-100">
                  <p>(and if you aren’t a mindset nerd yet you will be by the time you finish this training)</p>
               </span>
               <span class="header-text2 pb-3 d-inline-block w-100">
                  <p> Whatever you think you know about mindset, has only just scratched the surface</p>
               </span>
               <span class="header-text3 pb-2 d-inline-block w-100">
                  <p>Most people try to<br>
                     <span class="journal"> journal/meditate/do gratitude lists/affirmations</span>
                  </p>
               </span>
            </div>
         </div>
      </div>
      <div class="main-body">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="body-part text-center">
                     <div class="body-text">
                        <span class="img-title">
                           <p>99% of the time, these do NOT create financial shifts</p>
                        </span>
                        <span class="sub-img-title pb-3 mt-3">
                           <p>(I wish they did, we’d all be taking champagne bubble baths)</p>
                        </span>
                     </div>
                     <div class="body-iamge">
                        <img class="lady-img" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/img-a.jpg" alt="lady-image">
                        <div class="bord-a">
                           <div class="arrow-text text-center">
                              <p>Isn't this what all solopreneur moms of two kids look like every night? LOL</p>
                           </div>
                           <div class="arrow">
                              <img class="arrow-content" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/arrow.png" alt="">
                              <img class="arrow-content d-none" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/responsiv.png" alt="">
                           </div>
                        </div>
                        <div class="bord position-relative">
                           <div class="arrowb">
                              <img class="arrow-content" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/arrow.png" alt="">
                           </div>
                           <div class="arrow-textb text-center mx-auto mt-4">
                              <p>Isn't this what all solopreneur moms of two kids look like every night? LOL</p>
                           </div>
                        </div>
                     </div>
                     <div class="img-content">
                        <span>
                           <p class="line1 mt-3 pb-3"> Using those tools to shift your subconscious is like trying to knock down a brick wall with a toothpick It doesn’t make a dent!</p>
                        </span>
                        <span>
                           <p class="line2 pb-5">It might make you “feel” like you’re doing something about the problem, but there’s no real progress.</p>
                        </span>
                        <span>
                           <p class="line3 pb-4">Well, get ready to put a sledgehammer to that wall with the</p>
                        </span>
                     </div>
                     <div class="smackdown">
                        <img class="logo_icon" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/logo_icon.png" alt="logo">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="reasons p-5">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <div class="reasons-content px-3 pb-3">
                        <span>
                           <h3 class="text-center reason-head"> Get Ready to meet your Subconscious and the REAL reasons that keep you from:</h3>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container ">
               <div class="row">
                  <div class="col-2"></div>
                  <div class="col-8">
                     <div class="reasons-point">
                        <ul>
                           <li>Making Massive Shifts in your Income</li>
                           <li>Finding the motivation to start planning out your dream life and business</li>
                           <li>Figuring out why you can’t work less and make more</li>
                           <li>Being further along for the amount of time and energy you’ve already put in</li>
                           <li>Visibility, marketing and the right strategies to reach your goals</li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-2"></div>
               </div>
            </div>
         </div>
         <div class="signup ">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <div class="text-center mt-3 mb-3">
                        <p class="sign-text pb-2">Sign me Up for the Smackdown!</p>
                     </div>
                  </div>
               </div>
               <form method="POST" action="https://agneskowalskiinc.activehosted.com/proc.php" id="_form_17_" class="_form _form_17 _inline-form _inline-style _dark" novalidate>
                  <input type="hidden" name="u" value="17" />
                  <input type="hidden" name="f" value="17" />
                  <input type="hidden" name="s" />
                  <input type="hidden" name="c" value="0" />
                  <input type="hidden" name="m" value="0" />
                  <input type="hidden" name="act" value="sub" />
                  <input type="hidden" name="v" value="2" />
                  <div class="_form-content">
                     <div class="row text-center">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                           <div class="input-f position-relative d-inline-block float-right mb-3">
                              <input class="txt-name border float-right" type="text"  name="fullname" placeholder="Name" required>
                           </div>
                                                       
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                           <div class="input-f position-relative d-inline-block float-left mb-3">
                              <input class="txt-name border float-left" type="text" name="email" placeholder="Email" required>
                           </div>
                        </div>
                     </div>
                     <div class="top-buffer05"></div>
                     <div class="sub-butto pt-2">
                        <div class="btn-sign-up text-center">
                           <button type="submit" name="subscribe" id="_form_1_submit" class="btnsignup btn mt-5 mb-3">SIGN UP NOW!</button>
                        </div>
                     </div>
                     <div class="_clear-element"></div>
                  </div>
                  <div class="_form-thank-you" style="display:none;"></div>
               </form>
            </div>
            <div class="footer">
               <div class="container-fluid ">
                  <div class="text-center mt-4 footer-text">
                     <i class="fa fa-copyright " aria-hidden="true"></i>
                     2018 All rights reserved
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery-3.3.1.slim.min.js"></script>
      <script type="text/javascript">
         window.cfields = [];
         window._show_thank_you = function(id, message, trackcmp_url) {
             var form = document.getElementById('_form_' + id + '_'), thank_you = form.querySelector('._form-thank-you');
             form.querySelector('._form-content').style.display = 'none';
             thank_you.innerHTML = message;
             thank_you.style.display = 'block';
             if (typeof(trackcmp_url) != 'undefined' && trackcmp_url) {
                 // Site tracking URL to use after inline form submission.
                 _load_script(trackcmp_url);
             }
             if (typeof window._form_callback !== 'undefined') window._form_callback(id);
         };
         window._show_error = function(id, message, html) {
             var form = document.getElementById('_form_' + id + '_'), err = document.createElement('div'), button = form.querySelector('button'), old_error = form.querySelector('._form_error');
             if (old_error) old_error.parentNode.removeChild(old_error);
             err.innerHTML = message;
             err.className = '_error-inner _form_error _no_arrow';
             var wrapper = document.createElement('div');
             wrapper.className = '_form-inner';
             wrapper.appendChild(err);
             button.parentNode.insertBefore(wrapper, button);
             document.querySelector('[id^="_form"][id$="_submit"]').disabled = false;
             if (html) {
                 var div = document.createElement('div');
                 div.className = '_error-html';
                 div.innerHTML = html;
                 err.appendChild(div);
             }
         };
         window._load_script = function(url, callback) {
             var head = document.querySelector('head'), script = document.createElement('script'), r = false;
             script.type = 'text/javascript';
             script.charset = 'utf-8';
             script.src = url;
             if (callback) {
                 script.onload = script.onreadystatechange = function() {
                     if (!r && (!this.readyState || this.readyState == 'complete')) {
                         r = true;
                         callback();
                     }
                 };
             }
             head.appendChild(script);
         };
         (function() {
             if (window.location.search.search("excludeform") !== -1) return false;
             var getCookie = function(name) {
                 var match = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]+)'));
                 return match ? match[2] : null;
             }
             var setCookie = function(name, value) {
                 var now = new Date();
                 var time = now.getTime();
                 var expireTime = time + 1000 * 60 * 60 * 24 * 365;
                 now.setTime(expireTime);
                 document.cookie = name + '=' + value + '; expires=' + now + ';path=/';
             }
             var addEvent = function(element, event, func) {
                 if (element.addEventListener) {
                     element.addEventListener(event, func);
                     } else {
                     var oldFunc = element['on' + event];
                     element['on' + event] = function() {
                         oldFunc.apply(this, arguments);
                         func.apply(this, arguments);
                     };
                 }
             }
             var _removed = false;
             var form_to_submit = document.getElementById('_form_17_');
             var allInputs = form_to_submit.querySelectorAll('input, select, textarea'), tooltips = [], submitted = false;
             
             var getUrlParam = function(name) {
                 var regexStr = '[\?&]' + name + '=([^&#]*)';
                 var results = new RegExp(regexStr, 'i').exec(window.location.href);
                 return results != undefined ? decodeURIComponent(results[1]) : false;
             };
             
             for (var i = 0; i < allInputs.length; i++) {
                 var regexStr = "field\\[(\\d+)\\]";
                 var results = new RegExp(regexStr).exec(allInputs[i].name);
                 if (results != undefined) {
                     allInputs[i].dataset.name = window.cfields[results[1]];
                     } else {
                     allInputs[i].dataset.name = allInputs[i].name;
                 }
                 var fieldVal = getUrlParam(allInputs[i].dataset.name);
                 
                 if (fieldVal) {
                     if (allInputs[i].type == "radio" || allInputs[i].type == "checkbox") {
                         if (allInputs[i].value == fieldVal) {
                             allInputs[i].checked = true;
                         }
                         } else {
                         allInputs[i].value = fieldVal;
                     }
                 }
             }
             
             var remove_tooltips = function() {
                 for (var i = 0; i < tooltips.length; i++) {
                     tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
                 }
                 tooltips = [];
             };
             var remove_tooltip = function(elem) {
                 for (var i = 0; i < tooltips.length; i++) {
                     if (tooltips[i].elem === elem) {
                         tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
                         tooltips.splice(i, 1);
                         return;
                     }
                 }
             };
             var create_tooltip = function(elem, text) {
                 var tooltip = document.createElement('div'), arrow = document.createElement('div'), inner = document.createElement('div'), new_tooltip = {};
                 if (elem.type != 'radio' && elem.type != 'checkbox') {
                     tooltip.className = '_error';
                     arrow.className = '_error-arrow';
                     inner.className = '_error-inner';
                     inner.innerHTML = text;
                     tooltip.appendChild(arrow);
                     tooltip.appendChild(inner);
                     elem.parentNode.appendChild(tooltip);
                     } else {
                     tooltip.className = '_error-inner _no_arrow';
                     tooltip.innerHTML = text;
                     elem.parentNode.insertBefore(tooltip, elem);
                     new_tooltip.no_arrow = true;
                 }
                 new_tooltip.tip = tooltip;
                 new_tooltip.elem = elem;
                 tooltips.push(new_tooltip);
                 return new_tooltip;
             };
             var resize_tooltip = function(tooltip) {
                 var rect = tooltip.elem.getBoundingClientRect();
                 var doc = document.documentElement, scrollPosition = rect.top - ((window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0));
                 if (scrollPosition < 40) {
                     tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _below';
                     } else {
                     tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _above';
                 }
             };
             var resize_tooltips = function() {
                 if (_removed) return;
                 for (var i = 0; i < tooltips.length; i++) {
                     if (!tooltips[i].no_arrow) resize_tooltip(tooltips[i]);
                 }
             };
             var validate_field = function(elem, remove) {
                 var tooltip = null, value = elem.value, no_error = true;
                 remove ? remove_tooltip(elem) : false;
                 if (elem.type != 'checkbox') elem.className = elem.className.replace(/ ?_has_error ?/g, '');
                 if (elem.getAttribute('required') !== null) {
                     if (elem.type == 'radio' || (elem.type == 'checkbox' && /any/.test(elem.className))) {
                         var elems = form_to_submit.elements[elem.name];
                         if (!(elems instanceof NodeList || elems instanceof HTMLCollection) || elems.length <= 1) {
                             no_error = elem.checked;
                         }
                         else {
                             no_error = false;
                             for (var i = 0; i < elems.length; i++) {
                                 if (elems[i].checked) no_error = true;
                             }
                         }
                         if (!no_error) {
                             tooltip = create_tooltip(elem, "Please select an option.");
                         }
                         } else if (elem.type =='checkbox') {
                         var elems = form_to_submit.elements[elem.name], found = false, err = [];
                         no_error = true;
                         for (var i = 0; i < elems.length; i++) {
                             if (elems[i].getAttribute('required') === null) continue;
                             if (!found && elems[i] !== elem) return true;
                             found = true;
                             elems[i].className = elems[i].className.replace(/ ?_has_error ?/g, '');
                             if (!elems[i].checked) {
                                 no_error = false;
                                 elems[i].className = elems[i].className + ' _has_error';
                                 err.push("Checking %s is required".replace("%s", elems[i].value));
                             }
                         }
                         if (!no_error) {
                             tooltip = create_tooltip(elem, err.join('<br/>'));
                         }
                         } else if (elem.tagName == 'SELECT') {
                         var selected = true;
                         if (elem.multiple) {
                             selected = false;
                             for (var i = 0; i < elem.options.length; i++) {
                                 if (elem.options[i].selected) {
                                     selected = true;
                                     break;
                                 }
                             }
                             } else {
                             for (var i = 0; i < elem.options.length; i++) {
                                 if (elem.options[i].selected && !elem.options[i].value) {
                                     selected = false;
                                 }
                             }
                         }
                         if (!selected) {
                             elem.className = elem.className + ' _has_error';
                             no_error = false;
                             tooltip = create_tooltip(elem, "Please select an option.");
                         }
                         } else if (value === undefined || value === null || value === '') {
                         elem.className = elem.className + ' _has_error';
                         no_error = false;
                         tooltip = create_tooltip(elem, "This field is required.");
                     }
                 }
                 if (no_error && elem.name == 'email') {
                     if (!value.match(/^[\+_a-z0-9-'&=]+(\.[\+_a-z0-9-']+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i)) {
                         elem.className = elem.className + ' _has_error';
                         no_error = false;
                         tooltip = create_tooltip(elem, "Enter a valid email address.");
                     }
                 }
                 if (no_error && /date_field/.test(elem.className)) {
                     if (!value.match(/^\d\d\d\d-\d\d-\d\d$/)) {
                         elem.className = elem.className + ' _has_error';
                         no_error = false;
                         tooltip = create_tooltip(elem, "Enter a valid date.");
                     }
                 }
                 tooltip ? resize_tooltip(tooltip) : false;
                 return no_error;
             };
             var needs_validate = function(el) {
                 return el.name == 'email' || el.getAttribute('required') !== null;
             };
             var validate_form = function(e) {
                 var err = form_to_submit.querySelector('._form_error'), no_error = true;
                 if (!submitted) {
                     submitted = true;
                     for (var i = 0, len = allInputs.length; i < len; i++) {
                         var input = allInputs[i];
                         if (needs_validate(input)) {
                             if (input.type == 'text') {
                                 addEvent(input, 'blur', function() {
                                     this.value = this.value.trim();
                                     validate_field(this, true);
                                 });
                                 addEvent(input, 'input', function() {
                                     validate_field(this, true);
                                 });
                                 } else if (input.type == 'radio' || input.type == 'checkbox') {
                                 (function(el) {
                                     var radios = form_to_submit.elements[el.name];
                                     for (var i = 0; i < radios.length; i++) {
                                         addEvent(radios[i], 'click', function() {
                                             validate_field(el, true);
                                         });
                                     }
                                 })(input);
                                 } else if (input.tagName == 'SELECT') {
                                 addEvent(input, 'change', function() {
                                     validate_field(this, true);
                                 });
                             }
                         }
                     }
                 }
                 remove_tooltips();
                 for (var i = 0, len = allInputs.length; i < len; i++) {
                     var elem = allInputs[i];
                     if (needs_validate(elem)) {
                         if (elem.tagName.toLowerCase() !== "select") {
                             elem.value = elem.value.trim();
                         }
                         validate_field(elem) ? true : no_error = false;
                     }
                 }
                 if (!no_error && e) {
                     e.preventDefault();
                 }
                 resize_tooltips();
                 return no_error;
             };
             addEvent(window, 'resize', resize_tooltips);
             addEvent(window, 'scroll', resize_tooltips);
             window._old_serialize = null;
             if (typeof serialize !== 'undefined') window._old_serialize = window.serialize;
             _load_script("//d3rxaij56vjege.cloudfront.net/form-serialize/0.3/serialize.min.js", function() {
                 window._form_serialize = window.serialize;
                 if (window._old_serialize) window.serialize = window._old_serialize;
             });
             var form_submit = function(e) {
                 e.preventDefault();
                 if (validate_form()) {
                     // use this trick to get the submit button & disable it using plain javascript
                     document.querySelector('[id^="_form"][id$="_submit"]').disabled = true;
                     var serialized = _form_serialize(document.getElementById('_form_17_'));
                     var err = form_to_submit.querySelector('._form_error');
                     err ? err.parentNode.removeChild(err) : false;
                     _load_script('https://agneskowalskiinc.activehosted.com/proc.php?' + serialized + '&jsonp=true');
                 }
                 return false;
             };
             addEvent(form_to_submit, 'submit', form_submit);
         })();
         
      </script>
   </body>
</html>