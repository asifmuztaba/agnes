<?php
/*
  Template Name: Video 
 */

if (!session_id()) {
    session_start();
}

if (is_user_logged_in()) {
    ?>
    <?php
    get_header();
    ?>

    <?php if (have_posts()) : ?>  
        <?php while (have_posts()) : the_post(); ?>
         
            <!---login---->
            <section class="login-page">
                <div class="top-buffer6"></div>
                <div class="container">
                    <div class="dashboard">
                        <div class="title sec4">
                            <h3 class="text-center"><?php the_title(); ?></h3>
                            <a class="back_btn_dash" href="<?php echo get_permalink(get_option('dash_page_id')); ?>"> &#60;&#60; Back </a>
                        </div>
                        <div class="top-buffer1"></div>

                        <?php                        
                        $current_user = wp_get_current_user();
                        $subscribed_program = get_field('subscribed_program', 'user_' . $current_user->ID);
                        
                        if (!empty($subscribed_program)) {
                            ?>
                            <?php
                            for($j=0;$j<count($subscribed_program);$j++){
                                $post_id = $subscribed_program[$j]["subscribed_program"]->ID;
                                $program_video_rep = get_field("program_video_rep", $post_id);
                                ?>
                                <?php
                                /*
                                  echo "<pre>";
                                  print_r($program_video_rep);
                                  echo "</pre>";
                                 */
                                ?>

                                <div class="program1 videos">
                                    <h4><?php echo $subscribed_program[$j]["subscribed_program"]->post_title; ?></h4>
                                    <?php foreach ($program_video_rep as $program_video) { ?>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="dash-box">
                                                <a class="video"  title="<?php echo $program_video["pro_vide_title"]; ?>" href="<?php echo $program_video["pro_vid_video"]; ?>"><img class="icon" src="<?php echo get_template_directory_uri(); ?>/images/youtube.png"/><img src="<?php echo $program_video["pro_vid_img"]; ?>"/></a>
                                            </div>

                                            <p class="text-center"><?php echo $program_video["pro_vide_title"]; ?></p>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="display-buffer"></div>

                            <?php } ?>
                        <?php }else{ ?>
                                <p>No subscription is done.</p>
                        <?php }?>
                    </div>
                </div>
                <div class="top-buffer100"></div>
            </section>
            <!--- end Login--->
        <?php endwhile; ?>
    <?php else: ?> 
        <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
    <?php endif; ?>

    <?php
    get_footer();
}
else {
    wp_safe_redirect(get_permalink(get_option('login_page_id'))); /* Redirect To Login page */
    die();
}?>