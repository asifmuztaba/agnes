<?php 
if (function_exists('register_sidebar')) {
    register_sidebar(array(
       'description'   => '',
        'class'         => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>' ,
        'name' => 'Search',
        'id' => 'search'
    ));
    
    register_sidebar(array(
       'description'   => '',
        'class'         => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
        'name' => 'Sidebar',
        'id' => 'sidebar'
    ));
    
    register_sidebar(array(
       'description'   => '',
        'class'         => 'menu-header-menu-container2',
	'before_widget' => '<section class="widget %2$s menu-header-menu-container"  id="%1$s" >',
	'after_widget'  => '</section>',
	'before_title'  => '<h4 class="widget-title">',
	'after_title'   => '</h4>',
        'name' => 'Footer 1st Column',
        'id' => 'footer1'
    ));
    
    register_sidebar(array(
       'description'   => '',
        'class'         => '',
	'before_widget' => '<section class="widget %2$s menu-header-menu-container"  id="%1$s" >',
	'after_widget'  => '</section>',
	'before_title'  => '<h4 class="widget-title">',
	'after_title'   => '</h4>',
        'name' => 'Footer 2nd Column',
        'id' => 'footer2'
    ));
    
    register_sidebar(array(
       'description'   => '',
        'class'         => '',
	'before_widget' => '<section class="widget %2$s menu-header-menu-container"  id="%1$s" >',
	'after_widget'  => '</section>',
	'before_title'  => '<h4 class="widget-title">',
	'after_title'   => '</h4>',
        'name' => 'Footer 3rd Column',
        'id' => 'footer3'
    ));
}
?>