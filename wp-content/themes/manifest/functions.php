<?php
/* Start of Payment Gateway */
include("functions/theme_payment_gateway.php");
/* End of Payment Gateway */

/* include theme widget area */
include("functions/theme_widget_areas.php");//
/* end of include theme widget area */

/* include the Menu Registration page */
include("functions/register_menus.php");
/* end of include the Menu Registration page */

/* include the option page */
include("functions/theme_option_page.php");
/* end of include the option page */

/* Include the custom post type */
include("functions/custom_post_type.php");
/* end ov Include the custom post type */

/* Include the theme's Mail Function */
include("functions/mail_function/send_mail.php");
/* end of theme's Mail Function  */

function sa_register_settings() {
	register_setting( 'bpl_theme_options', 'bpl_options', 'bpl_validate_options' );
}

function bpl_theme_options() {
	add_theme_page( 'Company Name', 'Theme Options', 'edit_theme_options', 'theme_options', 'theme_options_page' );	
}

function theme_options_page() {	include('theme-option.php'); }
function arc_active(){
	$arc_option = array(		
		'arc_logo' => "logo.jpg",
		'arc_address' => "Address Goes Here",
		'arc_email' => "info@companyname.com",		
		'arc_contact_1' => "+11 123456",
		'arc_contact_2' => "+11 123456",
		'arc_menuclr' => "#2845A5",
		'arc_menutclr' => "#FFF",
		'arc_menuhclr' => "#2845A5",
		'arc_menuhtclr' => "#FFF",
		'arc_submenuclr' => "#2845A5",
		'arc_submenutclr' => "#FFF",
		'arc_sctitleclr' => "#2845A5",
		'arc_sclineclr' => "#BFC0DC",
		'arc_remrclr' => "#2845A5",
		'arc_remrtxt' => "READ MORE",
		'arc_fb' => "http://facebook.com/",
		'arc_instagram' => "https://www.instagram.com/",
		'arc_pinterest' => "https://in.pinterest.com/",
		'arc_youtube' => "http://youtube.com/",
		'arc_copyright' => "Copyright © 2016 Company Name. All Rights Reserved",
		);
	if (!get_option('arc_option')) {
		add_option('arc_option', $arc_option, '', 'yes');
	}
}

add_action( 'admin_init', 'arc_active' );
remove_action( 'admin_menu', 'bpl_theme_options' ); 
add_action( 'admin_menu', 'bpl_theme_options' );

/* Primary Navigation Menu */
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'twentyfifteen' ),
	));

function wp_theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( "Top Left Widget", 'twentysixteen' ),
		'id'            => 'top_left_widget',
		'description'   => __( '', 'twentysixteen' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
		));	
	register_sidebar( array(
		'name'          => __( "Bottom Left Widget", 'twentysixteen' ),
		'id'            => 'bottom_left_widget',
		'description'   => __( '', 'twentysixteen' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
		));
	register_sidebar( array(
		'name'          => __( "Bottom Right Widget", 'twentysixteen' ),
		'id'            => 'bottom_right_widget',
		'description'   => __( '', 'twentysixteen' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
		));
	
	register_sidebar( array(
		'name'          => __( 'Reach Out Advertise', 'twentysixteen' ),
		'id'            => 'reach_out_address',
		'description'   => __( '', 'twentysixteen' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
		));
        
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'twentysixteen' ),
		'id'            => 'blog_sidebar',
		'description'   => __( '', 'twentysixteen' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p class="s35">',
		'after_title'   => '</p>',
		));
}
add_action( 'widgets_init', 'wp_theme_widgets_init' );

/* add_action('init', 'prefix_register_post_type'); */

add_theme_support('post-thumbnails', array( 'post', 'page', 'clients', 'manifestation'));





/* Register Custom Post Types */
function clients() {
	$labels = array(
		'name'                  => _x( 'Clients', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Client', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Clients', 'text_domain' ),
		'name_admin_bar'        => __( 'Client', 'text_domain' ),
		'archives'              => __( 'Client Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Client:', 'text_domain' ),
		'all_items'             => __( 'All Clients', 'text_domain' ),
		'add_new_item'          => __( 'Add New Client', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Client', 'text_domain' ),
		'edit_item'             => __( 'Edit Client', 'text_domain' ),
		'update_item'           => __( 'Update Client', 'text_domain' ),
		'view_item'             => __( 'View Client', 'text_domain' ),
		'search_items'          => __( 'Search Client', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Client', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Client', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
	$args = array(
		'label'                 => __( 'Client', 'text_domain' ),
		'description'           => __( 'Client List', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'page-attributes', 'post-formats', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		);
	register_post_type( 'clients', $args );
}
add_action( 'init', 'clients', 0 );
/* ------------------------------------------------------------------------ */
function manifestation() {
	$labels1 = array(
		'name'                  => _x( 'Manifestation', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Manifestation', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Manifestation', 'text_domain' ),
		'name_admin_bar'        => __( 'Manifestation', 'text_domain' ),
		'archives'              => __( 'Manifestation Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Manifestation:', 'text_domain' ),
		'all_items'             => __( 'All Manifestation', 'text_domain' ),
		'add_new_item'          => __( 'Add New Manifestation', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Manifestation', 'text_domain' ),
		'edit_item'             => __( 'Edit Manifestation', 'text_domain' ),
		'update_item'           => __( 'Update Manifestation', 'text_domain' ),
		'view_item'             => __( 'View Manifestation', 'text_domain' ),
		'search_items'          => __( 'Search Manifestation', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Manifestation', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Manifestation', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
	$args1 = array(
		'label'                 => __( 'Manifestation', 'text_domain' ),
		'description'           => __( 'Manifestation List', 'text_domain' ),
		'labels'                => $labels1,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'page-attributes', 'post-formats', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 7,
		'menu_icon'             => 'dashicons-video-alt3',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		);
	register_post_type( 'manifestation', $args1 );
}
add_action( 'init', 'manifestation', 0 );
/* Custom Post Types Ends */





function my_login_logo() {
	?>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700' rel='stylesheet' type='text/css'>    
	<style type="text/css">        
		body.login 
		{
			background-image: url("<?php bloginfo('template_url'); ?>/images/bg.jpg");
			background-position: center top;
			background-repeat: repeat;
			margin:0;
			padding:0;
			color:#c8485f;
			font-family: 'Raleway', sans-serif;
			font-weight: 500;
			overflow-x:hidden;
			width: 100%;
			height: 100%;
		}
		body.login div#login h1 a 
		{
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/LoginLogo.png);
			background-size: 100% auto;
			height: 80px;
			width: 100%;
		}        
		body.login form 
		{
			background:#fcfbfc;
			background:-webkit-gradient(linear,left top,left bottom,from(#fcfbfc),to(#f7f6f7));
			background:-webkit-linear-gradient(#fcfbfc,#f7f6f7);
			background:-moz-linear-gradient(center top,#fcfbfc 0%,#f7f6f7 100%);
			background:-moz-gradient(center top,#fcfbfc 0%,#f7f6f7 100%);
			-webkit-border-radius:5px;
			border-radius:5px;
			-webkit-box-shadow:inset 0 -2px 6px rgba(0,0,0,0.05), inset 0 -2px 30px rgba(0,0,0,0.015), inset 0 1px 0 #fff, 0 1px 2px rgba(0,0,0,0.3);
			box-shadow:inset 0 -2px 6px rgba(0,0,0,0.05), inset 0 -2px 30px rgba(0,0,0,0.015), inset 0 1px 0 #fff, 0 1px 2px rgba(0,0,0,0.3);    
			color:#c8485f;
			text-shadow:0 1px 0 #ffffff;       
		}		
		body.login label 
		{
			color: #c8485f;
			font-size: 14px;
			font-weight: 500;
		}
		body.login .button 
		{
			background: #c8485f;
			border: 1px solid #c8485f;
			text-shadow: 0 -1px 1px #c8485f, 1px 0 1px #c8485f, 0 1px 1px #c8485f, -1px 0 1px #c8485f !important;
			box-shadow: 0 1px 0 #c8485f !important;
		}
		body.login #backtoblog a, body.login #nav a 
		{
			color: #fcfbfc;
			text-decoration: none;
		}
		body.login #backtoblog a, body.login #nav a {
			color: #c8485f;
		}
	</style>	
	<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function remove_customize_page(){
	global $submenu;
	unset($submenu['themes.php'][6]); 
}
add_action( 'admin_menu', 'remove_customize_page');

class header_walker_nav_menu extends Walker_Nav_Menu {  
	
	function start_lvl( &$output, $depth = 0, $args = array() ) {
	
		$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' );
		$display_depth = ( $depth + 1);	
		$output .= "\n" . $indent . '<ul class="col-xs-12 dropdown-menu bg-color l-align border-n">' . "\n";
	}

	
	function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); 
		
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		$output .= $indent . '<li class="border-b n-border border-t '. $class_names . '">';
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes .= '';

		$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$args->link_after,
			$args->after
			);
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

class footer_walker_nav_menu extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' );
		$display_depth = ( $depth + 1);	
		$output .= "\n" . $indent . '<ul class="">' . "\n";
	}


	function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' );
		

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );


		$output .= $indent . '<li class="footer-menu footer-u list-m '. $class_names . '">';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes .= '';

		$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$args->link_after,
			$args->after
			);	  
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

function custom_excerpt_length( $length ) {
	return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


add_shortcode('CommonSidebar', 'ShowCommonSidebar');
function ShowCommonSidebar(){
	ob_start();
	?>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="row left-p">
			<h3 class="text-upper heddings bishop hedding bishop-border"><b> <?php if($reach_out_title != "") { echo $reach_out_title; } else { ?> REACH OUT <?php } ?></b></h3>
		</div>
		<?php
		$count = 0;
		$args = array(
			'post_type' => 'post',                                
			'orderby' => 'post_date',
			'order' => 'DESC',
			'post_status' => 'publish',
			'cat' => '4',
			'showposts' => -1,
			);
		$posts = query_posts($args);
		if ( have_posts() ) : while ( have_posts() ) : the_post();
		$pimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		$video_link = get_field('video_link',$post->ID);
		$commentscount = get_comments_number();
		?>
		<div class="row Key keys left-p top-mr">
			<div class="clearfix top-buffer-30"></div>
			<?php if($pimage[0] != "") { ?><a href="<?php the_permalink(); ?>"><img title="<?php the_title(); ?>" alt="<?php the_title(); ?>" class="img-responsive reach center-block" src="<?php echo $pimage[0]; ?>" ></a><?php } ?>
			<div class="clearfix top-buffer-10"></div>
			<?php if($video_link != "") { ?><div class="sidebarvideo"><iframe width="100%" height="315" src="<?php echo $video_link; ?>" frameborder="0" allowfullscreen></iframe></div><?php } ?>
			<h3 class="text-upper"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<span class="key-s "><?php the_time("M Y"); ?>   &nbsp; &nbsp; | &nbsp; &nbsp;   <?php echo $commentscount." Comments"; ?></span>
			<?php the_excerpt(); ?>            
		</div>
		<?php
		$count++;
		endwhile; endif;
		wp_reset_query();
		?>            
		<div class="row Key keys left-p top-mr">
			<div class="clearfix top-buffer-30"></div>
			<?php dynamic_sidebar( 'reach_out_address' ); ?>
		</div>            
	</div>
	<?php
	$output_string = ob_get_contents();
	ob_end_clean();
	return $output_string;
}

class Post_Category extends WP_Widget { 
	public function __construct() {
		$widget_ops = array('classname' => 'Post_Category', 'description' => 'Displays a Categories!' );
		$this->WP_Widget('Post_Category', 'Post Category', $widget_ops);
	}  	
	function widget($args, $instance) {
		extract($args, EXTR_SKIP);		 $title = apply_filters('widget_title', empty( $instance['title'] ) ? __( 'Categories' ) : $instance['title']);
		$category = empty($instance['category']) ? '' : $instance['category'];
		
		echo (isset($before_widget)?$before_widget:'');

		if (!empty($category))
			echo $category;

		echo (isset($after_widget)?$after_widget:'');
	}
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) ); $title = esc_attr( $instance['title'] ); $category = $instance['category'];
		?><p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>        <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		<p>
			<label for="<?php echo $this->get_field_id('text'); ?>">Category : 
				<select class='widefat' id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text">
					<?php
					global $wpdb;
					$getcategory = $wpdb->get_results("SELECT term_taxonomy_id,term_id FROM ".$wpdb->prefix."term_taxonomy WHERE taxonomy = 'category' ORDER BY term_taxonomy_id ASC");
					$rows = $wpdb->num_rows;
					if($rows > 0) {
						$count = 1;
						foreach($getcategory as $categoryid) {
							$catid = $categoryid->term_taxonomy_id;
							$termid = $categoryid->term_id;

							$getcatgoryname = $wpdb->get_row("SELECT name FROM ".$wpdb->prefix."terms WHERE term_id = '".$catid."'");
							$catname = $getcatgoryname->name;
							?>
							<option value='<?php echo $termid; ?>' <?php if($category == $termid) { echo "selected='selected'"; } ?> > <?php echo $catname; ?> </option>
							<?php
						}
					}
					?>
				</select>                
			</label>
		</p>
		<?php
	}
	function update($new_instance, $old_instance) {
		$instance = $old_instance;		$instance['title'] = $new_instance['title'];
		$instance['category'] = $new_instance['category'];
		return $instance;
	}  
}

add_action( 'widgets_init', create_function('', 'return register_widget("Post_Category");') );


function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
{
	$sets = array();
	if(strpos($available_sets, 'l') !== false)
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
	if(strpos($available_sets, 'u') !== false)
		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
	if(strpos($available_sets, 'd') !== false)
		$sets[] = '23456789';
	if(strpos($available_sets, 's') !== false)
		$sets[] = '!@#$%&*?';
	$all = '';
	$password = '';
	foreach($sets as $set)
	{
		$password .= $set[array_rand(str_split($set))];
		$all .= $set;
	}
	$all = str_split($all);
	for($i = 0; $i < $length - count($sets); $i++)
		$password .= $all[array_rand($all)];
	$password = str_shuffle($password);
	if(!$add_dashes)
		return $password;
	$dash_len = floor(sqrt($length));
	$dash_str = '';
	while(strlen($password) > $dash_len)
	{
		$dash_str .= substr($password, 0, $dash_len) . '-';
		$password = substr($password, $dash_len);
	}
	$dash_str .= $password;
	return $dash_str;
}

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

add_filter( 'retrieve_password_message', 'rv_new_retrieve_password_message', 10, 2 );
function rv_new_retrieve_password_message( $message, $key ){
	// Bail if username or email is not entered
	if ( ! isset( $_POST['user_login'] ) )
		return;
	// Get user's data
	if ( strpos( $_POST['user_login'], '@' ) ) { # by email
		$user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
	} else { # by username
		$login = trim( $_POST['user_login'] );
		$user_data = get_user_by( 'login', $login );
	}
	// Store some info about the user
	$user_fname = $user_data->user_firstname;
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;
	// Assembled the URL for resetting the password
	$reset_url = add_query_arg(array(
		'action' => 'rp',
		'key' => $key,
		'login' => rawurlencode( $user_login )
	), wp_login_url() );
	// Create and return the message
	$message = sprintf( '<h2>%s</h2>', __( 'Hi ' ) . $user_fname );
	$message .= sprintf( '<p>%s</p>', __( 'It looks like you need to reset your password on the site. If this is correct, simply click the link below. If you were not the one responsible for this request, ignore this email and nothing will happen.' ) );
	$message .= sprintf( '<p><a href="%s">%s</a></p>', $reset_url, __( 'Reset Your Password' ) );
	return $message;
}

if ( !function_exists('wp_new_user_notification') ) {
    function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
        $user = new WP_User($user_id);
 
        $user_login = stripslashes($user->user_login);
        $user_email = stripslashes($user->user_email);
 
        $message  = sprintf(__('New user registration on your blog %s:'), get_option('blogname')) . "rnrn";
        $message .= sprintf(__('Username: %s'), $user_login) . "rnrn";
        $message .= sprintf(__('E-mail: %s'), $user_email) . "rn";
 
        @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);
 
        if ( empty($plaintext_pass) )
            return;
 
        $message  = __('Hi there,') . "rnrn";
        $message .= sprintf(__("Welcome to %s! Here's how to log in:"), get_option('blogname')) . "rnrn";
        $message .= wp_login_url() . "rn";
        $message .= sprintf(__('Username: %s'), $user_login) . "rn";
        $message .= sprintf(__('Password: %s'), $plaintext_pass) . "rnrn";
        $message .= sprintf(__('If you have any problems, please contact me at %s.'), get_option('admin_email')) . "rnrn";
        $message .= __('Adios!');
 
        wp_mail($user_email, sprintf(__('[%s] Your username and password'), get_option('blogname')), $message);
 
    }
}

add_action('admin_init', 'disable_dashboard');
function disable_dashboard() {
    if (!is_user_logged_in()) {
        return null;
    }
    if (!defined( 'DOING_AJAX' ) && !current_user_can('administrator') && is_admin()) {
        wp_redirect("http://www.agneskowalski.com/dashboard/");
        exit;
    }
}
add_action('after_password_reset', 'wpse_lost_password_redirect');
function wpse_lost_password_redirect() {
    wp_redirect("http://www.agneskowalski.com/login/");
    exit;
}

// Develop by ashish 


add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );
function ajax_test_enqueue_scripts() {

	wp_register_script('my_amazing_script', get_template_directory_uri() . '/js/jquery-latest.min.js', array('jquery'),'1.1',true);
	wp_enqueue_script('my_amazing_script');
	wp_enqueue_script('love',get_template_directory_uri() . '/js/foundational_data4.js',array('my_amazing_script'));
	wp_localize_script( 'love', 'postlove', array('ajax_url' => admin_url( 'admin-ajax.php' )));

}

//add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );

function post_love_add_love() { 
	 global $wpdb;
	 $userid = get_current_user_id();
	 $lession_id = $_REQUEST['lession_id'];
	 $wpdb->query("UPDATE ".$wpdb->prefix ."users SET lesson_status = ".$lession_id." WHERE ID = ".$userid." AND lesson_status < ".$lession_id);
	die();
	
}


?>