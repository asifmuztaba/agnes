<?php
/*
Template Name: About Page
*/
get_header();
?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		
		<?php
		/* GET THE INTRODUCTION SECTION CUSTOM FIELD DETAILS */
		$the_ID = get_the_ID();
		$introduction_image = get_field("introduction_image",$the_ID);
		$introduction_image_thumb = wp_get_attachment_image_src($introduction_image, 'full');
		$introduction_words = get_field("introduction_words",$the_ID);
		$introduction_lines = get_field("introduction_lines",$the_ID);
		$introduction_tag_lines = get_field("introduction_tag_lines",$the_ID);
		$frontpage_id = get_option('page_on_front');
		$newsletter_heading_1 = get_field("newsletter_heading_1",$frontpage_id);
		$newsletter_heading_2 = get_field("newsletter_heading_2",$frontpage_id);
		/* --- ENDS --- */
		?>
		<!-- INTRODUCTION SECTION STARTS HERE -->
		<section>
			<div class="seaction2">
				<div class="part1">
					<div class="container">
						<div class="set2">
							<div class="hed1">
								<div class="he1">
									<div class="top-buffer3"></div>
									<?php 
									for($i=0;$i<count($introduction_words);$i++) {
										?>
										<?php echo $introduction_words[$i]['introduction_words_line']; ?>
										<?php
									} ?>
								</div>
								<?php 
								for($j=0;$j<count($introduction_lines);$j++) {
									?>
									<div class="top-buffer1"></div>
									<p class="s22"><?php echo $introduction_lines[$j]['introduction_line_text']; ?></p>
									<?php
								} ?>
								<div class="bottom-buffer3"></div>
							</div>
						</div>
					</div>
					<div class="hed2">
						<div class="container">
							<div class="top-buffer3"></div>
							<?php 
							for($k=0;$k<count($introduction_tag_lines);$k++) {
								?>									
								<?php echo $introduction_tag_lines[$k]['introduction_tag_line']; ?>
								<?php
							} ?>
							<div class="bottom-buffer3"></div>
						</div>
					</div>
					<div class="top-buffer"></div>
					<div class="bottom-buffer3" style="display:inline-block;"></div>
					<div class="add-img">
						<img src="<?php echo $introduction_image_thumb[0]; ?>"/>
					</div>
				</div>
			</div>	
		</section>
		<!-- INTRODUCTION SECTION ENDS HERE -->
		

		<?php
		/* GET THE REASON SECTION CUSTOM FIELD DETAILS */
		$reason_title = get_field("reason_title",$the_ID);
		$reason_description = get_field("reason_description",$the_ID);		
		/* --- ENDS --- */
		?>
		<!-- REASON SECTION STARTS -->
		<section>
			<div class="container">
				<div class="sec3">
					<div class="top-buffer3"></div>
					<div class="top-buffer3"></div>
					<p class="s47"><?php echo $reason_title; ?></p>
					<p class="s23"><?php echo $reason_description; ?></p>
					<div class="bottom-buffer3"></div>
					<div class="bottom-buffer3"></div>
				</div>
			</div>
		</section>
		<!-- REASON SECTION ENDS -->
		

		<?php
		/* GET THE BUILD DREAM SECTION CUSTOM FIELD DETAILS */
		$dream_title = get_field("dream_title",$the_ID);
		$dream_line = get_field("dream_line",$the_ID);
		$dream_words = get_field("dream_words",$the_ID);
		$dream_help = get_field("dream_help",$the_ID);
		$dream_background = get_field("dream_background",$the_ID);
		$dream_background_thumb = wp_get_attachment_image_src($dream_background, 'full');
		/* --- ENDS --- */
		$pattern = "=^<p>(.*)</p>$=i";
		preg_match($pattern, $dream_title, $dream_title_final);
		?>
		<style>
			.h-seaction4 .sec4 {
				background-image: url("<?php echo $dream_background_thumb[0]; ?>");				
			}
		</style>
		<!-- BUILD DREAM SECTION STARTS -->
		<section>
			<div class="h-seaction4">
				<div class="sec4">
					<div class="container">
						<div class="hp1">
							<div class="top-buffer3"></div>
							<div class="top-buffer3"></div>
							<p class="s47"><?php echo $dream_title_final[1]; ?></p>
						</div>
						<div class="menus">
							<div class="hmenu">
								<div class="top-buffer3" style="display:inline-block;"></div>
								<?php 
								for($l=0;$l<count($dream_words);$l++) {
									?>									
									<?php echo $dream_words[$l]['dream_word']; ?>
									<?php
								} ?>
								<div class="bottom-buffer3" style="display:inline-block;"></div>
							</div>
						</div>
					</div>
					<div class="hhelp">
						<div class="container">
							<div class="top-buffer3"></div>
							<p class="s30"><?php echo $dream_line; ?></p>							
							<?php 
							for($m=0;$m<count($dream_help);$m++) {
								?>									
								<p class="s47"><?php echo $dream_help[$m]['dream_help_line']; ?></p>
								<?php
							} ?>
							<div class="bottom-buffer3"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BUILD DREAM SECTION ENDS -->


		<?php
		/* GET THE PROGRAM DESCRIPTION SECTION CUSTOM FIELD DETAILS */
		$program_that_says_title = get_field("program_that_says_title",$the_ID);
		$i_show_you_how_to_title = get_field("i_show_you_how_to_title",$the_ID);
		$why_trust_me_title = get_field("why_trust_me_title",$the_ID);		
		$program_left_image = get_field("program_left_image",$the_ID);
		$program_left_image_thumb = wp_get_attachment_image_src($program_left_image, 'full');
		$program_that_says = get_field("program_that_says",$the_ID);
		$i_show_you_how_to = get_field("i_show_you_how_to",$the_ID);
		$why_trust_me = get_field("why_trust_me",$the_ID);
		/* --- ENDS --- */
		?>
		<style>
			.h-seaction5 .sec5 {
				background-image: url("<?php echo $program_left_image_thumb[0]; ?>");  
			}
		</style>
		<!-- PROGRAM DESCRIPTION SECTION STARTS -->
		<section>
			<div class="h-seaction5">
				<div class="sec5">
					<div class="container">
						<div class="advice">
							<div class="top-buffer3"></div>
							<div class="top-buffer2"></div>
							<div class="ad1 arrow">
								<div class="hed5">
									<p><?php echo $program_that_says_title; ?></p>
								</div>
								<div class="top-buffer1"></div>
								<?php 
								for($o=0;$o<count($program_that_says);$o++) {
									?>									
									<p><?php echo $program_that_says[$o]['program_that_says_line']; ?></p>
									<?php
								} ?>
							</div>
							<div class="top-buffer3"></div>
							<div class="top-buffer2"></div>
							<div class="ad1 true">
								<div class="hed5">
									<p><?php echo $i_show_you_how_to_title; ?></p>
								</div>
								<?php 
								for($p=0;$p<count($i_show_you_how_to);$p++) {
									?>
									<div class="top-buffer1"></div>
									<p><?php echo $i_show_you_how_to[$p]['i_show_you_how_to_line']; ?></p>
									<?php
								} ?>
							</div>
							<div class="top-buffer3"></div>
							<div class="top-buffer2"></div>
							<div class="ad1 star">
								<div class="hed5">
									<p><?php echo $why_trust_me_title; ?></p>
								</div>
								<?php 
								for($q=0;$q<count($why_trust_me);$q++) {
									?>
									<div class="top-buffer1"></div>
									<p><?php echo $why_trust_me[$q]['why_trust_me_line']; ?></p>
									<?php
								} ?>
							</div>
							<div class="bottom-buffer3"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- PROGRAM DESCRIPTION SECTION ENDS -->
		

		<?php
		/* GET THE BEHIND THE SCENES SECTION CUSTOM FIELD DETAILS */
		$behind_the_scenes_title = get_field("behind_the_scenes_title",$the_ID);
		$behind_the_scenes_heading = get_field("behind_the_scenes_heading",$the_ID);
		$behind_the_scenes_description = get_field("behind_the_scenes_description",$the_ID);			
		/* --- ENDS --- */
		?>
		<!-- BEHIND THE SCENES SECTION STARTS -->
		<section>
			<div class="h-seaction6">
				<div class="top-buffer3" style="display:inline-block;"></div>
				<div class="top-buffer3"></div>
				<div class="sec6">
					<div class="">
						<div class="">
							<div class="top-buffer3" style="display:inline-block;"></div>
							<div class="discrib">
								<p class="s25"><?php echo $behind_the_scenes_title; ?></p>
								<p class="s64"><?php echo $behind_the_scenes_heading; ?></p>
								<div class="top-buffer1"></div>
								<?php 
								for($n=0;$n<count($behind_the_scenes_description);$n++) {
									?>									
									<p><?php echo $behind_the_scenes_description[$n]['description_line']; ?></p>
									<div class="top-buffer3"></div>
									<?php
								} ?>																
							</div>
						</div>
					</div>
				</div>
				<div class="bottom-buffer3"></div>
				<div class="bottom-buffer3" style="display:inline-block;"></div>
			</div>
		</section>
		<!-- BEHIND THE SCENES SECTION ENDS -->


		<?php
		/* GET THE PRINT MEDIA SECTION CUSTOM FIELD DETAILS */
		$media_left_image = get_field("media_left_image",$the_ID);
		$media_left_image_thumb = wp_get_attachment_image_src($media_left_image, 'full');
		$media_left_text = get_field("media_left_text",$the_ID);
		$media_title = get_field("media_title",$the_ID);
		$media_heading = get_field("media_heading",$the_ID);
		$media_description = get_field("media_description",$the_ID);
		$media_logo_1 = get_field("media_logo_1",$the_ID);
		$media_logo_1_thumb = wp_get_attachment_image_src($media_logo_1, 'full');
		$media_logo_2 = get_field("media_logo_2",$the_ID);
		$media_logo_2_thumb = wp_get_attachment_image_src($media_logo_2, 'full');
		/* --- ENDS --- */			
		?>
		<!-- PRINT MEDIA SECTION STARTS -->
		<section>
			<div class="h-seaction7">
				<div class="container">
					<div class="col-sm-12">
						<div class="bottom-buffer3"></div>
						<div class="row">
							<div class="col-sm-5">
								<div class="bottom-buffer3"></div>
								<div class="news">
									<img src="<?php echo $media_left_image_thumb[0]; ?>" alt="" title=""  />
									<div class="top-buffer3"></div>
									<p class="s36"><?php echo strip_tags($media_left_text, "<br>"); ?></p>
								</div>
							</div>
							<div class="col-sm-7">
								<div class="bottom-buffer3"></div>
								<div class="wnews">
									<p class="s45 book"><?php echo $media_title; ?></p>
									<div class="top-buffer2"></div>
									<p class="s45 bold"><?php echo $media_heading; ?></p>
									<div class="top-buffer3"></div>
									<p><?php echo $media_description; ?></p>
									<div class="top-buffer3"></div>
									<div class="blog">
										<img src="<?php echo $media_logo_1_thumb[0]; ?>" class="image-responsive"/>
									</div>
									<div class="top-buffer3"></div>
									<div class="top-buffer1"></div>
									<div class="tv">
										<img src="<?php echo $media_logo_2_thumb[0]; ?>"/>
										<div class="bottom-buffer3"></div>
										<div class="bottom-buffer1"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="bottom-buffer2"></div>
					</div>
				</div>
			</div>
		</section>
		<!-- PRINT MEDIA SECTION ENDS -->

		<!-- NEWSLETTER SECTION STARTS -->
		<section>
			<div class="seaction8 mailchimp___black">
				<div class="container">
					<div class="sec8">
						<div class="top-buffer3"></div>
						<div class="top-buffer3"></div>
						<div class="box">
							<div class="tag2inn">
								<div class="top-buffer3" style="display:inline-block;"></div>
								<div class="top-buffer2"></div>
								<p class="s23"><?php echo $newsletter_heading_1; ?></p>
								<div class="top-buffer"></div>
								<p class="s36"><?php echo $newsletter_heading_2; ?></p>	

								<!-- ActiveCampaign Integrated Form Starts -->
								<form method="POST" action="https://agneskowalskiinc.activehosted.com/proc.php" id="_form_1_" class="_form _form_1 _inline-form  _dark" novalidate>
  <input type="hidden" name="u" value="1" />
  <input type="hidden" name="f" value="1" />
  <input type="hidden" name="s" />
  <input type="hidden" name="c" value="0" />
  <input type="hidden" name="m" value="0" />
  <input type="hidden" name="act" value="sub" />
  <input type="hidden" name="v" value="2" />
      									<div class="input1 pull-left">
										<input type="text" name="firstname" placeholder="First Name" required/>
									</div>
									<div class="input1 pull-right">
										<input type="text" name="email" placeholder="Email Address" required/>
									</div>									
									<div class="sub-button">
										<img src="<?php echo get_template_directory_uri(); ?>/images/left.png">
										<input type="submit" value="Abundance Now!" name="subscribe" id="_form_1_submit" class="button mailchimp___submit" />
										<img src="<?php echo get_template_directory_uri(); ?>/images/right.png">
									</div>
									
    <div class="_form-thank-you" style="display:none;">
  </div>
								</form><script type="text/javascript">
window.cfields = [];
window._show_thank_you = function(id, message, trackcmp_url) {
  var form = document.getElementById('_form_' + id + '_'), thank_you = form.querySelector('._form-thank-you');
  form.querySelector('._form-content').style.display = 'none';
  thank_you.innerHTML = message;
  thank_you.style.display = 'block';
  if (typeof(trackcmp_url) != 'undefined' && trackcmp_url) {
    // Site tracking URL to use after inline form submission.
    _load_script(trackcmp_url);
  }
  if (typeof window._form_callback !== 'undefined') window._form_callback(id);
};
window._show_error = function(id, message, html) {
  var form = document.getElementById('_form_' + id + '_'), err = document.createElement('div'), button = form.querySelector('button'), old_error = form.querySelector('._form_error');
  if (old_error) old_error.parentNode.removeChild(old_error);
  err.innerHTML = message;
  err.className = '_error-inner _form_error _no_arrow';
  var wrapper = document.createElement('div');
  wrapper.className = '_form-inner';
  wrapper.appendChild(err);
  button.parentNode.insertBefore(wrapper, button);
  document.querySelector('[id^="_form"][id$="_submit"]').disabled = false;
  if (html) {
    var div = document.createElement('div');
    div.className = '_error-html';
    div.innerHTML = html;
    err.appendChild(div);
  }
};
window._load_script = function(url, callback) {
    var head = document.querySelector('head'), script = document.createElement('script'), r = false;
    script.type = 'text/javascript';
    script.charset = 'utf-8';
    script.src = url;
    if (callback) {
      script.onload = script.onreadystatechange = function() {
      if (!r && (!this.readyState || this.readyState == 'complete')) {
        r = true;
        callback();
        }
      };
    }
    head.appendChild(script);
};
(function() {
  if (window.location.search.search("excludeform") !== -1) return false;
  var getCookie = function(name) {
    var match = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]+)'));
    return match ? match[2] : null;
  }
  var setCookie = function(name, value) {
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 1000 * 60 * 60 * 24 * 365;
    now.setTime(expireTime);
    document.cookie = name + '=' + value + '; expires=' + now + ';path=/';
  }
      var addEvent = function(element, event, func) {
    if (element.addEventListener) {
      element.addEventListener(event, func);
    } else {
      var oldFunc = element['on' + event];
      element['on' + event] = function() {
        oldFunc.apply(this, arguments);
        func.apply(this, arguments);
      };
    }
  }
  var _removed = false;
  var form_to_submit = document.getElementById('_form_1_');
  var allInputs = form_to_submit.querySelectorAll('input, select, textarea'), tooltips = [], submitted = false;

  var getUrlParam = function(name) {
    var regexStr = '[\?&]' + name + '=([^&#]*)';
    var results = new RegExp(regexStr, 'i').exec(window.location.href);
    return results != undefined ? decodeURIComponent(results[1]) : false;
  };

  for (var i = 0; i < allInputs.length; i++) {
    var regexStr = "field\\[(\\d+)\\]";
    var results = new RegExp(regexStr).exec(allInputs[i].name);
    if (results != undefined) {
      allInputs[i].dataset.name = window.cfields[results[1]];
    } else {
      allInputs[i].dataset.name = allInputs[i].name;
    }
    var fieldVal = getUrlParam(allInputs[i].dataset.name);

    if (fieldVal) {
      if (allInputs[i].type == "radio" || allInputs[i].type == "checkbox") {
        if (allInputs[i].value == fieldVal) {
          allInputs[i].checked = true;
        }
      } else {
        allInputs[i].value = fieldVal;
      }
    }
  }

  var remove_tooltips = function() {
    for (var i = 0; i < tooltips.length; i++) {
      tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
    }
      tooltips = [];
  };
  var remove_tooltip = function(elem) {
    for (var i = 0; i < tooltips.length; i++) {
      if (tooltips[i].elem === elem) {
        tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
        tooltips.splice(i, 1);
        return;
      }
    }
  };
  var create_tooltip = function(elem, text) {
    var tooltip = document.createElement('div'), arrow = document.createElement('div'), inner = document.createElement('div'), new_tooltip = {};
    if (elem.type != 'radio' && elem.type != 'checkbox') {
      tooltip.className = '_error';
      arrow.className = '_error-arrow';
      inner.className = '_error-inner';
      inner.innerHTML = text;
      tooltip.appendChild(arrow);
      tooltip.appendChild(inner);
      elem.parentNode.appendChild(tooltip);
    } else {
      tooltip.className = '_error-inner _no_arrow';
      tooltip.innerHTML = text;
      elem.parentNode.insertBefore(tooltip, elem);
      new_tooltip.no_arrow = true;
    }
    new_tooltip.tip = tooltip;
    new_tooltip.elem = elem;
    tooltips.push(new_tooltip);
    return new_tooltip;
  };
  var resize_tooltip = function(tooltip) {
    var rect = tooltip.elem.getBoundingClientRect();
    var doc = document.documentElement, scrollPosition = rect.top - ((window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0));
    if (scrollPosition < 40) {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _below';
    } else {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _above';
    }
  };
  var resize_tooltips = function() {
    if (_removed) return;
    for (var i = 0; i < tooltips.length; i++) {
      if (!tooltips[i].no_arrow) resize_tooltip(tooltips[i]);
    }
  };
  var validate_field = function(elem, remove) {
    var tooltip = null, value = elem.value, no_error = true;
    remove ? remove_tooltip(elem) : false;
    if (elem.type != 'checkbox') elem.className = elem.className.replace(/ ?_has_error ?/g, '');
    if (elem.getAttribute('required') !== null) {
      if (elem.type == 'radio' || (elem.type == 'checkbox' && /any/.test(elem.className))) {
        var elems = form_to_submit.elements[elem.name];
        if (!(elems instanceof NodeList || elems instanceof HTMLCollection) || elems.length <= 1) {
          no_error = elem.checked;
        }
        else {
          no_error = false;
          for (var i = 0; i < elems.length; i++) {
            if (elems[i].checked) no_error = true;
          }
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (elem.type =='checkbox') {
        var elems = form_to_submit.elements[elem.name], found = false, err = [];
        no_error = true;
        for (var i = 0; i < elems.length; i++) {
          if (elems[i].getAttribute('required') === null) continue;
          if (!found && elems[i] !== elem) return true;
          found = true;
          elems[i].className = elems[i].className.replace(/ ?_has_error ?/g, '');
          if (!elems[i].checked) {
            no_error = false;
            elems[i].className = elems[i].className + ' _has_error';
            err.push("Checking %s is required".replace("%s", elems[i].value));
          }
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, err.join('<br/>'));
        }
      } else if (elem.tagName == 'SELECT') {
        var selected = true;
        if (elem.multiple) {
          selected = false;
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected) {
              selected = true;
              break;
            }
          }
        } else {
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected && !elem.options[i].value) {
              selected = false;
            }
          }
        }
        if (!selected) {
          elem.className = elem.className + ' _has_error';
          no_error = false;
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (value === undefined || value === null || value === '') {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "This field is required.");
      }
    }
    if (no_error && elem.name == 'email') {
      if (!value.match(/^[\+_a-z0-9-'&=]+(\.[\+_a-z0-9-']+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid email address.");
      }
    }
    if (no_error && /date_field/.test(elem.className)) {
      if (!value.match(/^\d\d\d\d-\d\d-\d\d$/)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid date.");
      }
    }
    tooltip ? resize_tooltip(tooltip) : false;
    return no_error;
  };
  var needs_validate = function(el) {
    return el.name == 'email' || el.getAttribute('required') !== null;
  };
  var validate_form = function(e) {
    var err = form_to_submit.querySelector('._form_error'), no_error = true;
    if (!submitted) {
      submitted = true;
      for (var i = 0, len = allInputs.length; i < len; i++) {
        var input = allInputs[i];
        if (needs_validate(input)) {
          if (input.type == 'text') {
            addEvent(input, 'blur', function() {
              this.value = this.value.trim();
              validate_field(this, true);
            });
            addEvent(input, 'input', function() {
              validate_field(this, true);
            });
          } else if (input.type == 'radio' || input.type == 'checkbox') {
            (function(el) {
              var radios = form_to_submit.elements[el.name];
              for (var i = 0; i < radios.length; i++) {
                addEvent(radios[i], 'click', function() {
                  validate_field(el, true);
                });
              }
            })(input);
          } else if (input.tagName == 'SELECT') {
            addEvent(input, 'change', function() {
              validate_field(this, true);
            });
          }
        }
      }
    }
    remove_tooltips();
    for (var i = 0, len = allInputs.length; i < len; i++) {
      var elem = allInputs[i];
      if (needs_validate(elem)) {
        if (elem.tagName.toLowerCase() !== "select") {
          elem.value = elem.value.trim();
        }
        validate_field(elem) ? true : no_error = false;
      }
    }
    if (!no_error && e) {
      e.preventDefault();
    }
    resize_tooltips();
    return no_error;
  };
  addEvent(window, 'resize', resize_tooltips);
  addEvent(window, 'scroll', resize_tooltips);
  window._old_serialize = null;
  if (typeof serialize !== 'undefined') window._old_serialize = window.serialize;
  _load_script("//d3rxaij56vjege.cloudfront.net/form-serialize/0.3/serialize.min.js", function() {
    window._form_serialize = window.serialize;
    if (window._old_serialize) window.serialize = window._old_serialize;
  });
  var form_submit = function(e) {
    e.preventDefault();
    if (validate_form()) {
      // use this trick to get the submit button & disable it using plain javascript
      document.querySelector('[id^="_form"][id$="_submit"]').disabled = true;
            var serialized = _form_serialize(document.getElementById('_form_1_'));
      var err = form_to_submit.querySelector('._form_error');
      err ? err.parentNode.removeChild(err) : false;
      _load_script('https://agneskowalskiinc.activehosted.com/proc.php?' + serialized + '&jsonp=true');
    }
    return false;
  };
  addEvent(form_to_submit, 'submit', form_submit);
})();

</script>

								<div class="bottom-buffer3"></div>
								<div class="bottom-buffer2" style="display:inline-block;"></div>
							</div>
						</div>
						<div class="bottom-buffer2"></div>
						<div class="bottom-buffer3"></div>
					</div>
				</div>
			</div>
		</section>
		<!-- NEWSLETTER SECTION ENDS -->

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>