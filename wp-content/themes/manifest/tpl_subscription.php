<?php
/*
  Template Name: User Subscription list
 */

if (!session_id()) {
    session_start();
}

if (is_user_logged_in()) {
    ?>
    <?php
    get_header();
    ?>

    <?php if (have_posts()) : ?>  
        <?php while (have_posts()) : the_post(); ?>

            <!---login---->
            <section class="a_table">
                <div class="top-buffer100"></div>
                <div class="container">
                    <div class="admin_table">
                        <div class="title sec4">
                            <h3 class="text-center"><?php the_title(); ?></h3>
                            <a class="back_btn_dash" href="<?php echo get_permalink(get_option('dash_page_id')); ?>"> &#60;&#60; Back To Dashboard </a>
                        </div>
                        <div class="top-buffer3"></div>
                        <div class="login-form">
                            <?php
                            $current_user = wp_get_current_user();

                            $subscribed_program = get_field('subscribed_program', 'user_' . $current_user->ID);

                            if (!empty($subscribed_program)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr.No.</th>
                                            <th>Program Name</th>
                                            <th>Purchase Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $pay_no = 1;
                                        for ($j = 0; $j < count($subscribed_program); $j++) {
                                            /*
                                            echo "<pre>";
                                            print_r($subscribed_program[$j]);
                                            echo "</pre>";
                                             */
                                            $date = date_create($subscribed_program[$j]['payment_detail']->post_date);
                                            $program_name = $subscribed_program[$j]['subscribed_program']->post_title;
                                            ?>
                                            <tr>
                                                <td><?php echo $pay_no; ?></td>
                                                <td><?php echo $program_name; ?></td>
                                                <td><?php echo date_format($date, "d/m/Y"); ?></td>
                                            </tr>
                                            <?php
                                            $pay_no++;
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <p> No subscription has been done. </p>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="top-buffer100"></div>
            </section>
            <!--- end Login--->

        <?php endwhile; ?>
    <?php else: ?> 
        <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
    <?php endif; ?>

    <?php
    get_footer();
}
else {
    wp_safe_redirect(get_permalink(get_option('login_page_id'))); /* Redirect To Login page */
    die();
}?>