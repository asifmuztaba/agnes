<?php
/*
  Template Name: PDF
 */

if (!session_id()) {
    session_start();
}

if (is_user_logged_in()) {
    ?>
    <?php
    get_header();
    ?>

    <?php if (have_posts()) : ?>  
        <?php while (have_posts()) : the_post(); ?>


            <!---login---->
            <section class="login-page">
                <div class="top-buffer6"></div>
                <div class="container">
                    <div class="dashboard pdfs">
                        <div class="title sec4">
                            <h3 class="text-center"><?php the_title(); ?></h3>
                            <a class="back_btn_dash" href="<?php echo get_permalink(get_option('dash_page_id')); ?>"> &#60;&#60; Back </a>
                        </div>
                        <div class="top-buffer1"></div>

                        <?php   
                        $current_user = wp_get_current_user();
                        $subscribed_program = get_field('subscribed_program', 'user_' . $current_user->ID);
                        
                        if (!empty($subscribed_program)) {
                            ?>
                            <?php 
                            for($j=0;$j<count($subscribed_program);$j++){
                                $post_id = $subscribed_program[$j]["subscribed_program"]->ID;
                                $pro_pdf_rep = get_field("pro_pdf_rep",$post_id);
                                ?>
                            <?php
                            
//                             echo "<pre>";
//                             print_r($pro_pdf_rep);
//                             echo "</pre>";
                            
                            ?>
                                <div class="program1">
                                    <h4><?php echo $subscribed_program[$j]["subscribed_program"]->post_title; ?></h4>
                                    <?php foreach ($pro_pdf_rep as $pro_pdf) { ?>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="dash-box">
                                            <div class="top-buffer8"></div>
                                            <h1 class="s36"><?php echo $pro_pdf["pro_pdf_title"]; ?></h1>
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/pdf.png" class="img-responsive"/>
                                        </div>
                                        <p class="text-center"><u><a target="_blank" href="<?php echo $pro_pdf["pro_pdf_dwnld"]; ?>">Download Now</a></u></p>
                                    </div>
                                    <?php }?>
                                </div>
                                <div class="display-buffer"></div>
                            <?php } ?>
                        <?php }else{ ?>
                                <p>No subscription is done.</p>
                        <?php }?>
                    </div>
                </div>
                <div class="top-buffer100"></div>
            </section>
            <!--- end Login--->


        <?php endwhile; ?>
    <?php else: ?> 
        <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
    <?php endif; ?>

    <?php
    get_footer();
}
else {
    wp_safe_redirect(get_permalink(get_option('login_page_id'))); /* Redirect To Login page */
    die();
}?>