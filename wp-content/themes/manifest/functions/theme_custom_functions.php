<?php
/* Include google map function */
include ("theme_custom_functions/my_map.php");
/* end of Include google map function */

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

add_action( 'init', 'blockusers_init' );

function blockusers_init() {
    if ( is_admin() && ! current_user_can( 'administrator' ) && 
       ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_redirect( home_url() );
        exit;
    }
}

function cm_breadcrumbs() {

    // Settings
    $separator = ' ';
    $breadcrums_id = 'breadcrumbs';
    $breadcrums_class = 'breadcrumbs';
    $home_title = 'Home';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy = 'product_cat';

    // Get the query & post information
    global $post, $wp_query;

    // Do not display on the homepage
    if (!is_front_page()) {


        // Home page
        echo '<a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a> ';

        if (is_archive() && !is_tax() && !is_category() && !is_tag()) {

            echo '<a>' . post_type_archive_title($prefix, false) . '</a> ';
        } else if (is_archive() && is_tax() && !is_category() && !is_tag()) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if ($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a> ';
            }

            $custom_tax_name = get_queried_object()->name;
            echo '<a class="bread-current bread-archive">' . $custom_tax_name . '</a> ';
        } else if (is_single()) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if ($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a>';
            }

            // Get post category info
            $category = get_the_category();

            if (!empty($category)) {
                $count_cat = count($category);
                // Get last category post is in
                if ($count_cat != 1) {
                    $last_category = end(array_values($category));
                } else {
                    $last_category = $category[0];
                }
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','), ',');
                $cat_parents = explode(',', $get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach ($cat_parents as $parents) {
                    if (empty($parents)) {
                        $cat_display .= '<a class="item-cat">' . $parents . '</a>';
                    }
                }
            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if (empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                $taxonomy_terms = get_the_terms($post->ID, $custom_taxonomy);
                $cat_id = $taxonomy_terms[0]->term_id;
                $cat_nicename = $taxonomy_terms[0]->slug;
                $cat_link = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name = $taxonomy_terms[0]->name;
            }

            // Check if the post is in a category
            if (!empty($last_category)) {
                echo $cat_display;
                echo '<a class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></a>';

                // Else if post is in a custom taxonomy
            } else if (!empty($cat_id)) {

                echo '<a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a>';
                echo '<a class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</a>';
            } else {

                echo '<a class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</a>';
            }
        } else if (is_category()) {

            // Category page
            echo '<a class="bread-current bread-cat">' . single_cat_title('', false) . '</a>';
        } else if (is_page()) {

            // Standard page
            if ($post->post_parent) {

                // If child page, get parents 
                $anc = get_post_ancestors($post->ID);

                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                if (!isset($parents))
                    $parents = null;
                foreach ($anc as $ancestor) {
                    $parents .= '<a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a> ';
                }

                // Display parent pages
                echo $parents;

                // Current page
                echo '<a title="' . get_the_title() . '"> ' . get_the_title() . '</a>';
            } else {

                // Just display current page if not parents
                echo '<a class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</a>';
            }
        } else if (is_tag()) {

            // Tag page
            // Get tag information
            $term_id = get_query_var('tag_id');
            $taxonomy = 'post_tag';
            $args = 'include=' . $term_id;
            $terms = get_terms($taxonomy, $args);
            $get_term_id = $terms[0]->term_id;
            $get_term_slug = $terms[0]->slug;
            $get_term_name = $terms[0]->name;

            // Display the tag name
            echo '<a class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</a>';
        } elseif (is_day()) {

            // Day archive
            // Year link
            echo '<a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a>';

            // Month link
            echo '<a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a>';

            // Day display
            echo '<a class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</a>';
        } else if (is_month()) {

            // Month Archive
            // Year link
            echo '<a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a>';

            // Month display
            echo '<a class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a>';
        } else if (is_year()) {

            // Display year archive
            echo '<a class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a>';
        } else if (is_author()) {

            // Auhor archive
            // Get the author information
            global $author;
            $userdata = get_userdata($author);

            // Display author name
            echo '<a class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</a>';
        } else if (get_query_var('paged')) {

            // Paginated archives
            echo '<a class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">' . __('Page') . ' ' . get_query_var('paged') . '</a>';
        } else if (is_search()) {

            // Search results page
            echo '<a class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</a>';
        } elseif (is_404()) {

            // 404 page
            echo '<a>' . 'Error 404' . '</a>';
        }
    }
}

function cm_setup() {
    add_image_size('tile-small', 271, 295, true); // Hard crop to exact dimensions (crops sides or top and bottom)    
}

add_action('after_setup_theme', 'cm_setup');

/* Tile category child category ajaz */

function codecanal_ajax_enqueue() {
    wp_localize_script('ajax-script', 'ceramic', array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('wp_enqueue_scripts', 'codecanal_ajax_enqueue');

add_action('wp_ajax_nopriv_get_child_tax_tile', 'ajax_get_children_tile_cat');
add_action('wp_ajax_get_child_tax_tile', 'ajax_get_children_tile_cat');

function ajax_get_children_tile_cat() {

    if (isset($_REQUEST)) {

        $term_id = $_REQUEST['termID'];
        $child_string = "";
        $terms = get_terms(
                array(
                    'taxonomy' => 'tiles-category',
                    'child_of' => $term_id,
                    'hide_empty' => 0,
                )
        );
        if (!empty($terms)) {
            echo "<option value=''>Select Sub Category</option>";
            foreach ($terms as $term_single) {
                echo "<option value='$term_single->term_id'>$term_single->name</option>";
            }
        } else {
            echo "<option value=''>No Sub Category Availabe</option>";
        }

        die();
    }
}


/* Company search child category start */
add_action('wp_ajax_nopriv_get_children_tile_cat_comp', 'ajax_get_children_tile_cat_comp');
add_action('wp_ajax_get_children_tile_cat_comp', 'ajax_get_children_tile_cat_comp');

function ajax_get_children_tile_cat_comp() {

    if (isset($_REQUEST)) {

        $term_id = $_REQUEST['termID'];
        $term = get_term_by('slug', $term_id, 'tiles-category');
        $term_id = $term->term_id;
        $child_string = "";
        $terms = get_terms(
                array(
                    'taxonomy' => 'tiles-category',
                    'child_of' => $term_id,
                    'hide_empty' => 0,
                )
        );
        if (!empty($terms)) {
            echo "<option value=''>Select Sub Category</option>";
            foreach ($terms as $term_single) {
                echo "<option value='$term_single->slug'>$term_single->name</option>";
            }
        } else {
            echo "<option value=''>No Sub Category Availabe</option>";
        }
        die();
    }
}

/* Company search child category End */


/* Function for saving title in post meta start 

function save_company_meta($post_id, $post, $update) {


    $post_type = get_post_type($post_id);

    // If this isn't a 'book' post, don't update it.
    if ("company" != $post_type)
        return;

    // - Update the post's metadata.
    $post_title = get_the_title($post_id);

    update_post_meta($post_id, 'company_title', sanitize_text_field($post_title));
}
add_action('save_post', 'save_company_meta', 10, 3);
function save_tile_meta($post_id, $post, $update) {

    $post_type = get_post_type($post_id);

    // If this isn't a 'book' post, don't update it.
    if ("tile" != $post_type)
        return;

    // - Update the post's metadata.
    $post_title = get_the_title($post_id);

    update_post_meta($post_id, 'tile_title', sanitize_text_field($post_title));
}

add_action('save_post', 'save_tile_meta', 10, 3);

/* Function for saving title in post meta End */

function wpdocs_my_search_form($form) {
    $form = '<div class="search-type"><div class="searchLob">
            <form role="search" method="get" id="searchform" class="searchform" action="' . home_url('/') . '" >
    <input type="search" class="searchInput" placeholder="Search the website"  value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="' . esc_attr__('Search') . '"  class="searchInputBtn"/>
    </form></div></div>';

    return $form;
}

add_filter('get_search_form', 'wpdocs_my_search_form');

class Front_End_Media {

    /**
     * A simple call to init when constructed
     */
    function __construct() {
        add_action('init', array($this, 'init'));
    }

    /**
     * Init the textdomain and all the the hooks/filters/etc
     */
    function init() {
        load_plugin_textdomain(
                'frontend-media', false, dirname(plugin_basename(__FILE__)) . '/languages/');
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_filter('ajax_query_attachments_args', array($this, 'filter_media'));
        add_shortcode('frontend-button', array($this, 'frontend_shortcode'));
    }

    /**
     * Call wp_enqueue_media() to load up all the scripts we need for media uploader
     */
    function enqueue_scripts() {
        wp_enqueue_media();
        wp_enqueue_script(
                'frontend-js', plugins_url('/', __FILE__) . 'js/frontend.js', array('jquery'), '2015-05-07'
        );
    }

    /**
     * This filter insures users only see their own media
     */
    function filter_media($query) {
        // admins get to see everything
        if (!current_user_can('manage_options'))
            $query['author'] = get_current_user_id();
        return $query;
    }

    function frontend_shortcode($args) {
        // check if user can upload files
        if (current_user_can('upload_files')) {
            $str = __('Select File', 'frontend-media');
            return '<input id="frontend-button" type="button" value="' . $str . '" class="button" style="position: relative; z-index: 1;"><img id="frontend-image" />';
        }
        return __('Please Login To Upload', 'frontend-media');
    }

}

new Front_End_Media();

add_action('add_meta_boxes', function() {
    add_meta_box('inquiry-parent', 'Inquiry Relation', 'inquiry_attributes_meta_box', 'inquiry', 'side', 'default');
});

function inquiry_attributes_meta_box($post) {
    //print_r($post);
    $parent_value = get_post_meta($post->ID);
    //echo "<pre>";
    //print_r($parent_value);
    ?>
    <a target="_blank" href="<?php echo get_permalink($post->post_parent); ?>" ><?php echo get_the_title($post->post_parent); ?></a>
    <?php
    $pages = wp_dropdown_pages(array('post_type' => 'company', 'selected' => $post->post_parent, 'name' => 'parent_id', 'show_option_none' => __('(no parent)'), 'sort_column' => 'menu_order, post_title', 'echo' => 0));
    if (!empty($pages)) {
        //    echo $pages;
    } // end empty pages check
}
?>