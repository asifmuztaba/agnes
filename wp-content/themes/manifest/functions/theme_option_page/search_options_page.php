<?php
add_action('admin_init', 'register_mysearch_settings');

function register_mysearch_settings() {
    register_setting('my-own-theme-options-for-search', 'search_comp');
    register_setting('my-own-theme-options-for-search', 'search_tile');
}

function search_options_page() {
    ?>

    <div class="wrap">
        <h2>Social Links</h2>
            <?php settings_errors(); ?> 
        <form method="post" action="options.php">
            <?php settings_fields('my-own-theme-options-for-search'); ?>
            <?php do_settings_sections('my-own-theme-options-for-search'); ?>
    <?php include('bootstrap_theme_includes.php'); ?>
            <br />
            <div class="row">

                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Search Page Options</h3>
                        </div>
                        <div class="panel-body">
                            <?php
                            $args = array(
                                'posts_per_page' => -1,
                                'orderby' => 'name',
                                'order' => 'DESC',
                                'post_status' => 'publish',
                                'post_type' => 'page',
                                'suppress_filters' => true,
                            );
                            $page_array = get_posts($args);
                            ?>

                            <label for="search_comp">Company Page</label>
                            <select class="form-control" name="search_comp">
                                <?php $reg_page_val = get_option('search_comp'); ?>
                                <?php
                                for ($i = 0; $i < count($page_array); $i++) {
                                    ?>
                                    <option <?php if ($reg_page_val == $page_array[$i]->ID) { ?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID; ?>"><?php echo $page_array[$i]->post_title; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            
                            <br />
                            <label for="search_tile">Tiles Page</label>
                            <select class="form-control" name="search_tile">
                                <?php $reg_page_val = get_option('search_tile'); ?>
                                <?php
                                for ($i = 0; $i < count($page_array); $i++) {
                                    ?>
                                    <option <?php if ($reg_page_val == $page_array[$i]->ID) { ?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID; ?>"><?php echo $page_array[$i]->post_title; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <br />

                        </div>
                    </div>
                </div>

                <?php include 'option_page_sidebar.php'; ?>

            </div>

            <?php submit_button(); ?>

        </form>
    </div>
<?php
}

/* My Theme Option */
?>
