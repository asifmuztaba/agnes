<?php
/*
  Template Name: Test Page
 */
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <title>Agnes Kowalski</title>
        <!-- Bootstrap core CSS -->
        <link href="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/css/style.css" rel="stylesheet">
        <script src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/js/jquery-1.12.4.min.js"></script>
    </head>

    <body>
        <div class="main_div">
            <!--seaction1-->
            <div class="seaction1">
                <div class="sec1">
                    <div class="images"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/img1.png" /> </div>
                    <div class="agrrement">
                        <div class="top-buffer3" style="display:inline-block;"></div>
                        <div class="hed">
                            <p class="s38 blue-back">IMAGINE BEING IN FULL AGREEMENT WITH LIFE</p>
                        </div>
                        <div class="dis">
                            <div class="top-buffer3"></div>
                            <p class="janesue">So you could:</p>
                            <div class="top-buffer3"></div>
                            <p class="spc">Be successful</p>
                            <p class="bek">Be sexy</p>
                            <p class="spc">Be powerful</p>
                            <p class="bek">Live out Loud</p>
                            <p class="spc">Live Fully</p>
                            <p class="bek">Live without Apology</p>
                            <p class="spc">Be totally connected</p>
                            <p class="bek">Be totally magnetic</p>
                            <p class="spc">Be totally manifesting</p>
                            <p class="bek">Express yourself</p>
                            <p class="spc">Express your desires</p>
                            <p class="bek">Express your creativity</p>
                            <p class="spc">Attract money</p>
                            <p class="bek">Attract love</p>
                            <p class="spc">Attract spiritual mastery</p>
                            <div class="top-buffer3"></div>
                            <p class="s27">What if you had 100% full</p>
                        </div>
                    </div>
                    <div class="permission_div">
                        <div class="agrrement">
                            <div class="dis">
                                <h2><span></span>PERMISSION TO PROSPER</h2> </div>
                        </div>
                    </div>
                    <div class="agrrement">
                        <div class="last">
                            <div class="top-buffer2"></div>
                            <p>A 20+ Module Money Mindset Transformation and Mastery Self-Paced Course </p>
                            <div class="top-buffer3"></div>
                            <div class="top-buffer1"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end seaction1-->
            <!--seaction2-->
            <div class="seaction2">
                <div class="sec2">
                    <div class="container">
                        <div class="top-buffer3"></div>
                        <div class="top-buffer3"></div>
                        <div class="points">
                            <p>You want to start living as the person you have always wanted to be (and figure out who the heck that is!)</p>
                            <p>You have never fully activated your inner potential financially, creatively or spiritually</p>
                            <p>You are tired of living for others and want to start living for yourself</p>
                            <p>You want to stop the programs and patterns that run on auto-pilot sabotaging your life and biz</p>
                            <p>You’re ready to leave behind an identity built from lack </p>
                            <p>You want to stop running from your feelings, fears and finances.</p>
                            <p>You are sick of living life by unconscious rules that don’t support your dreams and desires</p>
                        </div>
                        <div class="top-buffer2"></div>
                        <div class="hed2">
                            <h4>THEN</h4>
                            <h1 class="darkblack-blue"> it's time to come home</h1>
                            <h4>TO</h4>
                            <h2 class="darkblack-blue">YOUR ESSENCE</h2>
                            <p class="s22">(and out of wanting, hoping and striving but never arriving)</p>
                        </div>
                        <div class="bottom-buffer3"></div>
                    </div>
                </div>
            </div>
            <!--end seaction2-->
            <!--seaction3-->
            <div class="seaction3">
                <div class="sec3">
                    <div class="top-buffer3" style="display:inline-block;"></div>
                    <div class="top-buffer2"></div>
                    <div class="container">
                        <div class="student">
                            <div class="top-buffer1"></div>
                            <div class="hed2">
                                <h2>Students in this course will have the ability to:</h2> </div>
                            <div class="top-buffer2"></div>
                            <div class="points">
                                <p> Remove the blocks that are in the way of their ability to receive money, love and the most coveted desires</p>
                                <p>Understand how to diffuse resistance that causes a lack of flow financially, emotionally and spiritually</p>
                                <p>Heal emotionally from the stories and identities that have gathered in this lifetime</p>
                                <p>Release the need for fear, anxiety, codependency, shame, control and worry</p>
                                <p>Harness the power of manifestation in life and business</p>
                                <p>Master the laws of prosperity that govern your experiences in finances, love, attraction and opportunity</p>
                                <p>Experience the power of expression and creativity that has not fully actualized</p>
                                <p>Align your vibration to your deepest desires</p>
                                <p>Use mindset methods that leverage your results 10X, 20X and 100X</p>
                                <p>Establish a trust within yourself around love, money and manifesting that you can rely on</p>
                                <p>Create a disciplined spiritual and mindset practice that will transform their reality</p>
                                <div class="bottom-buffer3" style="display:inline-block;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-buffer2"></div>
                    <div class="bottom-buffer3" style="display:inline-block;"></div>
                </div>
            </div>
            <!--end seaction3-->
            <!--seaction4-->
            <div class="seaction4">
                <div class="sec4">
                    <div class="container">
                        <div class="top-buffer3"></div>
                        <div class="top-buffer2"></div>
                        <div class="col-md-4">
                            <div class="image"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/news.png" class="img-responsive" /> </div>
                            <div class="top-buffer3"></div>
                            <div class="img-dis">
                                <p>That’s Me</p>
                                <p class="popins">Mindset Coach</p>
                                <p>and</p>
                                <p class="popins">Wealth Therapist!</p>
                                <div class="top-buffer3"></div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="permit">
                                <div class="hed">
                                    <div class="top-buffer3"></div>
                                    <p class="margin-bottom">GRANT YOURSELF</p>
                                    <div class="top-buffer"></div>
                                    <div class="title_back">
                                        <h2>PERMISSION TO PROSPER</h2> </div>
                                    <div class="top-buffer"></div>
                                    <p>IN EVERY ASPECT OF YOUR LIFE</p>
                                </div>
                            </div>
                            <div class="top-buffer2"></div>
                            <div class="pera">
                                <p class="s22">Could you do it alone? Yes, if you have 10-20 years to burn and close to $100K to study with the masters (which is sort of what I did). Hi I am Agnes Kowalski, money mindset coach and wealth therapist, and yes I have spent somewhere close to that amount on my spiritual, emotional and mindset education and healing. Psychotherapy school, Therapists, Inner child work, Reiki Masters, Psychics, Intuitives, yes I had a self-help addiction, or rather a ‘not-enoughness’ issue and a ‘never-arrived’ issue.</p>
                                <div class="top-buffer3"></div>
                                <p class="s22">It wasn’t until I began to study the power of the subconscious mind and combined that with the depth of emotional healing from therapy that I found the magic that is in the laws of prosperity as myself and my clients have experienced.</p>
                                <div class="top-buffer2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end seaction4-->
            <!--seaction5-->
            <div class="seaction5">
                <div class="sec5">
                    <div class="container">
                        <div class="top-buffer3"></div>
                        <div class="top-buffer"></div>
                        <div class="col-sm-4">
                            <div class="image"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/img3.png" class="img-responsive" /> </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="images-dis">
                                <div class="top-buffer2"></div>
                                <p class="s22">And so after seeing impossible results manifest in my own life and the lives of countless clients transformed, I created a formula for recreating those results. In the wise words of Biggie Smalls (may he rest in peace) why I created this course:</p>
                                <div class="top-buffer3"></div>
                                <div class="disc">
                                    <p><i>I've been in this game for years, it made me a animal</i>
                                    </p>
                                    <p><i>There's rules to this sh*t, I wrote me a manual</i>
                                    </p>
                                    <p><i>A step-by-step booklet for you to get</i>
                                    </p>
                                    <p><i>Your game on track, not your wig pushed back</i>
                                    </p>
                                    <div class="top-buffer3"></div>
                                    <p class="s30" ;>Biggie Smalls</p>
                                </div>
                            </div>
                        </div>
                        <div style="display:inline-block;margin-bottom:40px;width:100%;"></div>
                    </div>
                </div>
            </div>
            <!--end seaction5-->
            <!--seaction6-->
            <div class="seaction6">
                <div class="sec6">
                    <div class="hed2">
                        <div class="top-buffer3"></div>
                        <h2 class="black margin-bottom">MINDSET IS NOT JUST <span>‘Positive Thinking’</span></h2>
                        <p class="s22">(if it were that easy everyone would be rich + happy already)</p>
                        <h2 class="black margin-bottom">THERE ARE RULES THAT GOVERN YOUR PROSPERITY</h2>
                        <p class="s22">I show you how to TAP your subconscious mind to create your own <span>INFINITE SUPPLY!!!!</span>
                        </p>
                        <div class="top-buffer"></div>
                        <div class="top-buffer3 remov"></div>
                    </div>
                    <div class="sec6-dis">
                        <div class="container">
                            <div class="deep-detail">
                                <div class="top-buffer3"></div>
                                <p class="s35">The real reason most manifestation and mindset programs DON’T work...is because the emotional healing doesn’t go deep enough. </p>
                                <div class="top-buffer2"></div>
                                <div class="hed2">
                                    <h2 class="black text-left">Good thing I’m a therapist and I can take you to the root.</h2> </div>
                                <p class="s22">The root of poverty consciousness</p>
                                <p class="s22">The root of shame</p>
                                <p class="s22">The root of lack </p>
                                <p class="s22">The root of low self-worth</p>
                                <p class="s22">The root of powerlessness</p>
                                <div class="top-buffer3"></div>
                                <p class="s35">So you can RISE, out of the ashes, out of the background, out of smallness, out of struggle and into PROSPERITY. </p>
                            </div>
                            <div class="bottom-buffer3" style="margin-bottom:35px;"></div>
                            <div class="bottom-buffer1"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end seaction6-->
            <!--seaction7-->
            <div class="seaction5">
                <div class="sec5">
                    <div class="container">
                        <div class="top-buffer3"></div>
                        <div class="top-buffer"></div>
                        <div class="images-dis">
                            <div class="text-center">
                                <p class="s20">HERE’S HOW WE’RE GOING TO ROLL</p>
                            </div>
                            <div class="hed2">
                                <h2 class="black white padd">20 No Fluff Prosperity Increasing Lessons To Dramatically Shift Your Money Mindset</h2>
                                <p class="p_regular">AND</p>
                                <h2 class="black white">20 No Fluff Assignments And Journal Prompts To Trigger Prosperity And Create New Neuropathways In Your Subconscious</h2> </div>
                        </div>
                        <div style="display:inline-block;margin-bottom:40px;width:100%;"></div>
                    </div>
                </div>
            </div>
            <!-- end seaction7-->
            <!--seaction8-->
            <div class="seaction8">
                <div class="container">
                    <div class="sec8 text-center">
                        <div class="top-buffer3"></div>
                        <div class="top-buffer2"></div>
                        <p class="s20 margin-bottom">THIS FORMULA HAS BEEN CREATED BY </p>
                        <p class="s20 margin-bottom">SEEING THOUSANDS OF CLIENTS, TESTING IT ON HUNDREDS OF PARTICIPANTS,</p>
                        <div class="hed2">
                            <h2 class="darkblack-blue">AND CREATING EXTRAORDINARY RESULTS LIKE THESE:</h2> </div>
                        <div class="top-buffer2"></div>
                        <div class="videos">
                            <div class="col-sm-6">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/oAtdq4t8lb0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="col-sm-6">
                                <iframe width="100%" height="250" src="https://www.youtube.com/embed/BbCLzbmsgDI" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="top-buffer3" style="display:inline-block;"></div>
                        <div class="post_image">
                            <div class="col-sm-4">
                                <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post1.png" /> </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post2.png" /> </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post3.png" /> </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post4.png" /> </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post5.png" /> </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="post_img responce_image"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post6.png" /> </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="post_img responce_image"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post7.png" /> </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="post_img responce_image"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post8.png" /> </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="post_img responce_image"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post9.png" /> </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post10.png" width="100%" /> </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post11.png" width="100%" /> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-buffer3"></div>
                <div class="top-buffer2"></div>
            </div>
        </div>
    </div>
    <!--end seaction8-->
    <div class="seaction5">
        <div class="sec5">
            <div class="container">
                <div class="top-buffer3"></div>
                <div class="top-buffer"></div>
                <div class="diclaration">
                    <p>Prosperity Lesson #1: <span>Declarations</span>
                    </p>
                    <p>Prosperity Lesson #2: <span>Declarations</span>
                    </p>
                    <p>Prosperity Lesson #3: <span>Money Story Deep Dive</span>
                    </p>
                    <p>Prosperity Lesson #4: <span>Prosperous Thinking</span>
                    </p>
                    <p>Prosperity Lesson #5: <span>Prosperous Outcomes</span>
                    </p>
                    <p>Prosperity Lesson #6: <span>Creating an Effective Mindset Practice</span>
                    </p>
                    <p>Prosperity Lesson #7: <span>Prosperous Goal Setting</span>
                    </p>
                    <p>Prosperity Lesson #8: <span>Prosperous Beliefs</span>
                    </p>
                    <p>Prosperity Lesson #9: <span>Prosperous Identity</span>
                    </p>
                    <p>Prosperity Lesson #10: <span>Prosperous Perception</span>
                    </p>
                    <p>Prosperity Lesson #11: <span>Prosperous Paradigms</span>
                    </p>
                    <p>Prosperity Lesson #12: <span>Prosperous Payoffs</span>
                    </p>
                    <p>Prosperity Lesson #13: <span>Troubleshooting your Mindset</span>
                    </p>
                    <p>Prosperity Lesson #14: <span>EFT for Money Mindset + Tapping Scripts</span>
                    </p>
                    <p>Prosperity Lesson #15: <span>Prosperous Responses</span>
                    </p>
                    <p>Prosperity Lesson #16: <span>Prosperous Non-Attachment</span>
                    </p>
                    <p>Prosperity Lesson #17: <span>Prosperous Mindset Stretching Exercises</span>
                    </p>
                    <p>Prosperity Lesson #18: <span>Clearing Emotional Blocks - Grief</span>
                    </p>
                    <p>Prosperity Lesson #19: <span>Clearing Emotional Blocks - Gratitude</span>
                    </p>
                    <p>Prosperity Lesson #20: <span>Clearing Emotional Blocks - Forgiveness</span>
                    </p>
                    <p>Prosperity Lesson #21: <span>Clearing Emotional Blocks - Anger</span>
                    </p>
                    <p>Prosperity Lesson #22/23/24: <span>Bonus Money Mindset Advanced</span>
                    </p>
                </div>
                <div style="display:inline-block;margin-bottom:40px;width:100%;"></div>
            </div>
        </div>
    </div>
    <div class="section8">
        <div class="container">
            <div class="post_image">
                <div class="top-buffer3"></div>
                <div class="top-buffer2"></div>
                <div class="col-sm-5">
                    <div class="post_img res"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post12.png" /> </div>
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="top-buffer3"></div>
                        <div class="col-sm-12">
                            <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post13.png" /> </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post14.png" /> </div>
                        </div>
                    </div>
                </div>
                <div style="display:inline-block;margin-bottom:5px;width:100%;"></div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post15.png" /> </div>
                        </div>
                        <div class="col-sm-12 res">
                            <div class="post_img text-center"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post16.png" style="width:auto;" /> </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="post_img res"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post17.png" /> </div>
                </div>
                <div class="col-sm-12">
                    <div class="post_img"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post18.png" /> </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="post_img responce_image res"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post19.png" style="width:auto;" /> </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="post_img responce_image res"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post20.png" style="width:auto;" /> </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="post_img responce_image res"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post21.png" style="width:auto;" /> </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="post_img responce_image res"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/post22.png" style="width:auto;" /> </div>
                        </div>
                    </div>
                </div>
                <div style="display:inline-block;margin-bottom:40px;width:100%;"></div>
            </div>
        </div>
    </div>
    <!--seaction9-->
    <div class="seaction9">
        <div class="sec9">
            <div class="container">
                <div class="buy">
                    <div class="top-buffer5 none"></div>
                    <div class="top-buffer5"></div>
                    <h1><span>Ready? Set?</span></br> <i>PROSPER!!!</i></h1>
                    <div class="btn1">
                        <p class="s20">I GIVE MYSELF PERMISSION TO PROSPER</p>
                        <div class="top-buffer2"></div>
                        <p class="s22">ONE PAYMENT OF <span>$497</span> SELF-PACED</p>
                        <div class="top-buffer"></div>
                        <?php
                        $template_url = get_template_directory_uri();
                        $ipn = $template_url . "/functions/payment/paypal/ipn.php";
                        $paypal_succ_page = get_option('user_pay_succ_id');
                        $paypal_succ_page = esc_url(get_permalink($paypal_succ_page));
                        $paypal_fail_page = get_option('user_pay_fail_id');
                        $paypal_fail_page = esc_url(get_permalink($paypal_fail_page));
                        $paypal_currency = get_option('paypal_currency');
                        if (get_option('paypal_mode') == "live") { /* if the payment mode is live */
                            $paypal_url = "www.paypal.com";
                            $paypal_merchant_email = get_option('paypal_merchant_email');
                        } else {
                            $paypal_url = "www.sandbox.paypal.com";
                            $paypal_merchant_email = "sandeep_biz@gmail123.com";
                            $paypal_currency = "USD";
                        }
                        /* Start of making the url for the Paypal with the given argument. */ $main_url = "https://" . $paypal_url . "/cgi-bin/webscr";
                        $porgram_id = "423";
                        $program_price = get_field("program_price", $porgram_id);
                        $program_title = get_the_title($porgram_id);
                        $paypal_url = add_query_arg(
                                array(
                            'business' => $paypal_merchant_email,
                            'cmd' => '_xclick',
                            'item_name' => $program_title,
                            'item_number' => $porgram_id,
                            'amount' => 0.50,
                            'custom' => "website",
                            'currency_code' => $paypal_currency,
                            'tax' => 0,
                            'notify_url' => $ipn,
                            'cancel_return' => $paypal_fail_page,
                            'return' => $paypal_succ_page
                                ), $main_url);
                        $mail_paypal_url = add_query_arg(
                                array('business' => $paypal_merchant_email,
                            'cmd' => '_xclick',
                            'item_name' => $program_title,
                            'item_number' => $porgram_id,
                            'amount' => 0.50,
                            'custom' => $porgram_id,
                            'currency_code' => $paypal_currency,
                            'tax' => 0,
                            'notify_url' => $ipn,
                            'cancel_return' => $paypal_fail_page,
                            'return' => $paypal_succ_page
                                ), $main_url);
                        ?>
                        <form action="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GW228DEM85K8Y" method="post" target="_top">
                            <div class="buttn"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/left.png" />
                                <!--<button type="button" name="buy">BUY NOW</button>-->
                                <input type="hidden" name="cmd" value="_s-xclick">
                                <input type="hidden" name="hosted_button_id" value="GW228DEM85K8Y"> <a style="background-position: 15% 35%;border: medium none;border-radius: 20px;color: #000;font-family: Poppins-Bold;font-size: 22px;letter-spacing: 1px;margin: 0 10px 10px;padding: 5px 30px;" href="<?php echo $paypal_url; ?>" class="btn_paypal">Buy Now</a>
                                <!-- <input type="submit" name="submit" value="BUY NOW" alt="PayPal - The safer, easier way to pay online!"> --><img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/right.png" /> </div>
                        </form>
                    </div>
                    <div class="top-buffer2"></div>
                    <p class="or">OR</p>
                    <div class="top-buffer2"></div>
                    <div class="btn1">
                        <p class="s22 margin-bottom">ADD 4 WEEKS EMAIL SUPPORT </p>
                        <p class="s20 margin-bottom">(1 EMAIL PER WEEK FROM THE MONEY MINDSET MASTER HERSELF) </p>
                        <p class="s22"><span>$697</span>
                    </div>
                    <div class="top-buffer"></div>
                    <form action="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E5FPZ44DAYGCJ" method="post" target="_top">
                        <div class="buttn"> <img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/left.png" />
                            <!--<button type="button" name="buy">BUY NOW</button>-->
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="E5FPZ44DAYGCJ"> <a style="background-image: url('http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/btnbk.png'); background-position: 15% 35%;border: medium none;border-radius: 20px;color: #000;font-family: Poppins-Bold;font-size: 22px;letter-spacing: 1px;margin: 0 10px 10px;padding: 5px 30px;" href="<?php echo $mail_paypal_url; ?>" class="btn_paypal">Buy Now</a>
                            <!-- <input type="submit" name="submit" value="BUY NOW" alt="PayPal - The safer, easier way to pay online!"> --><img src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/images/right.png" />
                            <div class="top-buffer2"></div>
                        </div>
                    </form>
                </div>
                <div class="top-buffer5 none"></div>
                <div class="top-buffer5"></div>
            </div>
        </div>
    </div>
</div>
<!--end seaction9-->
<!--footer-->
<footer>
    <div class="container">
        <div class="footer">
            <p>© 2017 ALL RIGHTS RESERVED</p>
        </div>
    </div>
</footer>
<!--end footer-->
</div>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106731453-1');
</script>
</body>

</html>