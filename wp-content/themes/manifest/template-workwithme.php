<?php
/*
Template Name: Work With Me Page
*/
get_header();
?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<?php
		/* GET THE WORK WITH ME SECTION CUSTOM FIELD DETAILS */
		$the_ID = get_the_ID();
		$left_image = get_field("left_image",$the_ID);
		$left_image_thumb = wp_get_attachment_image_src($left_image, 'full');
		$text_line_1 = get_field("text_line_1",$the_ID);
		$text_line_2 = get_field("text_line_2",$the_ID);
		$text_line_3 = get_field("text_line_3",$the_ID);
		$text_line_4 = get_field("text_line_4",$the_ID);
		$text_line_5 = get_field("text_line_5",$the_ID);
		/* --- ENDS --- */
		?>
		<!-- MindSet Coaching Section Starts -->
		<section>
			<div class="s-seaction">
				<div class="container">
					<div class="s-sec">
						<div class="image">
							<img src="<?php echo $left_image_thumb[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
						</div>
						<div class="disc">
							<div class="s-hed">
								<div class="top-buffer100"></div>
								<?php if($text_line_1 != '') { ?>
								<p class="s38"><?php echo $text_line_1; ?></p>
								<div class="top-buffer2"></div>
								<?php } ?>
								<?php if($text_line_2 != '') { ?>								
								<p class="s73"><?php echo $text_line_2; ?></p>
								<div class="top-buffer2"></div>
								<?php } ?>
								<?php if($text_line_3 != '') { ?>								
								<p class="s38"><?php echo $text_line_3; ?></p>
								<div class="top-buffer3"></div>
								<?php } ?>
								<?php if($text_line_4 != '') {
									$pattern = "=^<p>(.*)</p>$=i";
									preg_match($pattern, $text_line_4, $text_line_4_final);
									?>								
									<p class="s41"><?php echo $text_line_4_final[1]; ?></p>
									<div class="top-buffer1"></div>
									<?php 
								} ?>
								<?php if($text_line_5 != '') { ?>								
								<p class="s27"><?php echo $text_line_5; ?></p>
								<div class="top-buffer100"></div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- MindSet Coaching Section Ends -->

		<?php
		/* GET THE TECHNIQUES SECTION CUSTOM FIELD DETAILS */
		$section_title = get_field("section_title",$the_ID);
		$pattern = "=^<p>(.*)</p>$=i";
		preg_match($pattern, $section_title, $section_title_final);
		$technique_repeater = get_field("techniques",$the_ID);
		/* --- ENDS --- */
		?>
		<!-- Technique Section Starts -->
		<section>
			<div class="c-seaction3">
				<div class="container">
					<div class="ctitle">
						<div class="top-buffer3"></div>
						<div class="top-buffer3"></div>
						<p class="s65"><?php echo $section_title_final[1]; ?></p>
					</div>					
					<div class="buffer"></div>
					<div class="row">

						<?php 
						for($i=0;$i<count($technique_repeater);$i++) {							
							$icon_image_thumb = wp_get_attachment_image_src($technique_repeater[$i]['icon_image'], 'full');
							?>
							<div class="col-sm-3 col-md-2">
								<div class="row">
									<div class="icon1">
										<img src="<?php echo $icon_image_thumb[0]; ?>" alt="<?php echo $technique_repeater[$i]['title']; ?>" title="<?php echo $technique_repeater[$i]['title']; ?>" />
									</div>
								</div>
							</div>
							<div class="col-sm-9 col-md-10">
								<div class="experience">
									<div class="top-bufferr2"></div>
									<p class="s35"><?php echo $technique_repeater[$i]['title']; ?></p>
									<p><?php echo $technique_repeater[$i]['description']; ?></p>
								</div>
							</div>
							<div class="top-buffer2" style="display:inline-block;width:100%;"></div>
							<?php 
						} ?>

					</div>
					<div class="top-buffer3"></div>
					<div class="top-buffer2"></div>
				</div>
			</div>
		</section>
		<!-- Technique Section Ends -->

		<?php
		/* GET THE PACKAGE SECTION CUSTOM FIELD DETAILS */		
		$package_title = get_field("package_title",$the_ID);
		$pattern = "=^<p>(.*)</p>$=i";
		// preg_match($pattern, $package_title, $package_title_final);
		$package_description_repeater = get_field("package_description",$the_ID);		
		/* --- ENDS --- */
		?>
		<!-- Package Section Starts -->
		<section>
			<div class="seaction4">
				<div class="container">
					<div class="adds row">
						<div class="top-buffer2" style="display:inline-block;"></div>
						<p class="s45"><?php echo $package_title; ?></span></p>
						<?php 
						for($p=0;$p<count($package_description_repeater);$p++) {
							?>								
							<p class="s22"><?php echo $package_description_repeater[$p]['package_description_text']; ?></p>
							<?php
						} ?>
						<div class="bottom-buffer2" style="display:inline-block;"></div>
					</div>
				</div>
				<div class="bottom-buffer3"></div>
				<div class="bottom-buffer3"></div>
			</div>
		</section>
		<!-- Package Section Ends -->

		<?php
		/* GET THE RESULTS SECTION CUSTOM FIELD DETAILS */
		$left_side_image = get_field("left_side_image",$the_ID);
		$left_side_image_thumb = wp_get_attachment_image_src($left_side_image, 'full');
		$result_title = get_field("result_title",$the_ID);
		$result_notes_repeater = get_field("result_notes",$the_ID);		
		/* --- ENDS --- */
		?>
		<!-- RESULTS SECTION STARTS -->
		<section>
			<div class="cseaction5">
				<div class="rimage">
					<img src="<?php echo $left_side_image_thumb[0]; ?>"/>
				</div>
				<div class="container">
					<div class="result">
						<div class="top-buffer2"></div>
						<p class="s35"><?php echo $result_title; ?></p>
						<div class="result-dis">
							<div class="top-buffer2"></div>
							<?php 
							for($j=0;$j<count($result_notes_repeater);$j++) {
								?>								
								<p><?php echo $result_notes_repeater[$j]['result_text_line']; ?></p>
								<div class="top-buffer3"></div>								
								<?php 
							} ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- RESULTS SECTION ENDS -->

		<?php
		/* GET THE BOOK YOUR MINDSET SECTION CUSTOM FIELD DETAILS */		
		$mindset_highlight_title = get_field("mindset_highlight_title",$the_ID);
		$button_text = get_field("button_text",$the_ID);
		$button_link = get_field("button_link",$the_ID);
		$mindset_description_repeater = get_field("mindset_description",$the_ID);		
		/* --- ENDS --- */
		?>
		<!-- BOOK YOUR MINDSET SECTION STARTS -->
		<section>
			<div class="cseaction6">
				<div class="container">
					<div class="csec6">
						<div class="top-buffer3"></div>
						<div class="top-buffer1"></div>
						<p class="s21"><?php echo $mindset_highlight_title; ?></p>
						<div class="top-buffer3"></div>
						<div class="top-buffer3"></div>

						<?php 
						for($m=0;$m<count($mindset_description_repeater);$m++) {
							?>								
							<p><?php echo $mindset_description_repeater[$m]['mindset_description_text']; ?></p>
							<div class="top-buffer3"></div>								
							<?php 
						} ?>

						<div class="top-buffer3"></div>
						<div class="button">
							<div class="sub-button">
								<img src="<?php echo get_template_directory_uri(); ?>/images/left.png" class="imge1"/>
								<a class="bookaride" href="<?php echo $button_link; ?>" target="blank"><?php echo $button_text; ?></a>
								<img src="<?php echo get_template_directory_uri(); ?>/images/right.png" class="imge2"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BOOK YOUR MINDSET SECTION ENDS -->

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>