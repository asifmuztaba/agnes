<?php
/*

  Template Name: Blank Page

 */

if(is_page_template('template-blank.php'))
{
    get_header('manifest');
}
else
{
    get_header();
}
?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
<?php endif; ?>
<?php 
if(is_page_template('template-blank.php'))
{
?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	    var height = $(window).height();
	    $('iframe').css('height', height);
	});
</script>
<?php
    get_footer('manifest');
}
else
{
    get_footer();
}
?>