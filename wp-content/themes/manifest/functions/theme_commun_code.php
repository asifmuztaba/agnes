<?php

function new_excerpt_length($length) {
    return 50;
}

add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($more) {
    return '';
}

add_filter('excerpt_more', 'new_excerpt_more');

if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}
/*
add_action('admin_enqueue_scripts', 'mw_enqueue_color_picker');

function mw_enqueue_color_picker($hook_suffix) {
    // first check that $hook_suffix is appropriate for your admin page
    wp_enqueue_style('wp-color-picker');
    wp_enqueue_script('my-script-handle', plugins_url('my-script.js', __FILE__), array('wp-color-picker'), false, true);
}
*/
function load_admin_things() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');
}

add_action('admin_enqueue_scripts', 'load_admin_things');

function my_login_logo() {
    ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_option('logo_image'); ?>);
            background-size: 100% auto;
            height: 90px;
            max-width: 100%;
            width: 79%;
        }
        body{
            background: rgba(255,93,177,1)  !important;
            background: -moz-linear-gradient(-45deg, rgba(255,93,177,1) 0%, rgba(224,5,68,1) 100%) !important;
            background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(255,93,177,1)), color-stop(100%, rgba(224,5,68,1))) !important;
            background: -webkit-linear-gradient(-45deg, rgba(255,93,177,1) 0%, rgba(224,5,68,1) 100%) !important;
            background: -o-linear-gradient(-45deg, rgba(255,93,177,1) 0%, rgba(224,5,68,1) 100%) !important;
            background: -ms-linear-gradient(-45deg, rgba(255,93,177,1) 0%, rgba(224,5,68,1) 100%) !important;
            background: linear-gradient(135deg, rgba(255,93,177,1) 0%, rgba(224,5,68,1) 100%) !important;
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5db1', endColorstr='#e00544', GradientType=1 ) !important;
        }
        .login #backtoblog a, .login #nav a {
            color: #fff !important;
            text-decoration: none;
        }
        .login form {
  border-radius: 3px;
}
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'my_login_logo');

function my_login_logo_url() {
    return home_url();
}

add_filter('login_headerurl', 'my_login_logo_url');

function my_login_logo_url_title() {
    return "<img src='" . get_option('logo_image') . "' >";
}

add_filter('login_headertitle', 'my_login_logo_url_title');
/* End Of Changing Login logo */


add_action('init', 'my_out');

function my_out() {
    ob_start();
}

add_theme_support('menus');
?>