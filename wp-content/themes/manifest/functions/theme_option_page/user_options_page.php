<?php
add_action('admin_init', 'register_myuser_settings');

function register_myuser_settings() {
    register_setting('my-own-theme-options-for-user', 'login_page_id');
    register_setting('my-own-theme-options-for-user', 'dash_page_id');  
    register_setting('my-own-theme-options-for-user', 'f_pass_id');   
    register_setting('my-own-theme-options-for-user', 'user_video_id');
    register_setting('my-own-theme-options-for-user', 'user_pdf_id');
    register_setting('my-own-theme-options-for-user', 'user_subsc_id');
    register_setting('my-own-theme-options-for-user', 'user_pay_succ_id');  
    register_setting('my-own-theme-options-for-user', 'user_pay_fail_id');     
    
}

function user_options_page() {
    ?>

    <div class="wrap">
        <h2>Social Links</h2>
        <?php settings_errors(); ?> 
        <form method="post" action="options.php">
            <?php settings_fields('my-own-theme-options-for-user'); ?>
            <?php do_settings_sections('my-own-theme-options-for-user'); ?>
            <?php include('bootstrap_theme_includes.php'); ?>
            <br />
            <div class="row">

                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">User Page setting</h3>
                        </div>
                        <div class="panel-body">
                            <?php
                            $args = array(
                                'posts_per_page' => -1,
                                'orderby' => 'name',
                                'order' => 'ACS',
                                'post_status' => 'publish',
                                'post_type' => 'page',
                                'suppress_filters' => true,
                            );
                            $page_array = get_posts($args);
                            ?>

                            <label for="login_label">Login Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="login_page_id">
                                            <?php $reg_page_val = get_option('login_page_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            

                            <label for="dash_label">Dashboard Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="dash_page_id">
                                            <?php $reg_page_val = get_option('dash_page_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            

                            <label for="f_pass_label">Forgot Password Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="f_pass_id">
                                            <?php $reg_page_val = get_option('f_pass_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            
                            
                            <label for="user_video_id">Video Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="user_video_id">
                                            <?php $reg_page_val = get_option('user_video_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            
                            
                            <label for="user_pdf_id">PDF Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="user_pdf_id">
                                            <?php $reg_page_val = get_option('user_pdf_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            
                            
                            <label for="user_subsc_id">Subscription Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="user_subsc_id">
                                            <?php $reg_page_val = get_option('user_subsc_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            
                            <label for="user_pay_succ_id">PayPal Success Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="user_pay_succ_id">
                                            <?php $reg_page_val = get_option('user_pay_succ_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <label for="user_pay_fail_id">PayPal Fail Page</label>
                            <br />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select class="form-control" name="user_pay_fail_id">
                                            <?php $reg_page_val = get_option('user_pay_fail_id'); ?>
                                            <option value="">Select</option>
                                            <?php
                                            for ($i = 0; $i < count($page_array); $i++) {
                                                ?>
                                            <option <?php if($reg_page_val==$page_array[$i]->ID) {?> selected="selected"<?php } ?> value="<?php echo $page_array[$i]->ID;?>"><?php echo $page_array[$i]->post_title;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            
                        </div>
                    </div>
                </div>

                <?php include 'option_page_sidebar.php'; ?>

            </div>

            <?php submit_button(); ?>

        </form>
    </div>
    <?php
}

/* My Theme Option */
?>
