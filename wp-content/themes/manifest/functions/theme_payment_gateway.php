<?php
/* This is file for adding the payment gateway to the site */



/* Start of adding Paypal Payment Gateway */
$paypal_pages = array("page_slug" => "paypal_options_page",
            "menu_name" => "Paypal Options",
            "create_page" => "no",
            "php_page_name" => get_template_directory() . "/functions/payment/paypal/paypal_options_page.php",
            "is_procted" => "no"
);

/* End of adding Paypal Payment Gateway */


/* custom post type */
add_action('init', 'create_post_type_payment');

function create_post_type_payment() {
    register_post_type('payment', array(
        'labels' => array(
            'name' => __('Payment'),
            'singular_name' => __('Payment'),
            'add_new' => __('Add Payment'),
            'all_items' => __('All Payment'),
            'add_new_item' => __('Add New Payment'),
            'edit_item' => __('Edit Payment'),
            'new_item' => __('New  Payment'),
            'view_item' => __('View Payment'),
            'search_items' => __('Search Payment'),
            'not_found' => __('No Payment'),
            'not_found_in_trash' => __('No Payment found in Trash')
        ),
        'public' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => false,
        'has_archive' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-clipboard',
        'rewrite' => array('slug' => 'payment'),
        'supports' => array('title', 'author')
            )
    );
}

/* end of custom post type */



/* Add custom colum */

function add_payment_columns($columns) {
    unset($columns['date']);
    return array_merge($columns, array(
        'payment_status' => __('Payment Status'),
        'payment_gross' => __('Total Amount'),
        'payment_date' => __('Payment Date'),
    ));
}

add_filter('manage_payment_posts_columns', 'add_payment_columns'); // remeber that "payment" is your custom post type name
/* end of Add custom colum */

/* add data in custom culom */


add_action('manage_posts_custom_column', 'show_payment_column_for_listing_list', 10, 2); // remeber that "payment" is your custom post type name

function show_payment_column_for_listing_list($columns, $post_id) {
    global $typenow;
    if ($typenow == 'payment') {
        switch ($columns) {
            case 'payment_status':
                echo get_post_meta($post_id, 'payment_status', true);
                break;
            case 'payment_gross':
                echo get_post_meta($post_id, 'payment_given', true) . "&nbsp;" . get_post_meta($post_id, 'mc_currency', true);
                break;
            case 'payment_date':
                echo get_post_meta($post_id, 'payment_date', true);
        }
    }
}

/* end of add data in custom culom */



/* Meta Box for payer detil */
add_action("admin_init", "payer_detail");

function payer_detail() {
    add_meta_box("payer_detail_id", "Payer Detail", "payer_detail_out", "payment", "advanced", "high");
}

function payer_detail_out() {
    global $post, $wpdb;
    $payer_email = get_post_meta($post->ID, 'payer_email', true);
    $first_name = get_post_meta($post->ID, 'first_name', true);
    $last_name = get_post_meta($post->ID, 'last_name', true);
    $payer_id = get_post_meta($post->ID, 'payer_id', true);

    $payment_method = get_post_meta($post->ID, 'payment_method');

    if ($payment_method[0] == "jcc") {
        ?>
        <style type="text/css">
            #payer_detail_id , #payer_address_detail_id{
                display:none;		
            }
            #jcc_detail_id{
                display:block;		
            }
        </style>
    <?php } else {
        ?>
        <style type="text/css">
            #payer_detail_id , #payer_address_detail_id{
                display:block;		
            }
        </style>

    <?php } ?>
    <style type="text/css">
        .fulldiv, .fulldiv input[type='text'], .myinputes
        {
            width:100%;
        }
        .fulldiv input[type='button']
        {
            margin-top:5px;

        }
        .fulldiv input[type='text']
        {
            margin-top:5px;
        }
    </style>


    <p>

    <div class="fulldiv">
        <div class="myinputes">
            <table class="repatertab" width="100%" cellpadding="5" cellspacing="5">

                <tr>
                    <td>
                        <label for="first_name">First Name</label><br />
                        <input id="first_name" name="first_name" type="text" value="<?php echo $first_name; ?>" />
                    </td>
                    <td>
                        <label for="last_name">Last Name</label><br />
                        <input id="last_name" name="last_name" type="text" value="<?php echo $last_name; ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="payer_id">Payer ID</label><br />
                        <input id="payer_id" name="payer_id" type="text" value="<?php echo $payer_id; ?>" />
                    </td>
                    <td>
                        <label for="payer_email">Email</label><br />
                        <input id="payer_email" name="payer_email" type="text" value="<?php echo $payer_email; ?>" />
                    </td>
                </tr>

            </table>                 
        </div> 
    </div>         
    </p> 
    <?php
}
add_action('save_post', 'payer_detail_save');

function payer_detail_save($post_id) {
    $post = get_post($post_id);
    if ($post->post_type == 'payment') {
        update_post_meta($post->ID, 'first_name', $_POST['first_name']);
        update_post_meta($post->ID, 'last_name', $_POST['last_name']);
        update_post_meta($post->ID, 'payer_email', $_POST['payer_email']);
    }
}

/* end of Meta Box for payer detil */




/* Meta Box for payer Package */
add_action("admin_init", "payer_package");

function payer_package() {
    add_meta_box("payer_package_id", "Package Detail", "payer_package_out", "payment", "advanced", "high");
}

function payer_package_out() {
    global $post, $wpdb;
    $payment_for = get_post_meta($post->ID, 'payment_for', true);
    ?> 

    <p>

    <div class="fulldiv">
        <div class="myinputes">
            <table class="repatertab" width="100%" cellpadding="5" cellspacing="5">

                <tr>
                    <td>
                        <label for="payment_for">Payment For</label><br />
                        <input id="payment_for" name="payment_for" type="text" value="<?php echo $payment_for; ?>" />
                    </td>
                </tr>
            </table>                 
        </div> 
    </div>         
    </p> 
    <?php
}

add_action('save_post', 'payer_package_save');

function payer_package_save($post_id) {
    $post = get_post($post_id);
    if ($post->post_type == 'payment') {
        update_post_meta($post->ID, 'payment_for', $_POST['payment_for']);
    }
}

/* end of Meta Box for payer Package */





/* Meta Box for payer address */
add_action("admin_init", "payer_address_detail");

function payer_address_detail() {
    add_meta_box("payer_address_detail_id", "Payer Address Detail", "payer_address_detail_out", "payment", "advanced", "high");
}

function payer_address_detail_out() {
    global $post, $wpdb;
    $address_street = get_post_meta($post->ID, 'address_street', true);
    $address_zip = get_post_meta($post->ID, 'address_zip', true);
    $address_city = get_post_meta($post->ID, 'address_city', true);
    $address_state = get_post_meta($post->ID, 'address_state', true);
    $address_country = get_post_meta($post->ID, 'address_country', true);
    $address_country_code = get_post_meta($post->ID, 'address_country_code', true);
    ?>
    <style type="text/css">
        .fulldiv, .fulldiv input[type='text'], .myinputes
        {
            width:100%;
        }
        .fulldiv input[type='button']
        {
            margin-top:5px;

        }
        .fulldiv input[type='text']
        {
            margin-top:5px;
        }
    </style>


    <p>
    <div class="fulldiv">
        <div class="myinputes">
            <table class="repatertab" width="100%" cellpadding="5" cellspacing="5">
                <tr>
                    <td>
                        <label for="address_street">Street</label><br />
                        <input id="address_street" name="address_street" type="text" value="<?php echo $address_street; ?>" />
                    </td>

                    <td>
                        <label for="address_zip">Zip Code</label><br />
                        <input id="address_zip" name="address_zip" type="text" value="<?php echo $address_zip; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="address_city">City</label><br />
                        <input id="address_city" name="address_city" type="text" value="<?php echo $address_city; ?>" />
                    </td>

                    <td>
                        <label for="address_state">State</label><br />
                        <input id="address_state" name="address_state" type="text" value="<?php echo $address_state; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="address_country">Country</label><br />
                        <input id="address_country" name="address_country" type="text" value="<?php echo $address_country; ?>" />
                    </td>

                    <td>
                        <label for="address_country_code">Country Code</label><br />
                        <input id="address_country_code" name="address_country_code" type="text" value="<?php echo $address_country_code; ?>" />
                    </td>
                </tr>

            </table>                 
        </div> 
    </div>         
    </p> 
    <?php
}

add_action('save_post', 'payer_address_detail_save');

function payer_address_detail_save($post_id) {
    $post = get_post($post_id);
    if ($post->post_type == 'payment') {
        update_post_meta($post->ID, 'address_street', $_POST['address_street']);
        update_post_meta($post->ID, 'address_zip', $_POST['address_zip']);
        update_post_meta($post->ID, 'address_city', $_POST['address_city']);
        update_post_meta($post->ID, 'address_state', $_POST['address_state']);
        update_post_meta($post->ID, 'address_country', $_POST['address_country']);
        update_post_meta($post->ID, 'address_country_code', $_POST['address_country_code']);
    }
}

/* end of Meta Box for payer detil */


/* Meta Box for Payment detil */
add_action("admin_init", "payment_detail");

function payment_detail() {
    add_meta_box("payment_detail_id", "Payment Detail", "payment_detail_out", "payment", "advanced", "high");
}

function payment_detail_out() {
    global $post, $wpdb;
    $payment_status = get_post_meta($post->ID, 'payment_status', true);
    $payment_type = get_post_meta($post->ID, 'payment_type', true);
    $payment_given = get_post_meta($post->ID, 'payment_given', true);
    $payment_date = get_post_meta($post->ID, 'payment_date', true);
    $mc_fee = get_post_meta($post->ID, 'mc_fee', true);
    $payment_gross = get_post_meta($post->ID, 'payment_gross', true);
    $payment_currency = get_post_meta($post->ID, 'mc_currency', true);
    $verify_sign = get_post_meta($post->ID, 'verify_sign', true);
    $business = get_post_meta($post->ID, 'business', true);
    $receiver_email = get_post_meta($post->ID, 'receiver_email', true);
    $receiver_id = get_post_meta($post->ID, 'receiver_id', true);
    $payment_given = get_post_meta($post->ID, 'payment_given', true);
    $payment_type = get_post_meta($post->ID, 'payment_type', true);


    $package_cost = get_post_meta($post->ID, 'package_cost', true);
    $total_booking_amount = get_post_meta($post->ID, 'total_booking_amount', true);
    ?>
    <style type="text/css">
        .fulldiv, .fulldiv input[type='text'], .myinputes , .fulldiv select
        {
            width:100%;
        }
        .fulldiv input[type='button']
        {
            margin-top:5px;

        }
        .fulldiv input[type='text']
        {
            margin-top:5px;
        }
    </style>


    <p>
    <div class="fulldiv">
        <div class="myinputes">
            <table class="repatertab" width="100%" cellpadding="5" cellspacing="5">
                <tr>
                    <td>
                        <label for="payment_status">Payment Status</label><br />
                        <input id="payment_status" name="payment_status" type="text" value="<?php echo $payment_status; ?>" />
                    </td>

                    <td>
                        <label for="payment_date">Payment Date</label><br />
                        <input id="payment_date" name="payment_date" type="text" value="<?php echo $payment_date; ?>" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <label for="payment_given">Payment Given</label><br />
                        <input id="payment_given" name="payment_given" type="text" value="<?php echo $payment_given; ?>" style="color:#0074A2;" />
                    </td>
                    <?php
                    $payment_method = get_post_meta($post->ID, 'payment_method');
                    if ($payment_method[0] != "jcc") {
                        ?>
                        <td>
                            <label for="mc_fee">Transaction fee</label><br />
                            <input id="mc_fee" name="mc_fee" type="text" value="<?php echo $mc_fee; ?>" />
                        </td>

                    <?php } ?>

                </tr>

                    <?php
                    $payment_method = get_post_meta($post->ID, 'payment_method');
                    if ($payment_method[0] != "jcc") {
                        ?>
                    <tr>
                        <td colspan="2">
                            <label for="verify_sign">Verify Sign</label><br />
                            <input id="verify_sign" name="verify_sign" type="text" value="<?php echo $verify_sign; ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <label for="business">Receiver Business Name</label><br />
                            <input id="business" name="business" type="text" value="<?php echo $business; ?>" />
                        </td>

                    </tr>

                    <tr>
                        <td>
                            <label for="receiver_id">Receiver ID</label><br />
                            <input id="receiver_id" name="receiver_id" type="text" value="<?php echo $receiver_id; ?>" />
                        </td>
                        <td>
                            <label for="receiver_email">Receiver Email</label><br />
                            <input id="receiver_email" name="receiver_email" type="text" value="<?php echo $receiver_email; ?>" />
                        </td>
                    </tr>
    <?php } ?>



            </table>                 
        </div> 
    </div>         
    </p> 
    <?php
}

add_action('save_post', 'payment_detail_save');

function payment_detail_save($post_id) {
    $post = get_post($post_id);
    if ($post->post_type == 'payment') {
        update_post_meta($post->ID, 'payment_status', $_POST['payment_status']);
        update_post_meta($post->ID, 'payment_type', $_POST['payment_type']);
        update_post_meta($post->ID, 'payment_given', $_POST['payment_given']);
        update_post_meta($post->ID, 'payment_date', $_POST['payment_date']);
        update_post_meta($post->ID, 'mc_fee', $_POST['mc_fee']);
        update_post_meta($post->ID, 'payment_gross', $_POST['payment_gross']);
        update_post_meta($post->ID, 'verify_sign', $_POST['verify_sign']);
        update_post_meta($post->ID, 'business', $_POST['business']);
    }
}

/* end of Meta Box for Payment detil */
?>