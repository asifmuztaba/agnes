<?php
/*
* Template Name: Subconscious Blocks Page
*/
get_header();
?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<div class="top-buffer3"></div>
<div class="top-buffer1"></div>
<div class="container money-mindset-class">
  <div class="row">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-sm-9">
          <?php
          $image = get_field('back_image');
          if( !empty($image) ): ?>
          <div class="banner-top" style="background: rgb(112, 126, 135) url('<?php echo $image['url']; ?>') no-repeat scroll 0 0 / 100% auto;"><?php endif; ?>
            <div class="banner-text">
              <?php
              $substrings = get_field('section_strings');
              ?>
              <div class="title"><b><?php echo $substrings[0]['string_text']; ?></b><br>
              <?php echo $substrings[1]['string_text']; ?></div>
              <p><?php echo $substrings[2]['string_text']; ?></p>
              <p><?php echo $substrings[3]['string_text']; ?></p>
              <?php
              $image = get_field('sign_image');
              if( !empty($image) ): ?>
              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              <?php endif; ?>
            </div>
          </div>
          <div class="top-buffer2"></div>
          <p class="s35"><?php echo get_field('section_title'); ?></p>
          <div class="top-buffer2"></div>
          <div class="b22 metasinglemeta">
            <span class="s22 a22">
              <i class="fa fa-calendar" aria-hidden="true"></i>
              &nbsp; <?php the_time(get_option('date_format')); ?> I
            </span>
          </div>
          <div class="b23 metasinglemeta">
            <span class="a22">
              <i class="fa fa-comment" aria-hidden="true"></i>
              &nbsp; <a href="<?php the_permalink(); ?>#comment" class="go_to_comment">LEAVE A COMMENT</a> I
            </span>
          </div>
          <div class="b24">
            <span class="a22">
              SHARE ON &nbsp;
              <a class="fb-xfbml-parse-ignore" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>&amp;src=sdkpreparse" onclick="javascript:window.open(this.href,
              '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" /></a>
              <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>&amp;tw_p=tweetbutton&amp;url=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
              '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a>
              <a href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $pimage[0]; ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,
              '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/pinterest.png" /></a>
              <a href="mailto:?&subject=<?php echo urlencode(get_the_title()); ?>&body=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
              '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/email.png" /></a>
            </span>
          </div>
          <div class="buffer-top2"></div>
          <div class="sec-desc"><?php echo get_field('content_firstline'); ?></div>
          <div class="section2">
            <div class="qup1">
              <?php
              // check if the repeater field has rows of data
              if( have_rows('blocks') ):
              // loop through the rows of data
              while ( have_rows('blocks') ) : the_row();
              ?>
              <div class="qua">
                <h2><?php the_sub_field('block_title'); ?></h2>
                <p><?php the_sub_field('block_desc'); ?></p>
              </div>
              <?
              endwhile;
              else :
              // no rows found
              endif;
              ?>
            </div>
            <div class="smackdown">
              <div class="col-xs-12 col-sm-7 left-p">
                <p><?php echo get_field('s2_desc'); ?></p>
                <button class="btn btn-smackdown"><?php echo get_field('s2_btn_text'); ?></button>
              </div>
              <div class="col-xs-12 col-sm-5 images-right-p">
                <?php
                $image = get_field('s2_right_image');
                if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
              </div>
            </div>
            <div class="qup1">
              <?php
              // check if the repeater field has rows of data
              if( have_rows('blocks_2') ):
              // loop through the rows of data
              while ( have_rows('blocks_2') ) : the_row();
              ?>
              <div class="qua">
                <h2><?php the_sub_field('block_title'); ?></h2>
                <p><?php the_sub_field('block_desc'); ?></p>
              </div>
              <?
              endwhile;
              else :
              // no rows found
              endif;
              ?>
            </div>
            <div class="prosper">
              <div class="col-xs-12 col-sm-6 images-left-p">
                <?php
                $image = get_field('s4_left_image');
                if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
              </div>
              <div class="col-xs-12 col-sm-6 right-p">
                <p><?php echo get_field('s4_first_disc'); ?></p>
                <button class="btn btn-smackdown"><?php echo get_field('s4_button_text'); ?></button>
                <p><?php echo get_field('s4_first_disc_2'); ?></p>
                <u><?php echo get_field('link_type_text'); ?></u>
                <p>XO</p>
                <?php
                $image = get_field('s4_sign_image');
                if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
              </div>
            </div>
          </div>
          <?php /* <div class="button5">
            $pagelist = get_posts('sort_column=menu_order&sort_order=asc');
            $pages = array();
            foreach ($pagelist as $page) {
            $pages[] += $page->ID;
            }
            $current = array_search(get_the_ID(), $pages);
            $prevID = $pages[$current - 1];
            $nextID = $pages[$current + 1];
            ?>
            <?php if (!empty($prevID)) { ?>
            <a href="<?php echo get_permalink($prevID); ?>" title="<?php echo get_the_title($prevID); ?>"><input value="&lt; Prev" type="button" /></a>
            <?php } else { ?>
            <?php } ?>
            <?php if (!empty($nextID)) { ?>
            <a href="<?php echo get_permalink($nextID); ?>" title="<?php echo get_the_title($nextID); ?>"><input value="Next &gt;" class="button6" type="button" /></a>
            <?php } else { ?>
            <?php } ?>
            <!-- Single Post Navigation Ends -->
          </div> */  ?>
          <div class="top-buffer3"></div>
          <div class="top-buffer2"></div>
          <?php
          if (comments_open() || get_comments_number()) {
          comments_template();
          }
          ?>
          <div class="top-buffer3"></div>
          <div class="top-buffer2"></div>
          <p class="s35 metasinglemeta">You may also like to read</p>
          <div class="top-buffer2"></div>
          <div class="row">
            <div class="row p-right">
              <?php
              $count = 0;
              $args = array(
              'post_type' => 'post',
              'orderby' => 'rand',
              'post_status' => 'publish',
              'category_name' => $catname,
              'post__not_in' => array($post->ID),
              'showposts' => 3,
              );
              $posts = query_posts($args);
              if (have_posts()) : while (have_posts()) : the_post();
              $pimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
              ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 responsive2">
                <a href="<?php the_permalink(); ?>">
                  <?php if ($pimage[0] != "") { ?><img src="<?php echo $pimage[0]; ?>" class="img-responsive" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php } ?>
                  <div class="top-buffer"></div>
                  <p class="s35 a36"><?php the_title(); ?></p>
                </a>
              </div>
              <?php
              $count++;
              endwhile;
              endif;
              wp_reset_query();
              ?>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 textsatting">
          <?php if (is_active_sidebar('blog_sidebar')) { ?>
          <?php dynamic_sidebar('blog_sidebar'); ?>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<div class="top-buffer3"></div>
<div class="top-buffer2"></div>
<?php get_footer(); ?>