<?php
/*
  Template Name: Money Mindset Makeover
 */
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Money Mindset Mentor & Wealth Therapist">
		<meta name="author" content="Agnes Kowalski">
		<meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
		<link rel="shortcut icon" href="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/favicon.ico" type="image/x-icon" />
		<title>Money Mindset Makeover</title>
		<!-- Bootstrap core CSS -->
		<link href="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/css/bootstrap.min.css" rel="stylesheet">
		<link href="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/css/style.css" rel="stylesheet">
		<!--<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">-->
		<script src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/js/jquery-1.12.4.min.js"></script>
		<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
		<script type='text/javascript'>
			(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);
		</script>
		<style type="text/css">
			
		</style>
	</head>
	<body>
		<div class="main_div">
			<!-- thank you start -->
			<section>
				<div class="seaction5">
					<div class="sec6">
						<div class="container">
							<div class="top-buffer3"></div>
							<div class="top-buffer2"></div>
							<div class="ruls col-lg-8 col-md-12 col-sm-12 col-xs-12">
								<div class="rule1 thankyou">
									<h5>Does Your</h5>
									<h2>Money Mindset</h2>
									<h1>need a</h1>
									<h2 class="color-1">Makeover?</h2>
									<div class="top-buffer3"></div>
									<h5>I show you how to fix that in <span> 3 Easy to Apply Steps.</span></h5>
									<div class="top-buffer3"></div>
									<div class="top-buffer3"></div>
									<div class="top-input">
										<div id="mc_embed_signup">
											<form action="//agneskowalski.us10.list-manage.com/subscribe/post?u=64c042f9e172855ef53be4b93&amp;id=d777647e68" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
												<div id="mc_embed_signup_scroll">
													<div class="mc-field-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
														<input type="text" name="FNAME" class="" placeholder="Name" id="mce-FNAME" required />
													</div>
													<div class="mc-field-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
														<input type="email" name="EMAIL" class="required email" placeholder="Email*" id="mce-EMAIL" required />
													</div>
													<div id="mce-responses" class="clear">
														<div class="response" id="mce-error-response" style="display:none"></div>
														<div class="response" id="mce-success-response" style="display:none"></div>
													</div>
													
													<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_64c042f9e172855ef53be4b93_d777647e68" tabindex="-1" value=""></div>
													<div class="clearfix"></div>
													<div class="top-buffer3"></div>
													<div class="sub-button">
														<img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/left.png"><button name="subscribe" id="mc-embedded-subscribe" class="button" type="submit">MAKE OVER MY MONEY MINDSET</button><img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/right.png">
													</div>
													<div class="top-buffer3"></div>
												</div>
											</form>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<section>
				<div class="seaction6">
					<div class="container">
						<div class="top-buffer1"></div>
						<h2>How much does it suck:</h2>	
						<p>Not having the clarity on what mindset work is going to move you forward financially?</p>
						<h3>Lacking wealth consciousness and slipping back into scarcity?</h3>
						<p>Not being in cash flow and abundance on a regular basis? </p>
						<h3>Struggling to believe in yourself and what you do?</h3>
						<h4><i>UMM, ALOT!</i></h4>
						<div class="top-buffer1"></div>
					</div>
				</div>
			</section>
			
			<section>
				<div class="seaction7">
					<div class="sec7">
						<div class="container">	
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<div class="top-buffer3"></div>
								<div class="top-buffer2"></div>
								<p>You don’t have to keep hammering away at money mindset blocks with ineffective approaches, and blindly applying techniques with no idea if they will help you manifest.</p>
								<div class="top-buffer1"></div>
								<p>Listen, if you are not manifesting the money you want RIGHT NOW, then you have money mindset issues…</p>
								<div class="top-buffer1"></div>
								<p>If you apply the practices I show you inside the Money Mindset Makeover (which are the same formulas I use with my 6 and 7 figure earning clients) you will see results in your money mindset.</p>
								<div class="top-buffer1"></div>
								<h5>Imagine not having thoughts that are creating BLOCKS and RESISTANCE to the very things you want in your life like, MONEY? And instead…</h5>
								<div class="top-buffer3"></div>
								<ul>
									<li>Knowing the right questions to ask to pivot your ability to vibrationally attract more income </li>
									<div class="top-buffer1"></div>
									<li>Leveraging your subconscious programming so it works for you not against you</li>
									<div class="top-buffer1"></div>
									<li>Releasing old blocks that keep stopping you from making or holding on to more money</li>
									<div class="top-buffer1"></div>
									<li>Breaking through the struggles that keep you from being able to play on a bigger level</li>
									<div class="top-buffer1"></div>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<section>
				<div class="seaction8">
					<div class="container">	
						<div class="top-buffer3"></div>
						<h2>Here’s what’s going to happen when you click:</h2>
						<div class="top-buffer2"></div>
						<div class="top-input">
							<div id="mc_embed_signup">
								<form action="//agneskowalski.us10.list-manage.com/subscribe/post?u=64c042f9e172855ef53be4b93&amp;id=d777647e68" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
									<div id="mc_embed_signup_scroll">
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
										<div class="mc-field-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
											<input type="text" name="FNAME" class="" id="mce-FNAME" placeholder="Name" required />
										</div>
										<div class="mc-field-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
											<input type="email" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email*" required/>
										</div>
										<div id="mce-responses" class="clear">
											<div class="response" id="mce-error-response" style="display:none"></div>
											<div class="response" id="mce-success-response" style="display:none"></div>
										</div>
										<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_64c042f9e172855ef53be4b93_d777647e68" tabindex="-1" value=""></div>
										<div class="clearfix"></div>
										<div class="top-buffer3"></div>
										<div class="sub-button">
											<img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/left.png"><button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">MAKE OVER MY MONEY MINDSET</button><img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/right.png">
										</div>
										<div class="top-buffer2"></div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<section>
				<div class="seaction9">
					<div class="container">	
						<div class="top-buffer3"></div>
						<ul>
							<li><h2>1</h2>You will be sent 3 Emails over 3 Days that give you an easy to apply 3 Step Process to Making Over your Money Mindset.</li>
							<li><h2>2</h2>You can email me your results like these fine people who have gone through my Money Mindset Makeover:</li>
						</ul>
					</div>
				</div>
			</section>
			
			<section>
				<div class="seaction10">
					<div class="container">	
						<div>
							<div class="top-buffer3"></div>
							<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" align="center">
								<img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/b-top.png">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
						</div>
						<div class="clearfix"></div>
						<div>
							<div class="top-buffer3"></div>
							<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" align="center">
								<img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/b-left.png" class="img-responsive">
							</div>
							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 clearfix img-res" align="center">
								<img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/b-right.png" class="img-responsive">
							</div>
						</div>
						<div class="clearfix"></div>
						<div>
							<div class="top-buffer3"></div>
							<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
							<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" align="center">
								<img src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/images/b-bottom.png">
							</div>
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
						</div>
						<div class="clearfix"></div>
						<div class="top-buffer3"></div>
						
					</div>
				</div>
			</section>
			
			<section>
				<div class="seaction11">
					<div class="container">
						<div class="sec8">	
							<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
								<div class="top-buffer3"></div>
								<div class="top-buffer3"></div>
								<h2>Oh and in case we’ve never met, here’s<br /><span>a little about me</span></h2>
								<div class="top-buffer1"></div>
								<p>I’m Agnes Kowalski and I’m a Money Mindset Mentor. With a psychotherapy background, I have literally seen thousands of clients and thousands of subconscious money patterns over the years so I know how to help you strategize your money beliefs to align with your desires in a way that is practical and actually WORKS. I’ve worked with artists, 6 and 7 figure entrepreneurs, stay at home moms, adult film actors and everyone in between to show them how to clear subconscious patterns and self-actualize the life of their dreams.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<section>
				<div class="seaction12">
					<div class="container">
						<p>© 2017 All rights reserved</p>
					</div>
				</div>
			</section>
		</div>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106731453-1');
</script>
	</body>
</html>