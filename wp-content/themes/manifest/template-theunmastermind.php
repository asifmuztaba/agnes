<?php 
/* 
Template Name: The Un-Mastermind 
*/ 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Money Mindset Mentor & Wealth Therapist">
    <meta name="author" content="Agnes Kowalski">
    <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
    <link rel="shortcut icon" href="http://www.agneskowalski.com/wp-content/themes/theunmastermind/favicon.ico" type="image/x-icon" />
    <title>The Un-Mastermind</title>
    <!-- Bootstrap core CSS -->
    <link href="http://www.agneskowalski.com/wp-content/themes/theunmastermind/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.agneskowalski.com/wp-content/themes/theunmastermind/css/style-sept.css" rel="stylesheet">
	<script src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/js/jquery-1.12.4.min.js"></script>
</head>
<body>

<div class="main_div">
<!-- thank you start -->
	<section>
		<div class="seaction5">
			<div class="sec6">
				<div class="container">
					<div class="top-buffer3"></div>
					<!--<div class="ruls col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>-->
					<div class="ruls col-lg-12 col-md-12 col-sm-12 col-xs-12 res-size">
						<div class="rule1 thankyou">
						    <div>
							   <h2 class="color-1"><span></span><i>The Un-Mastermind</i></h2>
							</div>
							<p class="font-11">For those who are ready to get</p>
							<p class="font-22">Consistent in their Cash Money Consciousness</p>
							<div class="top-buffer2"></div>
							<p class="font-11">The virtual home of <h3>Prosperity Immersion</h3></p>
							<!--<h3><a href="#">Prosperity + Purpose Training</a></h3>-->
							<div class="top-buffer1"></div>
							<p class="font-11">with</p>
							<p class="font-22">Money Mindset Mentor</p>
							<h1 style="color:#fff;">Agnes Kowalski</h1>
							<div class="top-buffer1"></div>
							<!--<h3><a href="#">Mastermind</a></h3>
							<p class="font-1">A small group of like-minded individuals working <br />towards a definite purpose.</p>
							<div class="top-buffer2"></div>
							<h3><a href="#">That Purpose?</a></h3>
							<p class="font-1">Prosperity + Purpose in every area of your life<br /> Breakthroughs via Mindset Mastery</p>
							<div class="top-buffer5"></div>
							<div class="top-buffer3"></div>
							<div class="top-buffer1"></div>-->
							<p class="font-first-fold-title">What is The Un-Mastermind?</p>
<p class="font-first-fold">A small group of individuals working on individual goals.
We are in the same room.</p>
<p class="font-first-fold">Our collective consciousness is building each other up.</p>
<br><p class="font-first-fold" style="font-family: Montserrat-SemiBold !important;">
BUT
</p><br><p class="font-first-fold">
We are focused on our individual financial potential
And mastering money with our subconscious.</p><br>
<p class="font-first-fold">
We are not here to coach each other,
Though we can be supportive.
We are not here to cheerlead,
Though we can wish nothing but the best for our members.</p><br>
<p class="font-first-fold">
We ARE here to witness and learn.
We ARE here to breakthrough our OWN limitations.
We ARE here to stay focused on OUR prize.
							</p><br><br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction6">
			<div class="container">
				<div class="top-buffer2"></div>
				<div class="ruls col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
				<div class="ruls col-lg-8 col-md-12 col-sm-12 col-xs-12">
					<p class="font-1">Because group programs can be distracting and cost us our momentum which we need to be able to hit the targets we desire.</p>
					<h2><i>In a nutshell this is 1:1 coaching in a group setting.</i></h2>
					<p class="font-1">Weekly calls starting October 15th - April 15th, 6 month commitment.</p><br>
					<p class="font-1"><b>Requirements:</b></p>
					<p class="font-1">This is for those who are already making a min. Of $5k/month in their business or have successfully completed PTP</p><br>
					<p class="font-1"><b>Our work:</b></p>
					<p class="font-1">We will be working on the issues that come up for people trying to hit $10-50K months:</p><br>
				<div class="special-box">
					<ul>
						<li>Consistency</li>
						<div class="top-buffer1"></div>
						<li>Blocks around recurring income</li>
						<div class="top-buffer1"></div>
						<li>Blocks around passive income</li>
						<div class="top-buffer1"></div>
						<li>Pricing/charging your worth</li>
						<div class="top-buffer1"></div>
						<li>Money Archetypes that need to be neutralized to achieve sustainable income</li>
						<div class="top-buffer1"></div>
						<li>Deep understanding of the subconscious and how to prepare it for income leaps</li>
					</ul><br>
									<p class="font-new">*Additionally I will be creating custom lessons based on specific issues that come up in the group*</p>
					</div>
				</div>
				<div class="ruls col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
				<div class="clearfix"></div>
				<div class="top-buffer2"></div>
				<p>I show you how to raise the quality of your consciousness in prosperity, wealth and success, which then gives you the ability to change your life 180 or even 360 and to manifest whatever you choose.</p>
				<div class="top-buffer3"></div>
				<div class="top-buffer2"></div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction7">
			<div class="sec7">
				<div class="container">	
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
						<div class="top-buffer5"></div>
						<h3><i>How do I do that?</i></h3>
						<div class="top-buffer2"></div>
						<ul>
							<li>By recognizing where you need to grow.</li>
							<div class="top-buffer2"></div>
							<li>Showing you how to get out of suffering.</li>
							<div class="top-buffer2"></div>
							<li>Illuminating where you need to increase self-responsibility.</li>
							<div class="top-buffer2"></div>
							<li>Amplifying the concepts you need to embody to make the shifts required for your desires.</li>
							<div class="top-buffer2"></div>
							<li>Training you to tune into the reality you need to align with for the level of outcome you want.</li>
							<div class="top-buffer2"></div>
							<li>Teach you how to work through deep level emotions that will release money blocks.</li>
							<div class="top-buffer2"></div>
							<li>Showing you how to navigate your deep subconscious desires that keep you stuck.</li>
							<div class="top-buffer2"></div>
							<li>Showing you all the practical methods of aligning with your desired consciousness of prosperity.</li>
							<div class="top-buffer2"></div>
						</ul>
						<p>By being infinite me.</p>
						<p>Which gives you permission to be infinite you.</p>
						<div class="top-buffer5"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--<section>
	<div class="seaction6">
		<div class="container">
			<div class="top-buffer2"></div>
			<div class="ruls col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
			<div class="ruls col-lg-8 col-md-12 col-sm-12 col-xs-12 no-padding">
				<h2 class="bottom-margin0"><i>this mastermind is no fluff,</i></h2>
				<h2 class="top-margin0"><i>we dive into hardcore mindset topics like:</i></h2>
				<div class="top-buffer2"></div>
				<div class="fluff">
					<ul>
						<li>Cash flow crunch strategies</li>
						<div class="top-buffer1"></div>
						<li>Mindset approaches to doubling/tripling income</li>
						<div class="top-buffer1"></div>
						<li>Blocks to scaling</li>
						<div class="top-buffer1"></div>
						<li>Dream life blocks and alignment</li>
						<div class="top-buffer1"></div>
						<li>Emotional clearing of life long patterns</li>
						<div class="top-buffer1"></div>
						<li>Accountability for goals</li>
						<div class="top-buffer1"></div>
						<li>Worthiness Issues</li>
						<div class="top-buffer1"></div>
						<li>Creating and Keeping Prosperity Habits</li>
					</ul>
				</div>
			</div>
			<div class="ruls col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
			<div class="top-buffer3"></div>
			<div class="top-buffer2"></div>
		</div>
	</div>
    </section>-->
    	<section>
		<div class="seaction8 vertical_dis">
			<div class="container">	
				<div class="top-buffer5"></div>
				<p class="title"><i><u>This client crossed $500k.</u></i></p>
				<div class="top-buffer3"></div>
				<div class="col-sm-12">
				    <div align="center">
					     <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/500K.png"/>
						 <div class="top-buffer2"></div>
					</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction9">
			<div class="container">
				<div class="top-buffer3"></div>
				<h2><i>You next?</i></h2>
				<h3>Oh good. Here’s what you get:</h3>
				<div class="top-buffer2"></div>
				<h4><i>Un-Mastermind</i></h4>
				<div class="top-buffer2"></div>
				<div class="text_center">
					<p class="popins_semi">Top Shelf Level Mastermind <span>$1250/month</span></p>
					<p>* $5-10k/monthly min. income in your business to join</p>
				</div>
				<div class="top-buffer2"></div>
				<ul>
					<li>1 small group coaching call per week with each person getting customized laser coaching and homework on each call</li>
					<div class="top-buffer2"></div>
					<li>Support inside our private FB Community</li>
					<div class="top-buffer2"></div>
					<li>Unlimited access to Permission to Prosper trainings and mindset modules </li>
					<div class="top-buffer2"></div>
				</ul>
				<div class="top-buffer2"></div>
				<!--<div class="bottom-btn">
					<div class="sub-button">
						<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XHRJUBWMKNZUL" target="_blank"> <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/left.png"><button type="submit">JOIN HERE</button><img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/right.png"></a>
					</div>
				</div>-->
				<div class="col-sm-12" align="center">
				<p class="popins_semi"><strike>Regular price is $1500/month or $8000 upfront.</strike></p>
				<p class="popins_semi">Early Bird is $1250/month or $6500 upfront until Oct 10.</p>
				<div class="bottom-btn">
					<div class="sub-button">
						<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VE8F2N2SFBPZS" target="_blank"> <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/left.png"><button type="submit">JOIN HERE</button><img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/right.png"></a>
					</div>
				</div>
				<div class="top-buffer3"></div>

				<p class="popins_semi">6 payments x $1250:</p>
				<div class="bottom-btn">
					<div class="sub-button">
						<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E5LKVVEXDJM88" target="_blank"> <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/left.png"><button type="submit">JOIN HERE</button><img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/right.png"></a>
					</div>
				</div>				<div class="top-buffer3"></div>
				<div class="top-buffer3"></div>

				</div>
				<div class="top-buffer3"></div>
			</div>
		</div>
			</div>
		</div>
	</section>

	<section>
		<div class="seaction8 vertical_dis">
			<div class="container">	
				<div class="top-buffer5"></div>
				<p class="title"><i>Mastermind Clients have this to say about the high level accountability and mentorship:</i></p>
				<div class="top-buffer3"></div>
				<div class="col-sm-4">
				    <div class="img_class rigt">
					     <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/keisha.png" class="img-responsive"/>
						 <div class="top-buffer2"></div>
						 <p class="img_title"><i>Keisha Dixon</i></p>
						 <p class="img_dis">Founder, A Well Experience</p>
						 <div class="link">
						     <a href="https://awellexperience.me/" target="_blank"><u>www.awellexperience.me</u></a>
						 </div>
					</div>
				</div>
				<div class="col-sm-8">
				   <div class="ruls forsapn">
						<p>Taking Agnes' Money Mindset Moguls Mastermind has been lifechanging!  Since joining her mastermind 3 months ago I have:</p>
						<div class="top-buffer2"></div>
						<div class="part">
							<span>1.</span><p>Manifested a historical townhouse in the magnificant city of Brooklyn!</p>
							<span>2.</span><p>Reduced my credit card debt by $2500 monthly!</p>
							<span>3.</span><p>Grown my mlm team to 36 leaders and they are leaders!</p>
							<span>4.</span><p>Received $1476 that has been looking for me for 19 years!</p>
							<span>5.</span><p>Increased the number of clients I mentor!</p>
							<span>6.</span><p>Dialed in on my niche mentorship market!</p>
							<span>7.</span><p>Created my 1:1 and online mentorship programs!</p>
						</div>
						<div class="top-buffer2"></div>
						<p>Those are the tangibles....you know else has also happenend, what's actually even more magical and yet not as measurable?  Agnes has held a space for me to:</p> 
						<div class="top-buffer2"></div>
						
						<div class="part">
							<span>1.</span><p>Increase my sense of value!</p>
							<span>2.</span><p>Find my voice!</p>
							<span>3.</span><p>Be kind to myself!</p>
							<span>4.</span><p>Create a new career path!</p>
							<span>5.</span><p>Come out from behind the 'invisbility' of parnterships that were not mutually beneficial!</p>
							<span>6.</span><p>Reduce the undercurrent of anxiety surrounded around my money!</p>
							<span>7.</span><p>Open my heart to receiving!</p>
						</div>
						<div class="top-buffer2"></div>
						<p>I remember when I first joined the mastermind I shared with Agnes that I was scared, scared to join, scared to take the leap of faith, scared of the unknown...3 months later the only fear I have is thinking where would I be if I hadn't joined!</p>
						<div class="top-buffer5"></div>
						<div class="top-buffer2"></div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	
	
	<section>
	   <div class="director vertical_dis">
			<div class="container">	
							
				<div class="col-sm-8">
				   <div class="top-buffer3 none"></div>	
				   <div class="ruls">				
                      <div class="con_pre"> 
						<div class="pera-center padd">				   
							<p>I joined the Money Mindset Mastermind, because while I considered myself a fairly successful business person, I was still having to borrow money or live off credit cards for those "off" months, where I hadn't planned properly or was "coasting" on a big job I'd been paid for. </p>
							<div class="top-buffer2"></div>
							<p>I also knew that mentally, I wasn't thinking prosperous thoughts as consistently as I knew I was capable of. </p>
							<div class="top-buffer2"></div>
							<p>Within 3 months of Money Mindset training I changed how I think about money and literally quadrupled my income!</p>
						</div>
					  </div>	
					</div>
				</div>
				<div class="col-sm-4">
				   <div class="top-buffer3"></div>
				    <div class="img_class rigt">
					     <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/roberta.png" class="img-responsive"/>
						 <div class="top-buffer2"></div>
						 <p class="img_title"><i>Roberta Munroe</i></p>
						 <p class="img_dis"><i>Writer, Director and Film Producer, Captivated Artist Productions Inc.</i></p>
						 <div class="link">
						     <a href="http://captivatedinc.com/" target="_blank"><u>www.captivatedinc.com</u></a>
						 </div>
						 <div class="top-buffer3"></div>
					</div>
				</div>
			   </div>
			</div>
	</section>
	
	<section> 
	    <div class="vertical_dis">
			<div class="container">	
				<div class="top-buffer3"></div>			
				<div class="col-sm-6">
					     <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/vaniyn.png" class="img-responsive"  style="margin:auto;"/>
						 <div class="top-buffer3"></div>
				</div>
				<div class="col-sm-6">
				   <div class="ruls">
					   <div class="con_pres"> 
							<div class="pera-center">
								<p style="text-align:center;">A client got her own show on Footprints TV within the first 2 months of working together</p>
								<div class="top-buffer2"></div>
								<p class="img_title"><i>DARVINY</i></p>
								 <div class="link">
									 <a href="http://darviny.com/" target="_blank"><u>www.darviny.com</u></a>
								 </div>
							</div>
						</div>
					</div>
				</div>
			   </div>
			</div>
	</section>
	
	
<section>
	   <div class="director vertical_dis">
			<div class="container">	
				<div class="col-sm-8">	
				   <div class="ruls">
                     <div class="con_pre"> 
						<div class="pera-center">				   
						   <p style="padding-right:50px;">I launched my program- Restore Your Core- in Jan.2016. Now, I sell about 1-3 per day. Agnes guided me from start to end with this project. I was hoping that this program would sell well but I had a mediocre following at the time. Not only has the program taken off but my whole business has grown to multiple 6 Figures. Agnes is an invaluable element in my business. There is no way I could have, would have been this successful without her. </p>
						 </div>
					  </div>
					</div>
				</div>
				<div class="col-sm-4">
				   <div class="top-buffer3"></div>	
				    <div class="img_class rigt">
					     <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/lauren.png" class="img-responsive"/>
						 <div class="top-buffer2"></div>
						 <p class="img_title"><i>Lauren Ohayon</i></p>
						 <div class="link">
						     <a href="https://laurenohayon.com/" target="_blank"><u>www.laurenohayon.com</u></a>
						 </div>
						 <div class="top-buffer3"></div>
					</div>
				</div>
			   </div>
			</div>
	</section>
	
	
	<section>
	    <div class="vertical_dis">
			<div class="container">						
				<div class="col-sm-4">
				   <div class="top-buffer3"></div>
				    <div class="img_class">
					     <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/brea.png" class="img-responsive"/>
						 <div class="top-buffer2"></div>
						 <p class="img_title"><i>Brea Johnson</i></p>
						 <div class="link">
						     <a href="http://www.heartandbonesyoga.com/" target="_blank"><u>www.heartandbonesyoga.com</u></a>
						 </div>
						 <div class="top-buffer3"></div>
					</div>
				</div>
				<div class="col-sm-8">
				   <div class="ruls">
                     <div class="con_pre"> 
						<div class="pera-center">				   
						   <p>OMG. 40 people signed up. And actually likely two more as I'm working on some technical difficulties with one person and waiting to hear back from another. But there are officially 40 people!!! At $900 a pop! (though half did a payment plan, but it's all the same!)</p>
						   <div class="top-buffer2"></div>
						   <p>Thanks so much for our work together... this has all been magic and I feel like I'm playing with the matrix :) THANK-YOU!</p>
						 </div>
					  </div>
					</div>
				</div>
			   </div>
		 </div>
	</section>
	
	
	<section>
	   <div class="director vertical_dis">
			<div class="container">	
				<div class="col-sm-8">
				   <div class="ruls">
                     <div class="con_pre"> 
						<div class="pera-center">				   
						   <p style="padding-right:50px;">... and $8500 has basically fallen out of the sky since Monday!!! Are you kidding me? Dormant leads, referrals coming back to life after not referring anyone for months, brand new leads appearing out of nowhere... magic my friend, magic. Thank you thank you thank you.</p>
						 </div>
					  </div>
					</div>
				</div>
				<div class="col-sm-4">
				    <div class="top-buffer3"></div>	
				    <div class="img_class rigt">
					     <img src="http://www.agneskowalski.com/wp-content/themes/theunmastermind/images/mishelle.png" class="img-responsive"/>
						 <div class="top-buffer2"></div>
						 <p class="img_title"><i>Michelle Warner</i></p>
						 <div class="link">
						     <a href="http://themichellewarner.com/" target="_blank"><u>www.themichellewarner.com</u></a>
						 </div>
						 <div class="top-buffer3"></div>
					</div>
				</div>
			   </div>
			</div>
	</section>	
	
	<section>
		<div class="seaction10">
			<div class="container">
				<p>© 2018 All rights reserved</p>
			</div>
		</div>
	</section>

</div>
<style>
@media (max-width:1350px){
 .sec6{background-image:none; min-height:auto;}
 .res-size{width:100%;}
}
@media (max-width:1199px){
 .sec7{background-image:none; min-height:auto;}
}
</style>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106731453-1');
</script>
</body>
</html>