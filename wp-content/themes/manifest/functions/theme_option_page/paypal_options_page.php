<?php
add_action('admin_init', 'register_paypal_settings');

function register_paypal_settings() {
    register_setting('my-own-theme-options-for-paypal', 'paypal_mode');
    register_setting('my-own-theme-options-for-paypal', 'paypal_merchant_email');
    register_setting('my-own-theme-options-for-paypal', 'paypal_currency');
    register_setting('my-own-theme-options-for-paypal', 'paypal_success_page');
    register_setting('my-own-theme-options-for-paypal', 'paypal_fail_page');

    register_setting('my-own-theme-options-for-paypal', 'app_prmt_prdc_prc');
    register_setting('my-own-theme-options-for-paypal', 'sch_pro_reg_val');
    register_setting('my-own-theme-options-for-paypal', 'sch_pro_reg_expire');

    register_setting('my-own-theme-options-for-paypal', 'paypal_sch_sign_succ');
    register_setting('my-own-theme-options-for-paypal', 'paypal_sch_sign_fail');

    register_setting('my-own-theme-options-for-paypal', 'paypal_book_succ');
    register_setting('my-own-theme-options-for-paypal', 'paypal_book_fail');

    register_setting('my-own-theme-options-for-paypal', 'paypal_produce_fail');
    register_setting('my-own-theme-options-for-paypal', 'paypal_produce_succ');
}

function paypal_options_page() {
    ?>

    <div class="wrap">
        <h2>Paypal Setting</h2>
        <?php settings_errors(); ?> 
        <form method="post" action="options.php">
            <?php settings_fields('my-own-theme-options-for-paypal'); ?>
            <?php do_settings_sections('my-own-theme-options-for-paypal'); ?>
            <?php include(get_template_directory() . '/functions/theme_option_page/bootstrap_theme_includes.php'); ?>
            <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
            <br />
            <style type="text/css">
                a{
                    outline: none !important;
                }
                a:focus{
                    box-shadow: none !important;
                }
                #wpfooter {
                    position: relative;
                }
            </style>
            <div class="row">

                <div class="col-md-8">




                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Paypal Details</h3>
                        </div>
                        <div class="panel-body">

                            <label for="paypal_mode">Payment Mode</label>

                            <br />

                            <label for="demo_mode"> 
                                <input id="demo_mode" type="radio" value="sandbox" name="paypal_mode" class="form-control" <?php if (get_option('paypal_mode') == "sandbox") { ?> checked="checked" <?Php } ?> /> Sandbox
                            </label>
                            &nbsp;&nbsp;&nbsp;
                            <label for="live_mode"> 
                                <input id="live_mode" type="radio" value="live" name="paypal_mode" class="form-control" <?php if (get_option('paypal_mode') == "live") { ?> checked="checked" <?Php } ?> /> Live
                            </label>

                            <br />
                            <br />

                            <div class="paypal_merchant_email_div" <?php if (get_option('paypal_mode') == "sandbox") { ?> style="display: none;" <?Php } ?> >

                                <label for="paypal_merchant_email">Merchant Email</label>
                                <input id="paypal_merchant_email" type="text" name="paypal_merchant_email" value="<?php echo get_option('paypal_merchant_email'); ?>" class="form-control" />

                                <br />
                            </div>


                            <div class="paypal_currency_div" <?php if (get_option('paypal_mode') == "sandbox") { ?> style="display: none;" <?Php } ?> >

                                <label for="paypal_currency">Currency Code</label>
                                <input id="paypal_currency" type="text" name="paypal_currency" value="<?php echo get_option('paypal_currency'); ?>" class="form-control" />

                                <br />
                            </div>

                            <script type="text/javascript">
                                jQuery(document).ready(function ($) {
                                    $("input[name=paypal_mode]:radio").change(function () {
                                        if ($("#demo_mode").attr("checked")) {
                                            $(".paypal_merchant_email_div").slideUp("slow");
                                            $(".paypal_currency_div").slideUp("slow");
                                        } else {
                                            $(".paypal_merchant_email_div").slideDown("slow");
                                            $(".paypal_currency_div").slideDown("slow");
                                        }
                                    });
                                });
                            </script>

                        </div>
                    </div>
                    
                <?php submit_button(); ?>
                </div>

                <?php include get_template_directory() . '/functions/theme_option_page/option_page_sidebar.php'; ?>



        </form>
    </div>
    <?php
}

/* My Theme Option */
?>
