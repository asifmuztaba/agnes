<?php
/*
 * Template Name: Contact Page
 *
 */
get_header();
?>

<?php if (have_posts()) : ?>  
    <?php while (have_posts()) : the_post(); ?>

        <?php 
        /* GET THE WORK WITH ME SECTION CUSTOM FIELD DETAILS */
        $the_ID = get_the_ID();
        /* GET INTRODUCTION SECTION CUSTOM FIELD DETAILS */
        $contact_form_shortcode = get_field("contact_form_shortcode",$the_ID);
        ?>

        <!-- CONTACT FORM SECTION STARTS -->
        <section>
            <div class="m-seaction">
                <div class="main-sec">
                    <div class="container">
                        <div class="mhed">
                        	<div class="top-buffer2"></div>
                        	<p class="s76" style="color:#c8485f;"><?php echo get_field("introduction_title_author",5); ?></p>
                            <p class="s40"><?php the_title(); ?></p>
                            <div class="top-buffer2"></div>                        
                        </div>
                        <div class="mbody">
                            <div class="row">
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-8">
                                    <?php echo do_shortcode($contact_form_shortcode); ?>
                                <div class="top-buffer2"></div>
                                </div>
                                <div class="col-sm-2">&nbsp;</div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </section>
        <!-- CONTACT FORM SECTION ENDS -->


    <?php endwhile; ?>
<?php else: ?> 
    <div class="error"><?php _e('Oops ! Contact Page not Found !'); ?></div>
<?php endif; ?>

<?php get_footer(); ?>