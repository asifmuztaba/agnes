<?php
/*
  Template Name: Work With Me - Waitlist Thank you page
 */
?>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="Agnes Kowalski">
		<title>Agnes Kowalski</title>
		<link href="http://www.agneskowalski.com/wp-content/themes/workwithmewaitlist/css/bootstrap.min.css" rel="stylesheet">
		<link href="http://www.agneskowalski.com/wp-content/themes/workwithmewaitlist/css/bootstrap-theme.min.css" rel="stylesheet">
		<link href="http://www.agneskowalski.com/wp-content/themes/workwithmewaitlist/css/style.css" rel="stylesheet">
	</head>
	<body>
		<section class="thankyou">
			<div class="container">
			    <div class="col-md-6 col-sm-offset-5">
					<div class="thanx-head text-center">
						<h2>Thank you for signing up, I will contact you as soon as there is a free spot!</h2>
					</div>
				</div>
			</div>
		</section>
		<footer>
		    <p>© 2018 All rights reserved</p>
		</footer>
	    <script src="http://www.agneskowalski.com/wp-content/themes/workwithmewaitlist/js/jquery.min.js"></script>
		<script src="http://www.agneskowalski.com/wp-content/themes/workwithmewaitlist/js/bootstrap.min.js"></script>
	</body>
</html>