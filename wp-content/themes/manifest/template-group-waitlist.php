<?php
/* 
Template Name: Group Program Waitlist
*/
?>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="5 Day Laws of Prosperity Training">
		<meta name="author" content="Agnes Kowalski">
        <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
		<title>Group Program Waitlist - Thank You</title>
		<!-- Bootstrap core CSS -->
		<link href="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/waitlist-thank-you/css/bootstrap.min.css" rel="stylesheet">
		<link href="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/waitlist-thank-you/css/bootstrap-theme.min.css" rel="stylesheet">
		<link href="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/waitlist-thank-you/css/style.css" rel="stylesheet">
		<script src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/waitlist-thank-you/js/jquery.min.js"></script>
		<script src="http://www.agneskowalski.com/wp-content/themes/permissiontoprosper/waitlist-thank-you/js/bootstrap.min.js"></script>
		<style>
			.s20 {
			font-size:20px;
			font-family: "Roboto";
			}
		</style>
	</head>
	<body>
		
		<div class="section">
			<div class="main-bg">
				<div class="container">
					<div class="details">
					    <div class="top-buffer140"></div>
						<div class="top-buffer5"></div>
						<div class="top-buffer5"></div>
						<div class="top-buffer3"></div>
						<p class="s38">Thank you for joining the</p>
						<p class="s38"></p><h1><span>Group Program Waitlist!</span></h1>
						<div class="top-buffer1"></div>
						<div class="top-buffer5"></div>
                        <p class="s38">See you soon!</p>
						<div class="top-buffer5"></div>
						<div class="top-buffer5"></div>
						<div class="top-buffer3"></div>
						<div class="signup-form col-sm-10 col-sm-offset-1">
						</div>
					</div>
					<div class="bottom-buffer"></div>
				</div>
				
			</div>
			<footer>
				<div class="container">
					<div class="footer">
						<p>© 2018 All rights reserved</p>
					</div>
				</div>
			</footer>
			
		</div>
	</body>
</html>