<?php
/*
Template Name: Clients Page
*/
get_header();
?>

<?php //if (have_posts()) : ?>
	<?php //while (have_posts()) : the_post(); ?>
		<?php
		/* GET CLIENT LISTS */
		//$clients_args = array(
		//	'posts_per_page' => -1,
		//	'post_type' => 'clients',
		//	'orderby'   => 'menu_order',
		//	'order'     => 'ASC',
		//	'post_status' => 'publish',
		//	'suppress_filters' => true
		//	);
		//$clients_array = get_posts($clients_args);
		/* GET Custom Fields For CLIENTS PAGE */
		//$the_ID = get_the_ID();
		//$clients_page_title = get_field("clients_page_title",$the_ID);
		//$pattern = "=^<p>(.*)</p>$=i";
		//preg_match($pattern, $clients_page_title, $clients_page_title_final);
		?>

		<!--<section>
			<div class="c-seaction">
				<div class="container">
					<div class="sec0">
						<div class="top-buffer3"></div>
						<div class="ched">
							<p class="s40"><?php //echo $clients_page_title_final[1]; ?></p>
						</div>

						<div class="cbody">
							<?php
							//$i=1;
							//foreach ($clients_array as $clients_value) {
							//	if($i%2 == 0) {
								//	$pull = ' pull-right ';
								//	$buffer = ' buffer ';
								//} else {
								//	$pull = ' ';
								//	$buffer = ' buffer1 ';
								//}
								//$designation = get_field("designation",$clients_value->ID);
								//$attr = array(
								//	'alt' => trim(strip_tags($clients_value->post_title)),
								//	'title' => trim(strip_tags($clients_value->post_title)),
								//	);								
									?>

									<div class="top-buffer2"></div>
									<div class="client">
										<div class="col-sm-4 col-md-3 <?php //echo $pull; ?>">
											<div class="<?php //echo $buffer; ?>"></div>
											<div class="img">
												<?php 
												/* echo get_the_post_thumbnail($clients_value->ID, 'full', $attr); */												
												//$client_image_thumb = wp_get_attachment_image_src(get_post_thumbnail_id($clients_value->ID), 'full');
												?>
												<img src="<?php //echo $client_image_thumb[0]; ?>" alt="<?php //echo trim(strip_tags($clients_value->post_title)); ?>" title="<?php //echo trim(strip_tags($clients_value->post_title)); ?>" />
											</div>
										</div>
										<div class="col-sm-8 col-md-9">
											<div class="client-dis">
												<div class="top-buffer3"></div>
												<p><?php //echo  $clients_value->post_content; ?></p>
												<div class="top-buffer1"></div>
												<p class="s22"><?php //echo $clients_value->post_title; ?> <?php //if($designation != '') { echo ' - '.$designation; } ?></p>
											</div>
										</div>
									</div>

									<?php
									//$i++;
								//}
								?>
								<div class="top-buffer2"></div>
								<div class="top-buffer2"></div>
								<div class="top-buffer2"></div>
							</div>

						</div>


					</div>
				</div>
			</section>-->
			<!-- START Important -->
		<section>
			<div class="container">
				<div class="post_image">
					<div class="top-buffer3"></div>
					<div class="ched">
						<p class="s40">What <strong>My Clients</strong> Say…</p>
					</div>
					<div class="top-buffer2"></div>
					<div class="col-lg-offset-1 col-md-5">
						<div class="post_img res text-center">
							<a href="http://www.michellewarner.com/"><img src="http://www.agneskowalski.com/wp-content/themes/manifest/images/clientlove/1.png"></a>
						</div>
					</div>
					<div class="col-md-5">
						<div class="post_img res text-center">
							<a href="http://www.pinkandgrey.ca/"> <img src="http://www.agneskowalski.com/wp-content/themes/manifest/images/clientlove/2.png"></a>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6">
						<div class="post_img res text-center">
							<a href="#"> <img src="http://www.agneskowalski.com/wp-content/themes/manifest/images/clientlove/3.png"></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="post_img res text-center">
							<a href="#"> <img src="http://www.agneskowalski.com/wp-content/themes/manifest/images/clientlove/4.png"></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="post_img res text-center">
							<a href="#"> <img src="http://www.agneskowalski.com/wp-content/themes/manifest/images/clientlove/5.png"> </a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="post_img res text-center">
							<a href="https://sarahnegus.com"> <img src="http://www.agneskowalski.com/wp-content/themes/manifest/images/clientlove/6.png"> </a>
						</div>
					</div>
					<div style="display:inline-block;margin-bottom:40px;width:100%;"></div>
				</div>
			</div>
			<div class="container">
				<div class="col-sm-12">
					<!--<iframe width="100%" height="500px" src="https://www.youtube.com/embed/OQ1yre7efUk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>-->
					   <div class="post_img res text-center">
							<!--<a href="https://sarahnegus.com"> <img src="http://www.agneskowalski.com/wp-content/themes/manifest/images/clientlove/6.png"> </a>-->
                    			<img src="http://www.agneskowalski.com/wp-content/uploads/2018/10/Client-Love-New.jpg">
						</div>

				</div>
			</div>
			<div class="clearfix top-buffer3"></div>
			<div class="clearfix top-buffer2"></div>
		</section>
		<!-- END Important -->

		<?php //endwhile; ?>
	<?php //endif; ?>

	<?php get_footer(); ?>