<?php

/*

  Template Name: 31 Prosperity Prompts

 */

?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/favicon.ico" type="image/x-icon" />
    <title>31 Prosperity Prompts - Agnes Kowalski</title>
    <!-- Bootstrap core CSS -->
    <link href="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/css/style.css" rel="stylesheet">
	<script src="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/js/jquery-1.12.4.min.js"></script>

</head>
<body>
<div class="main_div">

<section>
     <div class="sec1">
          <div class="backimg clearfix">
				<div class="container">
					 <div class="col-md-8">
						  <div class="row">
							   <div class="header_dis clearfix">
							       <div class="top-buffer5"></div>
									<h1 class="head_title"><span></span>31 Prosperity<br> Increasing Prompts</h1>
									<h2>Lifetime Access Now for only <span>$31</span><h2>
									<div class="clearfix"></div>
									<div class="top-buffer2"></div>
									<?php 
                        $template_url = get_template_directory_uri();
                        $ipn = $template_url . "/functions/payment/paypal/31property_ipn.php";
                        $paypal_succ_page = get_option('user_pay_succ_id');
                        $paypal_succ_page = esc_url("http://www.agneskowalski.com/payment-successful-31-prosperity/");
                        $paypal_fail_page = get_option('user_pay_fail_id');
                        $paypal_fail_page = esc_url(get_permalink($paypal_fail_page));
                        $paypal_currency = get_option('paypal_currency');
                        if (get_option('paypal_mode') == "live") { /* if the payment mode is live */
                            $paypal_url = "www.paypal.com";
                            $paypal_merchant_email = get_option('paypal_merchant_email');
                        } else {
                            $paypal_url = "www.sandbox.paypal.com";
                            $paypal_merchant_email = "sandeep_biz@gmail123.com";
                            $paypal_currency = "USD";
                        } /* Start of making the url for the Paypal with the given argument. */ $main_url = "https://" . $paypal_url . "/cgi-bin/webscr";
                        $porgram_id = "1027";
                        $program_price = get_field("program_price", $porgram_id);
                        $program_title = get_the_title($porgram_id);
                        $paypal_url = add_query_arg(
                                array('business' => $paypal_merchant_email, 
                                    'cmd' => '_xclick', 
                                    'item_name' => $program_title, 
                                    'item_number' => "", 
                                    'amount' => $program_price, 
                                    'custom' => "31property", 
                                    'currency_code' => $paypal_currency, 
                                    'tax' => 0, 
                                    'notify_url' => $ipn, 
                                    'cancel_return' => $paypal_fail_page, 
                                    'return' => $paypal_succ_page
                                ), $main_url);
                         ?>
                                                                        
									<div class="button_start">
										<img src="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/images/left-arrow.png"/>
										<!-- <a type="submit" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GHU7QG2SJ44ZE" target="_blank" class="btn" name="start">START SHIFTING YOUR SCARCITY TO SUCCESS</a> -->
                                                                                <a type="submit" href="<?php echo $paypal_url; ?>" target="_blank" class="btn" name="start">START SHIFTING YOUR SCARCITY TO SUCCESS</a>
										<img src="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/images/right-arrow.png"/>
									</div>
									
									<div class="clearfix"></div>
									<div class="top-buffer9"></div>
									
									<h2 class="font-24">In this FILLABLE + PRINTABLE PDF Wealth Workbook,</h2>
									<h3 class="margin0"> You will get:</h3>
									
									<div class="clearfix"></div>
									<div class="top-buffer2"></div>
									
									<div class="clearfix"></div>
									<div class="row">
											<div class="col-sm-3">
												<div class="block">
													 <h3>CLARITY</h3>
													 <p>ON YOUR MONEY BLOCKS</p>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="block">
													 <h3>CONFIDENCE	</h3>
													 <p>IN THINKING AND ACTING FROM A PROSPEROUS MINDSET</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="block">
													 <h3>COURAGE</h3>
													 <p>TO SHIFT YOUR MONEY STORY</p>
												</div>
											</div>
									</div>
									<div class="clearfix"></div>
									<div class="top-buffer5"></div>
							   </div>
						  </div>
					 </div>
				 </div>
		 </div>
	 </div>
</section>

<div class="clearfix"></div>



<section class="text-center clearfix">
   <div class="wealth_title">
      <div class="top-buffer2"></div>
        <div class="container">
		      <h4>CHANGE YOUR THOUGHTS, CHANGE YOUR WEALTH</h4>
			  <p class="font-24"> Immediately <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GHU7QG2SJ44ZE" target="_blank">Start Shifting out of Scarcity and into Prosperity</a> <br> As used regularly by 6 and 7 Figure Earners</p>
			  <h2 class="font-24">Results like this...</h2>
		</div>
	  <div class="top-buffer2"></div>
   </div>
</section>

<section class="gray text-center sm-size">
     <div class="container">
	    <div class="col-sm-8">
		     <div class="top-buffer5"></div>
		     <div class="user_dis">
			      <p class="font-20">Doing the exercises you gave me is going to CHANGE MY LIFE.</p>
				  <h4 class="font-24">Deepa Ramachandran</h4>
				  
				  <p class="font-16">Finance + Money Coach</p>
				  <a class="font-16" href="https://www.acumencpa.com/" target="_blank">www.acumencpa.com</a>
			 </div> 
			 <div class="top-buffer5"></div>
		</div>
	    <div class="col-sm-4">
		    <div class="user_img">
			    <img src="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/images/img2.png"/>
			</div>
		</div>
	 </div>
</section>

<div class="clearfix"></div>
<div class="top-buffer5"></div>
<div class="clearfix"></div>

<section class="gray text-center">
     <div class="container">
	    <div class="col-sm-4">
		    <div class="user_img">
			    <img src="http://www.agneskowalski.com/wp-content/themes/31prosperityprompts/images/img3.png"/>
			</div>
		</div>
		
		 <div class="col-sm-8">
		     <div class="top-buffer2"></div>
		     <div class="user_dis">
			      <p class="font-20">I had to get up out of my bed to journal this thought down (hit me right as I was drifting off) Agnes this exercise right here is worth the price of a year of admission..at the very least.</p>
				  <h4 class="font-24">Keisha Dixon</h4>
				  
				  <p class="font-16">Tapping + Mindset Strategist</p>
				  <a class="font-16" href="https://awellexperience.me/" target="_blank">www.awellexperience.me</a>
			 </div> 
			 <div class="top-buffer2"></div>
		</div>
		
	 </div>
</section>

<div class="clearfix"></div>
<div class="top-buffer5"></div>
<div class="clearfix"></div>

<!--footer-->
<footer>
   <div class="container">
       <div class="footer">
	           <p>© 2018 ALL RIGHTS RESERVED</p>
	   </div>
   </div>
</footer>
<!--end footer-->
</div>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());
  gtag('config', 'UA-106731453-1');
</script>
	</body>
</html>