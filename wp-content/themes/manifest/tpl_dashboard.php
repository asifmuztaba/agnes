<?php
/*
  Template Name: Dashboard
 */

if (!session_id()) {
    session_start();
}

if (is_user_logged_in()) {
    ?>
    <?php
    get_header();
    ?>

    <?php if (have_posts()) : ?>  
        <?php while (have_posts()) : the_post(); ?>

            <!---login---->
            <section class="login-page">
                <div class="container-fluid">
                    <div class="banner_img_div">
                        <style type="text/css">
                            .banner_img_div{
                                display: inline;
                                text-align: center;
                            }
                            .banner_img_div img{
                                max-width: 100%;
                                height: auto;
                                width: auto;
                            }
                        </style>
                        <div class="row">
                            <?php
                            if (has_post_thumbnail()) {
                                the_post_thumbnail();
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="top-buffer6"></div>
                <div class="container">
                    <div class="dashboard">
                        <div class="title sec4">
                            <h3 class="text-center"><?php the_title(); ?></h3>
                            <a class="back_btn_dash" href="<?php echo wp_logout_url(home_url()); ?>"> Logout </a>
                        </div>
                        <?php
                        $user_video_id = get_option('user_video_id');
                        $user_pdf_id = get_option('user_pdf_id');
                        $user_subsc_id = get_option('user_subsc_id');
                        ?>
                        <div class="login-form">
<!--                             <div class="col-sm-4">
                                <div class="dash-box">
                                    <div class="top-buffer8"></div>
                                    <h1 class="s36"><a href="<?php //echo get_permalink($user_video_id); ?>">VIDEOS</a></h1>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="dash-box">
                                    <div class="top-buffer8"></div>
                                    <h1 class="s36"><a href="<?php //echo get_permalink($user_pdf_id); ?>">PDF<span>s</span></a></h1>
                                </div>
                            </div> -->
							<div class="col-sm-6">
                                <div class="dash-box">
                                    <div class="top-buffer6"></div>
                                    <h1 class="s36"><a href="<?php echo get_permalink(1292); ?>">Permission To Prosper<br> Videos + PDF<span>s</span></a></h1>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="dash-box">
                                    <div class="top-buffer6"></div>
                                    <h1 class="s36"><a href="<?php echo get_permalink($user_subsc_id); ?>">SUBSCRIPTION<br> DETAILS</a></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="top-buffer100"></div>
            </section>
            <!--- end Login--->


        <?php endwhile; ?>
    <?php else: ?> 
        <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
    <?php endif; ?>

    <?php
    get_footer();
}
else {
    wp_safe_redirect(get_permalink(get_option('login_page_id'))); /* Redirect To Dashboard page */
    die();
}?>