<?php
error_reporting(0);

$theme_name = get_current_theme(); 
$imgpath = "../wp-content/themes/manifest/images/";
/* LOGIN LOGO :: LoginLogo.png */

if($_REQUEST['action'] == 'saveoption') {
	$arc_option = get_option('arc_option');
	$tmptime = time();
	
	$old_logo = $_POST['old_logo'];
	if($_FILES['arc_logo']['name'] != "") {
		$logo = $_FILES['arc_logo']['name'];
		$misCopied = copy($_FILES['arc_logo']['tmp_name'],$imgpath.$logo);
	}
	
	if(isset($_FILES['arc_logo']['name']) && trim($_FILES['arc_logo']['name']) != '')
		$arc_option['arc_logo'] = $logo;
	else
		$arc_option['arc_logo'] = $old_logo;

	if(isset($_POST['arc_address']) && $_POST['arc_address'] != '')
		$arc_option['arc_address'] = $_POST['arc_address'];
	else
		$arc_option['arc_address'] = '';

	if(isset($_POST['arc_email']) && $_POST['arc_email'] != '')
		$arc_option['arc_email'] = $_POST['arc_email'];
	else
		$arc_option['arc_email'] = '';
	
	if(isset($_POST['arc_contact_1']) && $_POST['arc_contact_1'] != '')
		$arc_option['arc_contact_1'] = $_POST['arc_contact_1'];
	else
		$arc_option['arc_contact_1'] = '';

	if(isset($_POST['arc_contact_2']) && $_POST['arc_contact_2'] != '')
		$arc_option['arc_contact_2'] = $_POST['arc_contact_2'];
	else
		$arc_option['arc_contact_2'] = '';

	if(isset($_POST['arc_menuclr']) && trim($_POST['arc_menuclr']) != '')
		$arc_option['arc_menuclr'] = trim($_POST['arc_menuclr']);
	else
		$arc_option['arc_menuclr'] = '';

	if(isset($_POST['arc_menutclr']) && trim($_POST['arc_menutclr']) != '')
		$arc_option['arc_menutclr'] = trim($_POST['arc_menutclr']);
	else
		$arc_option['arc_menutclr'] = '';

	if(isset($_POST['arc_menuhclr']) && trim($_POST['arc_menuhclr']) != '')
		$arc_option['arc_menuhclr'] = trim($_POST['arc_menuhclr']);
	else
		$arc_option['arc_menuclr'] = '';

	if(isset($_POST['arc_menuhtclr']) && trim($_POST['arc_menuhtclr']) != '')
		$arc_option['arc_menuhtclr'] = trim($_POST['arc_menuhtclr']);
	else
		$arc_option['arc_menuhtclr'] = '';

	if(isset($_POST['arc_submenuclr']) && trim($_POST['arc_submenuclr']) != '')
		$arc_option['arc_submenuclr'] = trim($_POST['arc_submenuclr']);
	else
		$arc_option['arc_submenuclr'] = '';

	if(isset($_POST['arc_submenutclr']) && trim($_POST['arc_submenutclr']) != '')
		$arc_option['arc_submenutclr'] = trim($_POST['arc_submenutclr']);
	else
		$arc_option['arc_submenutclr'] = '';

	if(isset($_POST['arc_sctitleclr']) && trim($_POST['arc_sctitleclr']) != '')
		$arc_option['arc_sctitleclr'] = trim($_POST['arc_sctitleclr']);
	else
		$arc_option['arc_sctitleclr'] = '';

	if(isset($_POST['arc_sclineclr']) && trim($_POST['arc_sclineclr']) != '')
		$arc_option['arc_sclineclr'] = trim($_POST['arc_sclineclr']);
	else
		$arc_option['arc_sclineclr'] = '';

	if(isset($_POST['arc_remrclr']) && trim($_POST['arc_remrclr']) != '')
		$arc_option['arc_remrclr'] = trim($_POST['arc_remrclr']);
	else
		$arc_option['arc_remrclr'] = '';

	if(isset($_POST['arc_remrtxt']) && trim($_POST['arc_remrtxt']) != '')
		$arc_option['arc_remrtxt'] = trim($_POST['arc_remrtxt']);
	else
		$arc_option['arc_remrtxt'] = '';

	if(isset($_POST['arc_fb']) && trim($_POST['arc_fb']) != '')
		$arc_option['arc_fb'] = trim($_POST['arc_fb']);
	else
		$arc_option['arc_fb'] = '';
	
	if(isset($_POST['arc_instagram']) && trim($_POST['arc_instagram']) != '')
		$arc_option['arc_instagram'] = trim($_POST['arc_instagram']);
	else
		$arc_option['arc_instagram'] = '';
	
	if(isset($_POST['arc_pinterest']) && trim($_POST['arc_pinterest']) != '')
		$arc_option['arc_pinterest'] = trim($_POST['arc_pinterest']);
	else
		$arc_option['arc_pinterest'] = '';
	
	if(isset($_POST['arc_youtube']) && trim($_POST['arc_youtube']) != '')
		$arc_option['arc_youtube'] = trim($_POST['arc_youtube']);
	else
		$arc_option['arc_youtube'] = '';	
	
	if(isset($_POST['arc_fbshare']) && trim($_POST['arc_fbshare']) != '') $arc_option['arc_fbshare'] = trim($_POST['arc_fbshare']); else $arc_option['arc_fbshare'] = '';
	if(isset($_POST['arc_twittershare']) && trim($_POST['arc_twittershare']) != '') $arc_option['arc_twittershare'] = trim($_POST['arc_twittershare']); else $arc_option['arc_twittershare'] = '';
	if(isset($_POST['arc_pintrestshare']) && trim($_POST['arc_pintrestshare']) != '') $arc_option['arc_pintrestshare'] = trim($_POST['arc_pintrestshare']); else $arc_option['arc_pintrestshare'] = '';
	if(isset($_POST['arc_gplusshare']) && trim($_POST['arc_gplusshare']) != '') $arc_option['arc_gplusshare'] = trim($_POST['arc_gplusshare']); else $arc_option['arc_gplusshare'] = '';
	if(isset($_POST['arc_linkedinshare']) && trim($_POST['arc_linkedinshare']) != '') $arc_option['arc_linkedinshare'] = trim($_POST['arc_linkedinshare']); else $arc_option['arc_linkedinshare'] = '';
	if(isset($_POST['arc_tumlrshare']) && trim($_POST['arc_tumlrshare']) != '') $arc_option['arc_tumlrshare'] = trim($_POST['arc_tumlrshare']); else $arc_option['arc_tumlrshare'] = '';
	if(isset($_POST['arc_emailshare']) && trim($_POST['arc_emailshare']) != '') $arc_option['arc_emailshare'] = trim($_POST['arc_emailshare']); else $arc_option['arc_emailshare'] = '';
	if(isset($_POST['arc_printshare']) && trim($_POST['arc_printshare']) != '') $arc_option['arc_printshare'] = trim($_POST['arc_printshare']); else $arc_option['arc_printshare'] = '';
	
    /* FOOTER MINDSET BLOCK SETTINGS STARTS */
    if(isset($_POST['arc_footer_block_title']) && $_POST['arc_footer_block_title'] != '') {
        $arc_option['arc_footer_block_title'] = $_POST['arc_footer_block_title'];
    } else {
        $arc_option['arc_footer_block_title'] = '';
    }
    if(isset($_POST['arc_footer_block_description']) && $_POST['arc_footer_block_description'] != '') {
        $arc_option['arc_footer_block_description'] = $_POST['arc_footer_block_description'];
    } else {
        $arc_option['arc_footer_block_description'] = '';
    }
    if(isset($_POST['arc_footer_block_url']) && $_POST['arc_footer_block_url'] != '') {
        $arc_option['arc_footer_block_url'] = $_POST['arc_footer_block_url'];
    } else {
        $arc_option['arc_footer_block_url'] = '';
    }
    if(isset($_POST['arc_copyright']) && $_POST['arc_copyright'] != '') {
        $arc_option['arc_copyright'] = $_POST['arc_copyright'];
    } else {
        $arc_option['arc_copyright'] = '';
    }
    /* FOOTER MINDSET BLOCK SETTINGS ENDS */

    update_option('arc_option',$arc_option);
    echo '<script>window.location="themes.php?page=theme_options"</script>';
    exit;
}
$arc_option = get_option('arc_option');
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/stylepanel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colorpicker/css/layout.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/colorbox.css" />

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/colorpicker/js/eye.js"></script>
<?php /*?><script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/colorpicker/js/utils.js"></script><?php */?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/colorpicker/js/layout.js?ver=1.0.2"></script>
<?php /*?><script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.colorbox-min.js"></script><?php */?>
<div>
	<span class="page-heading"><?php bloginfo('name'); ?> Theme Options</span>
    <div>
        <form name="frm" action="<?php echo $PHP_SELF; ?>" method="post" enctype="multipart/form-data" acceptcharset="UTF-8" >
        	<fieldset class="fieldset">
               <div class="fieldsection">
                <div class="fieldtitle"><label>Logo</label></div>
                <div class="clr"></div>
            </div>
            <div class="fieldsection">
                <div class="field-left">
                    <input type="file" name="arc_logo" id="arc_logo" value="" onChange="return validateFile(this);" />
                    <input type="hidden" id="old_logo" name="old_logo" size="50" value="<?php echo $arc_option["arc_logo"]; ?>">
                    <br/><br/>
                    <?php
                    if($arc_option["arc_logo"] != "") {
                        print '<div style="background-color:#FFF; padding:20px;"><img src="'.$imgpath.$arc_option["arc_logo"].'"></div>';
                    } ?>
                </div>
            </div>
            <div class="clr"></div>              
        </fieldset>
        <fieldset class="fieldset">
           <div class="fieldsection">
            <div class="fieldtitle"><label>Color Options</label></div>
            <div class="clr"></div>
        </div>
        <div class="field-left">
           <div class="innerdiv">
            <label class="innerlabel"><b>Menubar Background Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField1" name="arc_menuclr" type="text" value="<?php echo $arc_option["arc_menuclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Menubar Text Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField2" name="arc_menutclr" type="text" value="<?php echo $arc_option["arc_menutclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Selected Menubar Background Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField3" name="arc_menuhclr" type="text" value="<?php echo $arc_option["arc_menuhclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Selected Menubar Text Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField4" name="arc_menuhtclr" type="text" value="<?php echo $arc_option["arc_menuhtclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Submenu Background Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField5" name="arc_submenuclr" type="text" value="<?php echo $arc_option["arc_submenuclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Submenu Text Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField6" name="arc_submenutclr" type="text" value="<?php echo $arc_option["arc_submenutclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Section / Category Title Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField7" name="arc_sctitleclr" type="text" value="<?php echo $arc_option["arc_sctitleclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Section / Category Line Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField8" name="arc_sclineclr" type="text" value="<?php echo $arc_option["arc_sclineclr"]; ?>">
        </div>
        <div class="innerdiv">
            <label class="innerlabel"><b>Read More Button Color</b></label>
            <input maxlength="6" size="10" class="color_txt" id="colorpickerField9" name="arc_remrclr" type="text" value="<?php echo $arc_option["arc_remrclr"]; ?>">
        </div>
    </div>
    <div class="clr"></div>              
</fieldset>
<fieldset class="fieldset">
   <div class="fieldsection">
    <div class="fieldtitle"><label>Contact Information</label></div>
    <div class="clr"></div>
</div>
<div class="field-left">
   <div class="innerdiv">
    <label class="innerlabel"><b>Address</b></label>
    <input class="field_txt" id="arc_address" name="arc_address" type="text" value="<?php echo $arc_option["arc_address"]; ?>">
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Email Address</b></label>
    <input class="field_txt" id="arc_email" name="arc_email" type="text" value="<?php echo $arc_option["arc_email"]; ?>">
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Contact Number</b></label>
    <input class="field_txt" id="arc_contact_1" name="arc_contact_1" type="text" value="<?php echo $arc_option["arc_contact_1"]; ?>">
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Contact Number</b></label>
    <input class="field_txt" id="arc_contact_2" name="arc_contact_2" type="text" value="<?php echo $arc_option["arc_contact_2"]; ?>">
</div>
</div>
<div class="clr"></div>              
</fieldset>
<fieldset class="fieldset">
   <div class="fieldsection">
    <div class="fieldtitle"><label>Social Media Information</label></div>
    <div class="clr"></div>
</div>
<div class="field-left">
   <div class="innerdiv">
    <label class="innerlabel"><b>Facebook</b></label>
    <input class="field_txt" id="arc_fb" name="arc_fb" type="text" value="<?php echo $arc_option["arc_fb"]; ?>">
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Instagram</b></label>
    <input class="field_txt" id="arc_instagram" name="arc_instagram" type="text" value="<?php echo $arc_option["arc_instagram"]; ?>">
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Pinterest</b></label>
    <input class="field_txt" id="arc_pinterest" name="arc_pinterest" type="text" value="<?php echo $arc_option["arc_pinterest"]; ?>">
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>YouTube</b></label>
    <input class="field_txt" id="arc_youtube" name="arc_youtube" type="text" value="<?php echo $arc_option["arc_youtube"]; ?>">
</div>
</div>
<div class="clr"></div>
</fieldset>            
<fieldset class="fieldset">
   <div class="fieldsection">
    <div class="fieldtitle"><label>Social Share Information</label></div>
    <div class="clr"></div>
</div>
<div class="field-left">
   <div class="innerdiv">
    <label class="innerlabel" for="arc_Icons"><b>Facebook</b></label>
    <input class="checkbox_txt" id="arc_fbshare" name="arc_fbshare" type="checkbox" value="1" <?php if($arc_option['arc_fbshare'] =='1') { ?> checked="checked" <?php } ?>>
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Twitter</b></label>
    <input class="checkbox_txt" id="arc_twittershare" name="arc_twittershare" type="checkbox" value="1" <?php if($arc_option['arc_twittershare'] =='1') { ?> checked="checked" <?php } ?>>
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Pinterest</b></label>
    <input class="checkbox_txt" id="arc_pintrestshare" name="arc_pintrestshare" type="checkbox" value="1" <?php if($arc_option['arc_pintrestshare'] =='1') { ?> checked="checked" <?php } ?>>
</div>                    
<div class="innerdiv">
    <label class="innerlabel"><b>Google Plus</b></label>
    <input class="checkbox_txt" id="arc_gplusshare" name="arc_gplusshare" type="checkbox" value="1" <?php if($arc_option['arc_gplusshare'] =='1') { ?> checked="checked" <?php } ?>>
</div>                    
<div class="innerdiv">
    <label class="innerlabel"><b>Linkedin</b></label>
    <input class="checkbox_txt" id="arc_linkedinshare" name="arc_linkedinshare" type="checkbox" value="1" <?php if($arc_option['arc_linkedinshare'] =='1') { ?> checked="checked" <?php } ?>>
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Tumblr</b></label>
    <input class="checkbox_txt" id="arc_tumlrshare" name="arc_tumlrshare" type="checkbox" value="1" <?php if($arc_option['arc_tumlrshare'] =='1') { ?> checked="checked" <?php } ?>>
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Email</b></label>
    <input class="checkbox_txt" id="arc_emailshare" name="arc_emailshare" type="checkbox" value="1" <?php if($arc_option['arc_emailshare'] =='1') { ?> checked="checked" <?php } ?>>
</div>
<div class="innerdiv">
    <label class="innerlabel"><b>Print</b></label>
    <input class="checkbox_txt" id="arc_printshare" name="arc_printshare" type="checkbox" value="1" <?php if($arc_option['arc_printshare'] =='1') { ?> checked="checked" <?php } ?>>
</div>
</div>
<div class="clr"></div>
</fieldset>

<fieldset class="fieldset">
   <div class="fieldsection">
    <div class="fieldtitle"><label>General Options</label></div>
    <div class="clr"></div>
</div>
<div class="field-left">
   <div class="innerdiv">
    <label class="innerlabel"><b>Read More Button Text</b></label>
    <input class="field_txt" id="arc_remrtxt" name="arc_remrtxt" type="text" value="<?php echo $arc_option["arc_remrtxt"]; ?>">
</div>
</div>
<div class="clr"></div>              
</fieldset>

<!-- BOOK YOUR MINDSET SESSION STARTS HERE -->
<fieldset class="fieldset">
   <div class="fieldsection">
    <div class="fieldtitle"><label>Footer - Book Your Mindset Session Block</label></div>
    <div class="clr"></div>
</div>
<div class="field-left">
   <div class="innerdiv">
       <label class="innerlabel">Block Title</label>
       <input class="field_txt" id="arc_footer_block_title" name="arc_footer_block_title" type="text" value="<?php echo $arc_option["arc_footer_block_title"]; ?>" />
   </div>
   <div class="innerdiv">
       <label class="innerlabel">Block Description</label>
       <input class="field_txt" id="arc_footer_block_description" name="arc_footer_block_description" type="text" value="<?php echo $arc_option["arc_footer_block_description"]; ?>" />
   </div>
   <div class="innerdiv">
       <label class="innerlabel">Block Redirection URL</label>
       <input class="field_txt" id="arc_footer_block_url" name="arc_footer_block_url" type="text" value="<?php echo $arc_option["arc_footer_block_url"]; ?>" />
   </div>
   <div class="innerdiv">
       <label class="innerlabel">Footer Copyright Text</label>
       <input class="field_txt" id="arc_copyright" name="arc_copyright" type="text" value="<?php echo $arc_option["arc_copyright"]; ?>" />
   </div>
</div>
<div class="clr"></div>              
</fieldset>
<!-- BOOK YOUR MINDSET SESSION ENDS HERE -->


<fieldset class="fieldset"> 
    <div class="fieldsection">
        <input type="hidden" name="action" value="saveoption">
        <input class="button-primary" type="submit" name="updatesave" value="Update">                
    </div>
</fieldset>
</form>
</div>
</div>
<script language="javascript" type="application/javascript">
    function validateFile(myimg) {
       if(myimg.value != "") {
          var str=new String();
          str=myimg.value;
          var len1;
          len1=str.length;
          var f=str.substr(len1-3,len1);
          var e=str.substr(len1-4,len1);
          if(!(f=="jpg" || f=="JPG" || f=="png" || f=="PNG" || f=="gif" || f=="GIF" || e=="jpeg" || e=="JPEG" )) {
             alert("You must have to select only image.")
             myimg.value = "";
             return false;
         }
     }
 }
</script>