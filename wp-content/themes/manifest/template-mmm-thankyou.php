<?php
/*
  Template Name: Money Mindset Makeover - Thank you
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Money Mindset Mentor & Wealth Therapist">
    <meta name="author" content="Agnes Kowalski">
    <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
    <link rel="shortcut icon" href="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/favicon.ico" type="image/x-icon" />
    <title>Thank You</title>
    <!-- Bootstrap core CSS -->
    <link href="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/thankyou-css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/thankyou-css/style.css" rel="stylesheet">
	<script src="http://www.agneskowalski.com/wp-content/themes/moneymindsetmakeover/thankyou-js/jquery-1.12.4.min.js"></script>
</head>
<body>
<div class="main_div">
<!-- thank you start -->
	<section>
		<div class="seaction5">
			<div class="sec5 sec6">
				<div class="container">
					<div class="top-buffer2"></div>
					<div class="ruls col-lg-8 col-md-12 col-sm-12 col-xs-12">
						<div class="rule1 thankyou">
							<h2>You <span>did it,</span></h2>
							<h4>You are ready to Makeover that</h4>
							<h4>Janky Money Mindset and get real about your Money Story, </h4>
							<h4>I’m proud of you!</h4>
							<div class="top-buffer2"></div>
							
							<p>While that’s on the way to your inbox, <br />
							go ahead and join the other smart cookies inside of my fb group:</p>
							<div class="top-buffer2"></div>
							<a href="https://www.facebook.com/groups/certifiedbynoone/"><h1>Money Mindset Moguls</h1></a>
							
							<h3><b>P.S. </b>- Love you more than my Roomba,<br />
								and i like seriously love that thing</h3>
							<div class="top-buffer05"></div>
							<h5>See you tomorrow for<br />
								<span>Step 1 of your Money Mindset Makeover!</span></h5>
							<div class="top-buffer2"></div>
							<P class="font-1">xo</P>
							
							<h6>Agnes Kowalski</h6>
							<div class="top-buffer3"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="seaction12">
			<div class="container">
				<p>© 2017 All rights reserved</p>
			</div>
		</div>
	</section>
<!-- thank you end -->

</div>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106731453-1');
</script>
</body>
</html>