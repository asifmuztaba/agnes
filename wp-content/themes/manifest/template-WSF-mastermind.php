<?php
/* Template Name: Wealthy Self Worth Mastermind */
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Wealthy Self Worth Mastermind - Agnes Kowalski</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="" />
		<link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/css/bootstrap.min.css">
		<link href="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	</head>
    <body>

	<section id="first_section">
	     <div class="banner_img">
		     <div class="container">
			     <div class="col-md-6 col-lg-8">
				     <div class="main_title text-center">
					    <h1 class="green">Wealthy <span class="maroon">Self</span> Worth</h1>
						<h2 class="bg-green inline-block white"><i>Mastermind</i></h2>
						<div class="top-buffer3"></div>
						<div class="your_sec green italic">
						     <p>Trigger your <span>subconscious.</span></p>
						     <p>Face your <span>excuses.</span></p>
						     <p>Change your  <span>self-image.</span></p>
						</div>	
						<div class="start_sec">
						     <div class="box">
							      <p>A 5 month intensive mastermind.</p>
						          <p class="start_date">Starts week of July 9<sup>th</sup></p>
							 </div>
						</div>
						<div class="group_info">
						    <p>Weekly Laser Coaching calls. </p>
						    <p>Private FB Group.</p>
						</div>
					 </div>
				 </div>
			 </div>
		 </div>
	</section>
	<section id="methodology">
	    <div class="container">
		    <div class="col-md-8 col-md-offset-2">
			    <div class="text-center pt-50 pb-50">
				    <h2 class="title">Methodology</h2>
					<div class="top-buffer2"></div>
				    <p>My unreleased to the public 10 Laws of Wealthy Self-Worth framework.</p>
				    <p>You will also receive a detailed intake form to customize the mindset work.</p>
					
					<div class="top-buffer2"></div>
					<ul class="step">
					    <li><p>Each month we go through 2 modules of WSW</p></li>
					    <li><p>Every Module has implementable homework</p></li>
					    <li><p>Every call is mindset laser coaching</p></li>
					</ul>
					<div class="top-buffer1"></div>
					<h2 class="title">Schedule</h2>
					<p class="top-buffer1">Starts July 10th Every Tuesday exact time to TBA and will be arranged once all attendees are signed up.</p>
				</div>
			</div>
		</div>
	</section>
	
	<section id="who_section" class="bg-lightgreen">
	    <div class="container">
		    <div class="row">
			    <div class="col-md-10 col-md-offset-1"> 
				    <div class="row">
						<div class="col-sm-6">
							<div class="box pt-50 pb-50 xs-pb-0">
								 <h3 class="sub-title text-center white">Who is this for</h3>
								 <div class="top-buffer2"></div>
								 <ul class="step true-icon">
									 <li><p>Experienced mindset practitioners</p></li>
									 <li><p>Masters of their craft regardless of industry</p></li>
									 <li><p>Have previously been coached/mentored</p></li>
									 <li><p>Those who are ready to take full responsibility over their consciousness and rock their reality into a new income and self worth level</p></li>
								 </ul>
							</div>					
						</div>
						
						<div class="col-sm-6">
							<div class="box pt-50 pb-50">
								 <h3 class="sub-title text-center white">Who is this for</h3>
								 <div class="top-buffer2"></div>
								 <ul class="step close-icon">
									 <li><p>You are a total beginner at mindset</p></li>
									 <li><p>You are just starting the journey of being a professional at your craft</p></li>
									 <li><p>You have never been coached before</p></li>
									 <li><p>You are struggling in the day to day</p></li>
								 </ul>
								 <p class="white roboto-regular link">If that’s you, start with <a href="#" class="white underline unset-hover">Permission to Prosper</a></p>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section id="payment_section">
	    <div class="container text-center">
		    <div class="pt-50 pb-50">
				<h2 class="title">Cost: $5000</h2>
				<div class="top-buffer1"></div>
				
				<div class="plans row">
				     <div class="col-sm-6 col-md-4 border-right">
					    <p class="green">1 Payment = $5000</p>
						<div class="button">
						    <img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/left-arrow.png"/>
						    <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VE8F2N2SFBPZS"><button type="submit" name="submit">SIGN ME UP</button></a> 
							<img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/right-arrow.png"/>
						</div>
					 </div>
					 <div class="col-sm-6 col-md-4 border-right xs-border-rm">
					    <p class="green">2 payments = $2750</p>
						<div class="button">
						    <img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/left-arrow.png"/>
						    <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RHPNBE4DBEQTL"><button type="submit" name="submit">SIGN ME UP</button></a> 
							<img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/right-arrow.png"/>
						</div>
					 </div>
					 <div class="col-sm-6 col-md-4 xs-left">
					    <p class="green">Monthly payments = $1250 (x5)</p>
						<div class="button">
						    <img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/left-arrow.png"/>
						    <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E5LKVVEXDJM88"><button type="submit" name="submit">SIGN ME UP</button></a> 
							<img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/right-arrow.png"/>
						</div>
					 </div>
				</div>
		    </div>
		</div>
	</section>
	
	<footer class="text-center bg-lightgreen">
	    <p class="white">© 2018 All rights reserved</p>
	</footer>
		
	<script src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/js/jquery.min.js"></script>
	<script src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/js/bootstrap.min.js"></script>
    </body>
</html>