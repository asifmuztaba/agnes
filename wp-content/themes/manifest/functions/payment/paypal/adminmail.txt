
                    <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:96%; float:left; margin:2%">
                        <tr><td style="font-size:16px;">Hello Admin,</td></tr>
                        <tr>
                            <td style="padding:10px 0; width:150px;"> New Payment for the program Permission To Prosper has been done. The details are as below :</td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%; margin:10px 0; font-size:14px; border-collapse:collapse;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Username</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">mlr5085@gmail.com</td>
                                    </tr>	
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Program Name</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Permission To Prosper</td>
                                    </tr>		
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Payment Given</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">297.00 USD</td>
                                    </tr>						
                                </table>
                            </td>
                        </tr>
                    </table>