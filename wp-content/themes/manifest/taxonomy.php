<?php
/**
* A Simple Category Template
*/
get_header();

echo "welcome"; exit;
global $wpdb,$post;
$terms = get_the_terms($post->id, 'TAXONOMY_NAME');
$term =	$wp_query->queried_object;
$catgoryid = $term->term_id;
$catgoryslug = $term->slug;
$catgoryname = $term->name;
?>
<div class="clearfix top-buffer-30"></div>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/dance.jpg" alt="<?php echo $banner_title; ?>" title="<?php echo $banner_title; ?>" width="100%">
		</div>
	</div>
</div>

<div class="clearfix top-buffer-30"></div>
<div class="clearfix top-buffer-30"></div>

<div class="container">
	<div class="clearfix top-buffer-30"></div>	
	<div class="row text-center s-padding coursepage">
		<?php
        $getcourses = $wpdb->get_results("SELECT object_id FROM ".$wpdb->prefix."term_relationships WHERE term_taxonomy_id = '".$catgoryid."' ORDER BY object_id DESC");
        $rows = $wpdb->num_rows;
        if($rows > 0) {
            foreach($getcourses as $coursesdata) {
                $objid = $coursesdata->object_id;
                $getdata = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."posts WHERE ID = '".$objid."'");
                $gettitle = $getdata->post_title;
				$cimage = wp_get_attachment_image_src( get_post_thumbnail_id( $getdata->ID ), 'single-post-thumbnail' );
				$title = get_field('title',$post->ID);
				$short_description = get_field('short_description',$post->ID);
				?>                                            
				<div class="col-lg-4 col-sm-6 col-xs-12 paddi-top singlecourse"><a href="<?php the_permalink(); ?>"><img class="img-circle circal-border border-r" src="<?php echo $cimage[0]; ?>" width="200" height="200" alt="<?php echo $gettitle; ?>" title="<?php echo $gettitle; ?>"></a>
					<h2 class="white"><a href="<?php the_permalink(); ?>"><?php echo $gettitle; ?></a></h2>
					<?php echo $short_description; ?>
					<div class="clearfix top-buffer-30"></div>
				</div>
             <?php
            }
        } else {
            ?>
            <h3>Sorry, no courses found.</h3>
            <?php
        }
        ?>
    </div>	  
</div>
<div class="clearfix top-buffer-30"></div>
<div class="clearfix top-buffer-30"></div>
<?php get_footer(); ?>