<?php
/*
  Template Name: Forgot Password
 */

session_start();
if (isset($_POST['btn_reset_pass'])) {
    $email = $_POST['user_login'];
    if (email_exists($email)) {
        $_SESSION['mail_check'] = "We have send reset password link to your email id. Kindly check your mail box.";
        ?>
        <form name="lostpasswordform" id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">

            <input type="hidden" name="user_login" id="user_login"  class="validate[required,custom[email]]" value="<?php echo $_POST['user_login']; ?>" />

            <input type="hidden" name="redirect_to" value="<?php echo get_permalink(get_option('login_page_id'));
        ; ?>">
        </form>
        <script language="JavaScript">
            document.forms["lostpasswordform"].submit();
        </script>
        <?php
    } else {
        $_SESSION['pass_error_msg'] = "Email does not Exist";
    }
}
?>
<?php get_header(); ?>

<?php if (have_posts()) : ?>  
    <?php while (have_posts()) : the_post(); ?>


        <!---login---->
        <section class="login-page">
            <div class="top-buffer100"></div>
            <div class="top-buffer100"></div>
            <div class="container">
                <div class="login">
                    <div class="title sec4">
                        <h3><?php the_title(); ?></h3>
                    </div>
                    <div class="login-form">
                        <form name="lostpass_form" id="lostpass_form" method="post">
        <?php if ($_SESSION['pass_error_msg']) { ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?php echo $_SESSION['pass_error_msg'];
                                $_SESSION['pass_error_msg'] = ""; ?>
                                </div>
                                <?php } ?>
                            <div class="form-group">
                                <input type="email" class="text-input" required="" name="user_login" id="username" placeholder="Enter Your Email Address">
                                <input type="hidden" name="redirect_to" value="<?php echo $redirect; ?>">
                            </div>
                            <div class="sub-button">
                                <button class="log-submit" type="submit" name="btn_reset_pass">Send Me New Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="top-buffer100"></div>
            <div class="top-buffer100" style="margin-top:93px;"></div>
        </section>
        <!--- end Login--->


    <?php endwhile; ?>
<?php else: ?> 
    <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
<?php endif; ?>

<?php get_footer(); ?>