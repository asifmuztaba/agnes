<?php
/*
  Template Name: CMS
 */
?>
<?php
get_header();
?>

<?php if (have_posts()) : ?>  
    <?php while (have_posts()) : the_post(); ?>


        <!---login---->
        <section class="login-page">
            <div class="top-buffer6"></div>
            <div class="container">
                <div class="dashboard pdfs">
                    <div class="title sec4">
                        <h3 class="text-center"><?php the_title(); ?></h3>
                    </div>
                    <div class="top-buffer1"></div>
                    <div class="top-buffer1"></div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-buffer100"></div>
        </section>
        <!--- end Login--->


    <?php endwhile; ?>
<?php else: ?> 
    <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
<?php endif; ?>

<?php
get_footer();
?>