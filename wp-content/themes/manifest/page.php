<?php
get_header();
?>

<?php if (have_posts()) : ?>  
    <?php while (have_posts()) : the_post(); ?>

        <!-- PAGE CONTENT SECTION STARTS -->
        <section>
            <div class="m-seaction">
                <div class="main-sec">
                    <div class="container">
                        <div class="mhed">
                            <div class="top-buffer2"></div>
                            <p class="s40"><?php the_title(); ?></p>
                            <div class="top-buffer2"></div>                        
                        </div>
                        <div class="mbody">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </section>
        <!-- PAGE CONTENT SECTION ENDS -->

    <?php endwhile; ?>
<?php else: ?> 
    <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
<?php endif; ?>

<?php get_footer(); ?>