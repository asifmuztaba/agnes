<?php
/* Template Name: Wealthy Self Worth Workshop Sales Page */
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Wealthy Self Worth Workshop - Agnes Kowalski</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="" />
        <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/salespage/assets/css/bootstrap.min.css">
        <link href="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/salespage/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>

    <section id="first_section">
         <div class="banner_img">
             <div class="container">
                 <div class="col-md-6 col-lg-8">
                     <div class="main_title text-center">
                        <h1 class="green">Wealthy <span class="maroon">Self</span> Worth</h1>
                        <h2 class="bg-green inline-block white"><i>One Day Workshop</i></h2>
                        <div class="top-buffer3"></div>
                        <div class="your_sec green italic">
                             <p>Face your <span>Excuses.</span></p>
                             <p>Stop Avoiding your <span>Success. </span></p>
                             <p>Tap into your Internal   <span>Affluence.</span></p>
                        </div>  
                        <div class="start_sec">
                             <div class="box">
                                  <p>Small Group Intimate Immersion.</p>
                                  <p class="start_date">10am - 4pm</p>
                             </div>
                        </div>
                        <div class="group_info col-lg-8 col-lg-offset-2">
                            <p>You will receive an intake before the workshop that will be the roadmap for your breakthrough.</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                 </div>
             </div>
         </div>
    </section>
    
    <section id="who_section" class="bg-lightgreen">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1"> 
                    <div class="row pb-50 pt-50">
                        <h2 class="title white text-center">Methodology</h2>
                        
                        <ul class="step true-icon top-buffer1">
                            <li>1.&nbsp; <p>Placing what is going to be released into the subconscious with the intake form, so things start to “brew” in advance.</p></li>
                            <li>2.&nbsp; <p>During your hotseat we use stream of consciousness prompts to access what you REALLY believe in your subconscious about yourself and about money/success.</p></li>
                            <li>3.&nbsp; <p>We release those blocks by acknowledging them in the present moment using the conscious mind.</p></li>
                            <li>4.&nbsp; <p>We replace those beliefs, with what you actually need to believe to become the person who can create what you desire. </p></li>
                            <li>5.&nbsp; <p>We strategize on a mindset plan and a realistic action plan that is aligned with your design so that you can start increasing your self worth and income immediately.</p></li>
                        </ul>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="methodology">
        <div class="container">
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div class="text-center pt-50">
                    <h2 class="title">Schedule</h2>
                    <div class="clearfix"></div>
                
                        <div class="text-left green top-buffer2">
                            <div class="colom">
                                <div class="col-xs-6">
                                     <p>10:00am <span class="pull-right"> -</span></p>
                                </div>
                                <div class="col-xs-6">
                                     <p>Introduction</p>
                                </div> 
                            </div>
                            
                            <div class="colom">
                                <div class="col-xs-6">
                                     <p>10:15am <span class="pull-right"> -</span></p>
                                </div>
                                <div class="col-xs-6">
                                     <p>Hotseat 1 + breakout exercises</p>
                                </div> 
                            </div>
                            
                            <div class="colom">
                                <div class="col-xs-6">
                                     <p>11:15am <span class="pull-right"> -</span></p>
                                </div>
                                <div class="col-xs-6">
                                     <p>Hotseat 2 + breakout exercises</p>
                                </div> 
                            </div>
                            
                            <div class="colom">
                                <div class="col-xs-6">
                                     <p>12:15pm <span class="pull-right"> -</span></p>
                                </div>
                                <div class="col-xs-6">
                                     <p>Hotseat 3 + breakout exercises</p>
                                </div> 
                            </div>
                            
                            <div class="colom">
                                <div class="col-xs-6">
                                     <p>01:30pm <span class="pull-right"> -</span></p>
                                </div>
                                <div class="col-xs-6">
                                     <p>Break for Lunch</p>
                                </div> 
                            </div>
                            
                            <div class="colom">
                                <div class="col-xs-6">
                                     <p>02:00pm  -  03:00pm <span class="pull-right"> -</span></p>
                                </div>
                                <div class="col-xs-6">
                                     <p>Hotseat 4 + breakout exercises</p>
                                </div> 
                            </div>
                            
                            <div class="colom">
                                <div class="col-xs-6">
                                     <p>03:00pm  -  04:00pm <span class="pull-right"> -</span></p>
                                </div>
                                <div class="col-xs-6">
                                     <p>Hotseat 5 + breakout exercises</p>
                                </div> 
                            </div>
                            
                        </div>
                    
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-8 col-md-offset-2">
                    <p class="top-buffer2 pb-50 text-center">Everyone will get 60mins approx. in the hotseat to work through their money and self worth stuff.</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>

    <section id="payment_section">
        <div class="banner_img">
            <div class="container text-center">
                <div class="col-sm-7 col-sm-offset-5 col-md-8 col-md-offset-4 pt-70 pb-70">
                    <h2 class="title">JOIN</h2>
                    <div class="top-buffer1"></div>
                    
                    <div class="sign inline-block">
                        <h1 class="maroon">Agnes Kowalski</h1>
                        <h2 class="text-right">Wealth Therapist</h2>
                    </div>
                    
                    <div class="top-buffer5 clearfix"></div>
                    <div class="plans row">
                         <div class="col-lg-6 border-right xs-border-rm">
                            <p class="green">Vancouver</p>
                            <div class="date top-buffer1">
                                <p>August 28th 2018</p>
                                <p>10:00am - 04:00pm est</p>
                                <p>Location TBA</p>
                            </div>
                            <div class="button top-buffer1">
                                <img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/left-arrow.png"/>
                                <a href="https://www.paypal.com/cgi-bin/webscr?business=agneskowalskitherapy@yahoo.ca&cmd=_xclick&item_name=Wealth%20Therapist%20Orlando&item_number=29&amount=500&custom=Wealth&currency_code=USD&tax=0&notify_url=http://www.agneskowalski.com/wp-content/themes/manifest/functions/payment/paypal/ipn.php&cancel_return=http://www.agneskowalski.com/payment-failure/&return=http://www.agneskowalski.com/payment-wealth-successful/"><button type="submit" name="submit">Grab A Spot Now: $500</button></a> 
                                <img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/right-arrow.png"/>
                            </div>
                            <div class="top-buffer2"></div>
                         </div>
                         <div class="col-lg-6 xs-left">
							<center> <img style="position: relative;margin-bottom: -205px;"src="http://www.agneskowalski.com/wp-content/uploads/2018/06/Untitled-1-1.png" class="img-responsive"/></center>
                        <p class="green">NYC</p>
                            <div class="date top-buffer1">
                                <p>July 13th 2018</p>
                                <p>10:00am - 04:00pm est</p>
                                <p>Location TBA</p>
                            </div>
                            <div class="button top-buffer1">
                                <img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/left-arrow.png"/>
                                <a href="#"><button type="submit" name="submit">Grab A Spot Now: $500</button></a>
                                <img src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/images/right-arrow.png"/>
                            </div>
                            <div class="top-buffer2"></div>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer class="text-center bg-lightgreen">
        <p class="white">© 2018 All rights reserved</p>
    </footer>
        
    <script src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/js/jquery.min.js"></script>
    <script src="http://www.agneskowalski.com/wp-content/themes/wealthyselfworth/mastermind/assets/js/bootstrap.min.js"></script>
    </body>
</html>