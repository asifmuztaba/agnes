<?php
get_header();
global $post;

$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

if (have_posts()) : while (have_posts()) : the_post();
$arc_option = get_option('arc_option');
$commentscount = get_comments_number();

$category = get_the_category($post->ID);
$catname = $category[0]->cat_name;

$arc_fbshare = $arc_option['arc_fbshare'];
$arc_twittershare = $arc_option['arc_twittershare'];
$arc_pintrestshare = $arc_option['arc_pintrestshare'];
$arc_gplusshare = $arc_option['arc_gplusshare'];
$arc_linkedinshare = $arc_option['arc_linkedinshare'];
$arc_tumlrshare = $arc_option['arc_tumlrshare'];
$arc_emailshare = $arc_option['arc_emailshare'];
$arc_printshare = $arc_option['arc_printshare'];

$pimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
?>

<div class="top-buffer3"></div>
<div class="top-buffer1"></div>


<section>
    <div class="container">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <?php if ($pimage[0] != "") { ?>
            <img title="<?php the_title(); ?>" alt="<?php the_title(); ?>" class="img-responsive reach center-block" src="<?php echo $pimage[0]; ?>" >
            <?php } ?>
            <div class="top-buffer2"></div>
            <p class="s35"><?php the_title(); ?></p>
            <div class="top-buffer2"></div>

            <div class="b22 metasinglemeta">
                <span class="s22 a22">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    &nbsp; <?php the_time(get_option('date_format')); ?> I 
                </span>
            </div>
            <div class="b23 metasinglemeta">
                <span class="a22">
                    <i class="fa fa-comment" aria-hidden="true"></i>
                    &nbsp; <a href="<?php the_permalink(); ?>#comment" class="go_to_comment">LEAVE A COMMENT</a> I 
                </span>
            </div>
            <div class="b24">
                <span class="a22">

                    SHARE ON &nbsp; 
                    <a class="fb-xfbml-parse-ignore" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>&amp;src=sdkpreparse" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" /></a>
                    <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>&amp;tw_p=tweetbutton&amp;url=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a>
                    <a href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $pimage[0]; ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/pinterest.png" /></a>
                    <a href="mailto:?&subject=<?php echo urlencode(get_the_title()); ?>&body=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/email.png" /></a>

                    </span>
                </div>

                <div class="top-buffer2"></div>

                <div class="blog_content_sec">
                    <?php the_content(); ?>
                </div>


                <div class="button5">
                    <?php
                    /* ---  Single Post Navigation --- */
                    $pagelist = get_posts('sort_column=menu_order&sort_order=asc');
                    $pages = array();
                    foreach ($pagelist as $page) {
                        $pages[] += $page->ID;
                    }
                    $current = array_search(get_the_ID(), $pages);
                    $prevID = $pages[$current - 1];
                    $nextID = $pages[$current + 1];
                    ?>
                    <?php if (!empty($prevID)) { ?>
                    <a href="<?php echo get_permalink($prevID); ?>" title="<?php echo get_the_title($prevID); ?>"><input value="&lt; Prev" type="button" /></a>
                    <?php } else { ?>
                    <?php } ?>
                    <?php if (!empty($nextID)) { ?>
                    <a href="<?php echo get_permalink($nextID); ?>" title="<?php echo get_the_title($nextID); ?>"><input value="Next &gt;" class="button6" type="button" /></a>
                    <?php } else { ?>
                    <?php } ?>
                    <!-- Single Post Navigation Ends -->
                </div>

                <div class="top-buffer3"></div>
                <div class="top-buffer2"></div>


                <?php
                if (comments_open() || get_comments_number()) {
                    comments_template();
                }
                ?>

                <div class="top-buffer3"></div>
                <div class="top-buffer2"></div>

                <p class="s35 metasinglemeta">You may also like to read</p>
                <div class="top-buffer2"></div>
                <div class="row">

                    <div class="row p-right">
                        <?php
                        $count = 0;
                        $args = array(
                            'post_type' => 'post',
                            'orderby' => 'rand',
                            'post_status' => 'publish',
                            'category_name' => $catname,
                            'post__not_in' => array($post->ID),
                            'showposts' => 3,
                            );
                        $posts = query_posts($args);
                        if (have_posts()) : while (have_posts()) : the_post();
                        $pimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 responsive2">
                            <a href="<?php the_permalink(); ?>">
                                <?php if ($pimage[0] != "") { ?><img src="<?php echo $pimage[0]; ?>" class="img-responsive" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php } ?>
                                <div class="top-buffer"></div>
                                <p class="s35 a36"><?php the_title(); ?></p>
                            </a>
                        </div>
                        <?php
                        $count++;
                        endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 textsatting">
                <?php if (is_active_sidebar('blog_sidebar')) { ?>
                <?php dynamic_sidebar('blog_sidebar'); ?>
                <?php } ?>
            </div>
    </div>
</section>
    <?php
    endwhile;
    endif;
    ?>
    <script type="text/javascript">
        jQuery(function ($) {
            $('.go_to_comment').click(function () {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top
                }, 1000);
            });
        });
    </script>

    <div class="top-buffer3"></div>
    <div class="top-buffer2"></div>
    <?php get_footer(); ?>