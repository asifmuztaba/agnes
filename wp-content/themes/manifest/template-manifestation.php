<?php

/*

 * Template Name: Manifestation Template

 *

 */

get_header();

?>

<?php if (have_posts()) : ?>



	<?php

	global $wp_query;

	/* ---- Display All Projects ---- */

	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

	$args_manifestation = array('post_type' => 'manifestation', 'post_status' => 'publish', 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged, 'posts_per_page' => 9);

	$manifestation_data = new WP_Query($args_manifestation);

	/* GET Custom Fields For MANIFESTATION PAGE */

	$the_ID = get_the_ID();

	$manifestation_page_title = get_field("manifestation_page_title",$the_ID);

	$manifestation_logo = get_field("manifestation_logo",$the_ID);

	$manifestation_link = get_field("manifestation_link",$the_ID);

	$manifestation_logo_thumb = wp_get_attachment_image_src($manifestation_logo, 'full');

	?>



	<!-- MANIFESTATION SECTION STARTS -->

	<section>

		<div class="m-seaction">

			<div class="main-sec">

				<div class="container">

					<div class="mhed">

						<div class="top-buffer2"></div>

						<p class="s40"><?php echo $manifestation_page_title; ?></p>

						<div class="top-buffer2"></div>

						<div class="mlogo">

							<a href="<?php echo $manifestation_link; ?>" target="_blank"><img src="<?php echo $manifestation_logo_thumb[0]; ?>" alt="<?php echo the_title(); ?>" title="<?php echo the_title(); ?>" /></a>

							<div class="bottom-buffer2"></div>

						</div>

					</div>

					<div class="mbody">

						<div class="row">

							<div class="col-sm-12">

								

								<?php 

								for ($i = 0; $i < count($manifestation_data->posts); $i++) {

									$urlArr = explode("/",get_field("video_link",$manifestation_data->posts[$i]->ID));

									$urlArrNum = count($urlArr);

									$youtubeVideoId = $urlArr[$urlArrNum - 1];

									$thumbURL = 'http://img.youtube.com/vi/'.$youtubeVideoId.'/mqdefault.jpg';

									$thumbs[$i] = wp_get_attachment_image_src(get_post_thumbnail_id($manifestation_data->posts[$i]->ID), array(350, 350));

									if($thumbs[$i][0] != '') { $thumb = $thumbs[$i][0]; } else { $thumb = $thumbURL; }

									?>



									<div class="col-md-4 col-sm-6">

										<div class="vid1">
												<a class="video fancybox-media"  title="<?php echo $manifestation_data->posts[$i]->post_title; ?>" href="<?php echo get_field("video_link",$manifestation_data->posts[$i]->ID); ?>">

												<img src="<?php echo $thumb; ?>"/>

											</a>

											<div class="top-buffer1"></div>

											<p><?php echo $manifestation_data->posts[$i]->post_title; ?></p>
											<div class="top-buffer3"></div>
											<div class="top-buffer2"></div>
										</div>

									</div>

									<?php 

								} ?>



							</div>



							<!-- Pagination Section Starts -->

							<?php

							$big = 999999999;

							$translated = __('Page', 'mytextdomain');

							$pagination = paginate_links(array(

								'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),

								'format' => '?paged=%#%',

								'current' => max(1, get_query_var('paged')),

								'total' => $manifestation_data->max_num_pages,

								'show_all' => TRUE,

								'type' => 'array',

								'prev_text' => __('<span class="clr" aria-hidden="true">&laquo;</span><span class="sr-only clr">Previous</span>'),

								'next_text' => __('<span class="clr" aria-hidden="true">&raquo;</span><span class="sr-only clr">Next</span>'),

								));

							wp_reset_query();

							?>

							<?php if (count($pagination) > 1) { ?>

							<div class="col-sm-12">

								<div class="top-buffer3"></div>

								<nav aria-label="Page navigation">

									<ul class="pagination">		

										<?php

										for ($p = 0; $p < count($pagination); $p++) {

											echo '<li  class="page-item">'.$pagination[$p].'</li>';

										}

										?>	

									</ul>

								</nav>

								<div class="bottom-buffer3"></div>

							</div>

							<?php } ?>

							<!-- Pagination Section Ends -->



						</div>

					</div>

				</div>

			</div>

		</div>

	</section>

	<!-- MANIFESTATION SECTION ENDS -->



<?php else: ?>

	<!-- If Projects not found then -->

	<div class="error"><?php _e('Post Not found.'); ?></div>

<?php endif; ?>



<?php get_footer(); ?>