<?php
get_header();
?>

<!-- PAGE CONTENT SECTION STARTS -->
<section>
    <div class="m-seaction">
        <div class="main-sec">
            <div class="container">
                <div class="mhed">
                    <div class="top-buffer2"></div>
                    <p class="s40">404 Page not found</p>
                    <div class="top-buffer2"></div>                        
                </div>
                <div class="mbody">
                    <div class="row">
                        <div class="col-sm-12" style="padding: 40px 0px 65px;">
                            <h1 class="page-title" style="color: #67a9a5;font-family: BebasNeuewebfont;font-size: 44px;text-align: center;">This is not the webpage you are looking for.</h1>
                            <p style="font-family: BebasNeuewebfont;font-size: 40px;text-align: center;">Please go to <a href="<?php bloginfo('url'); ?>" style="color: #c8485f;">Home</a> and try again!</p>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</section>
<!-- PAGE CONTENT SECTION ENDS -->

<?php get_footer(); ?>