<?php
/*
  Template Name: WWO Thank You
 */
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Thank You</title>
    <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/wwo/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/wwo/fonts/stylesheet.css">
    <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/wwo/css/font-awesome.css">
    <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/wwo/css/t_style.css">
  </head>
  <body>
    <section class="main-section">
      <div class="image">
        <!-- <div class="align-middle d-table-cell height-100vh"> -->
            <div class="container">
              <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="align-middle d-table-cell height-100vh">

                  <div class="content text-center">
                    <span class="line1 d-inline-block w-100 mt-0">THANK You </span>
                    <span class="line2 d-inline-block w-100 mt-3">
                      <p>Your Weekly Wealth Organizer is  on the way.</p>
                    </span>
                    <span class="line7">
                      <p class="mb-0 pt-4">In the meantime,</p>
                    </span>
                    <span class="line6">
                      <p class="mb-0 mt-1">share it with a friend!</p>
                    </span>
                    <div class="share mb-2 mt-2">
                      <span class="symbol"><a href="https://www.facebook.com/sharer.php?u=http://www.agneskowalski.com/weekly-wealth-organizer/"><i class="fa fa-facebook fb" aria-hidden="true"></i></a></span>&nbsp
                      <span class="symbol"><a href="https://twitter.com/share?url=http://www.agneskowalski.com/weekly-wealth-organizer/"><i class="fa fa-twitter fb" aria-hidden="true"></i></a></span>&nbsp
                      <span class="symbol"><a href="http://pinterest.com/pin/create/bookmarklet/?is_video=false&url=http://www.agneskowalski.com/weekly-wealth-organizer/&media=&description=Weekly%20Wealth%20Organizer"><i class="fa fa-pinterest-p fb" aria-hidden="true"></i></a></span>&nbsp
                     <span class="symbol"><a target="_blank" href="mailto:?Subject=Weekly%20Wealth%20Organizer&Body=http://www.agneskowalski.com/weekly-wealth-organizer/"> <i class="fa fa-envelope fb" aria-hidden="true"></i></a></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    <div class="footer">
      <div class="container-fluid">
        <div class="footer-text text-center">
          <i class="fa fa-copyright " aria-hidden="true"></i>2018 All rights reserved
        </div>
      </div>
    </div>
    </section>
    <script href="http://www.agneskowalski.com/wp-content/themes/wwo/js/bootstrap.min.js"></script>
    <script href="http://www.agneskowalski.com/wp-content/themes/wwo/js/jquery-3.3.1.slim.min.js"></script>
  </body>
</html>