<?php

/* Footer Copyright Option Starts */

$arc_option = get_option('arc_option');

$arc_copyright = $arc_option["arc_copyright"];

$arc_footer_block_title = $arc_option["arc_footer_block_title"];

$arc_footer_block_description = $arc_option["arc_footer_block_description"];

$arc_footer_block_url = $arc_option["arc_footer_block_url"];

?>



<!-- BREAKTHROUGH SECTION STARTS -->

<!--<section>

	<div class="seaction9">

		<div class="container">

			<div class="sec9">

				<div class="top-buffer3"></div>

				<div class="top-buffer2"></div>

				<div class="box">

					<div class="top-buffer3"></div>

					<p class="s50"><?php //echo $arc_footer_block_title; ?></p>

					<div class="top-buffer1"></div>

					<a href="<?php //echo $arc_footer_block_url; ?>" target="_blank" class="breakthrough_ride"><p class="s22"><?php //echo $arc_footer_block_description; ?></p></a>

					<div class="bottom-buffer2"></div>

				</div>

				<div class="bottom-buffer2"></div>

				<div class="bottom-buffer3"></div>

			</div>

		</div>

	</div>

</section>-->

<!-- BREAKTHROUGH SECTION ENDS -->





<!-- FOOTER COPYRIGHT TEXT STARTS -->

<footer id="page-footer">

	<div class="container">

		<div class="footer">

			<p><a style="color:#FFF;" href="https://www.agneskowalski.com/privacy-policy/" target="_blank">Privacy Policy</a> | <?php echo $arc_copyright; ?></p>

		</div>

	</div>

</footer>

<!-- FOOTER COPYRIGHT TEXT ENDS -->



</div>



<!-- Attach Script Files Starts -->

<!-- <script type="text/javascript" src="<?php //echo get_template_directory_uri(); ?>/js/jquery.min.js"></script> -->

<!-- Add jQuery library -->

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ie10-viewport-bug-workaround.js"></script>



<!-- Add mousewheel plugin (this is optional) -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/video.js"></script>





<?php wp_footer(); ?>



<script type="text/javascript">

	/* Header Navigation Script Starts */

	$('.header .nav > li').click(function(){

		$('.header .nav>li.hover').toggleClass('hover');

		$(this).addClass('hover');

	});



	$('.header .nav li li').click(function(){

		$('.header .nav li .hover').toggleClass('hover');

		$(this).addClass("hover");

	});



	$(document).ready(function() {

		$('.fancybox-media').fancybox({

			openEffect  : 'none',

			closeEffect : 'none',

			maxWidth : '640',

			height : '385',

			padding : '0',

			title : this.title,

			type : 'iframe',

			helpers : {

				media : {}

			}

		});



		/* Replace White Navigation Images */

		var leftblack = "<?php echo get_template_directory_uri(); ?>/images/left.png";

		var rightblack = "<?php echo get_template_directory_uri(); ?>/images/right.png";

		var leftwhite = "<?php echo get_template_directory_uri(); ?>/images/wleft.png";

		var rightwhite = "<?php echo get_template_directory_uri(); ?>/images/wright.png";

		$left__icon = $(".mailchimp___black img.left__icon");

		$right__icon = $(".mailchimp___black img.right__icon");

		$left__icon.attr('src', leftwhite);

		$right__icon.attr('src', rightwhite);

	});

</script>



<!-- MAILCHIMP VALIDATION SCRIPT STARTS  -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/mc-validate.js"></script>

<!-- <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script> -->

<script type='text/javascript'>

	(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));

	var $mcj = jQuery.noConflict(true);

</script>

<!-- MAILCHIMP VALIDATION SCRIPT ENDS -->

<style>li#menu-item-20{border-right:none !important;}</style>

<style>.money-mindset-guide-header a{color:#333 !important; text-decoration: underline;} .money-mindset-guide-link{color:#FFF !important; text-decoration: underline;}</style>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106731453-1');
</script>
<style>.page-id-9 footer#page-footer{margin-top: 3%;} .page-id-195 footer#page-footer{margin-top: 3%;}</style>
<!-- mouseFlow code start -->
<script type="text/javascript">
  window._mfq = window._mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/1e137333-11b5-4802-a7f6-e79fa3fd619b.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>
<!-- mouseFlow code end -->
</body>

</html>