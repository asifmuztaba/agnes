<?php
/*
  Template Name: PTP Discounts
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
    <link rel="shortcut icon" href="http://www.agneskowalski.com/wp-content/themes/mindsetmogulsmastermind/favicon.ico" type="image/x-icon" />
    <title>Mindset Moguls Mastermind</title>
    <!-- Bootstrap core CSS -->
    <link href="http://www.agneskowalski.com/wp-content/themes/mindsetmogulsmastermind/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.agneskowalski.com/wp-content/themes/mindsetmogulsmastermind/css/style.css" rel="stylesheet">
	<script src="http://www.agneskowalski.com/wp-content/themes/mindsetmogulsmastermind/js/jquery-1.12.4.min.js"></script>
</head>
<body>
<div class="main_div">
<!-- thank you start -->
	<section>
		<div class="seaction5">
			<div class="sec6">
				<div class="container">
					<div class="top-buffer2"></div>
					<div class="ruls col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
					<div class="ruls col-lg-8 col-md-12 col-sm-12 col-xs-12 res-size">
						<div class="rule1 thankyou">
							<h2 class="color-1">$ Mindset Moguls Mastermind $</h2>
							<p class="font-1">For those who are ready to get</p>
							<p class="font-2">Consistent in their Cash Money Consciousness</p>
							<div class="top-buffer2"></div>
							<p class="font-1">The virtual home of intensive</p>
							<h3><a href="#">Prosperity + Purpose Training</a></h3>
							<div class="top-buffer1"></div>
							<p class="font-1">with</p>
							<p class="font-2">Money Mindset Mentor</p>
							<h1>Agnes Kowalski</h1>
							<h4>Mastermind</h4>
							<p class="font-1">A small group of like-minded individuals working <br />towards a definite purpose.</p>
							<h4>That Purpose?</h4>
							<h5><span>Prosperity + Purpose</span> in every area of your life <br /> Breakthroughs via <span>Mindset Mastery</span></h5>
							<div class="top-buffer2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction6">
			<div class="container">
				<div class="top-buffer2"></div>
				<div class="ruls col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
				<div class="ruls col-lg-8 col-md-12 col-sm-12 col-xs-12">
					<h2>Here’s what we’ll do inside</h2>
					<ul>
						<li>High-level mindset strategy (not for the beginner)</li>
						<div class="top-buffer1"></div>
						<li>Support and Accountability for Maintaining and Increasing your Wealth Consciousness</li>
						<div class="top-buffer1"></div>
						<li>Deep dive emotional healing to clearing mindset blocks</li>
						<div class="top-buffer1"></div>
						<li>Laser coaching for deep transformation</li>
					</ul>
				</div>
				<div class="ruls col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
				<div class="clearfix"></div>
				<div class="top-buffer2"></div>
				<p>I show you how to raise the quality of your consciousness in prosperity, wealth and success, which then gives you the ability to change your life 180 or even 360 and to manifest whatever you choose. </p>
				<div class="top-buffer3"></div>
				<div class="top-buffer2"></div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction7">
			<div class="sec7">
				<div class="container">	
					<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
						<div class="top-buffer3"></div>
						<h3>How do I do that?</h3>
						<div class="top-buffer2"></div>
						<ul>
							<li>By recognizing where you need to grow.</li>
							<div class="top-buffer1"></div>
							<li>Showing you how to get out of suffering.</li>
							<div class="top-buffer1"></div>
							<li>Illuminating where you need to increase self-responsibility.</li>
							<div class="top-buffer1"></div>
							<li>Amplifying the concepts you need to embody to make the shifts required for your desires.</li>
							<div class="top-buffer1"></div>
							<li>Training you to tune into the reality you need to align with for the level of outcome you want.</li>
							<div class="top-buffer1"></div>
							<li>Challenge you to step into unconditional love for yourself and others</li>
							<div class="top-buffer1"></div>
							<li>Teach you how to work through deep level emotions that will release money blocks</li>
							<div class="top-buffer1"></div>
							<li>Showing you all the practical methods of aligning with your desired consciousness of prosperity</li>
							<div class="top-buffer1"></div>
						</ul>
						<p>By being infinite <span>me.</span></p>
						<p>Which gives you permission to be infinite <span>you.</span></p>
						<div class="top-buffer3"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction8">
			<div class="container">	
				<div class="top-buffer3"></div>
				<div class="top-buffer2"></div>
				<p>I literally have clients who don’t have to do anything to attract prosperity like Dottie who had been working with me for 2 wks when I got this message:</p>
				<div class="top-buffer3"></div>
				<img src="http://www.agneskowalski.com/wp-content/themes/mindsetmogulsmastermind/images/b-top.png" alt="ad"/>
				<div class="top-buffer3"></div>
				<div class="top-buffer2"></div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction9">
			<div class="container">
				<div class="top-buffer3"></div>
				<h2><i>You next?</i></h2>
				<h3>Oh good. Here’s what you get:</h3>
				<div class="top-buffer2"></div>
				<ul>
					<li>1 small group coaching call per week with each person getting customized laser coaching and homework on each call</li>
					<div class="top-buffer1"></div>
					<li>Unlimited support inside our private FB community, yes you can ask questions daily if you need </li>
					<div class="top-buffer1"></div>
					<li>Unlimited access to Permission to Prosper trainings and mindset modules </li>
					<div class="top-buffer1"></div>
					<li>Learning from other high level mindset participants</li>
					<div class="top-buffer1"></div>
					<li>50% off on all live events</li>
					<div class="top-buffer1"></div>
				</ul>
				<div class="top-buffer2"></div>
				<h4>Investment</h4>
				<div class="top-buffer2"></div>
				<h3>$1000/month with a 3 month minimum, month to month after that.</h3>
				<div class="top-buffer3"></div>
				<h5>I’m ready to get my money mindset RIGHT!</h5>
				<div class="bottom-btn">
					<div class="top-buffer2"></div>
					<p><i>For PTP Graduates</i></p>
					<div class="sub-button">
												<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=UDNEMH2KZ5W5N
														   " target="_blank"> <img src="http://www.agneskowalski.com/wp-content/themes/mindsetmogulsmastermind/images/left.png"><button type="submit">JOIN HERE</button><img src="http://www.agneskowalski.com/wp-content/themes/mindsetmogulsmastermind/images/right.png"></a>
					</div>
				<div>
				<div class="top-buffer3"></div>
			</div>
		</div>
			</div>
		</div>
	</section>
	
	<section>
		<div class="seaction10">
			<div class="container">
				<p>© 2017 All rights reserved</p>
			</div>
		</div>
	</section>

</div>
<style>
@media (max-width:1350px){
 .sec6{background-image:none; min-height:auto;}
 .res-size{width:100%;}
}
@media (max-width:1199px){
 .sec7{background-image:none; min-height:auto;}
}
</style>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106731453-1');
</script>
</body>
</html>