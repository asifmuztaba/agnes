<?php

/*

Template Name: Home Page

*/

get_header();

?>



<?php if (have_posts()) : ?>

	<?php while (have_posts()) : the_post(); ?>

		<?php

		/* GET THE WORK WITH ME SECTION CUSTOM FIELD DETAILS */

		$the_ID = get_the_ID();

		/* GET INTRODUCTION SECTION CUSTOM FIELD DETAILS */

		$introduction_heading_1 = get_field("introduction_heading_1",$the_ID);

		$introduction_heading_2 = get_field("introduction_heading_2",$the_ID);

		$introduction_heading_3 = get_field("introduction_heading_3",$the_ID);

		$introduction_title_1 = get_field("introduction_title_1",$the_ID);

		$introduction_title_2 = get_field("introduction_title_2",$the_ID);

		$introduction_title_author = get_field("introduction_title_author",$the_ID);

		$introduction_title_description = get_field("introduction_title_description",$the_ID);

		$introduction_information = get_field("introduction_information",$the_ID);		

		$introduction_left_side_image = get_field("introduction_left_side_image",$the_ID);

		$introduction_left_side_image_thumb = wp_get_attachment_image_src($introduction_left_side_image, 'full');

		$newsletter_heading_1 = get_field("newsletter_heading_1",$the_ID);

		$newsletter_heading_2 = get_field("newsletter_heading_2",$the_ID);

		?>

		

		<!-- INTRODUCTION SECTION STARTS -->

		<section>

			<div class="seaction2">

				<div class="sec1">

					<div class="set2 pull-right">

						<div class="tag1">

							<div class="top-buffer1"></div>

							<p class="s36"><?php echo $introduction_heading_1; ?></p>

							<p class="s25"><?php echo $introduction_heading_2; ?></p>

							<p class="eual"><?php echo $introduction_heading_3; ?></p>

							<p class="s94"><?php echo $introduction_title_1; ?></p>

							<div class="top-buffer1"></div>

							<p class="s20"><i><?php echo $introduction_title_2; ?></i></p>

							<p class="s76"><?php echo $introduction_title_author; ?></p>

							<div class="top-buffer"></div>

							<p class="s18"><?php echo $introduction_title_description; ?></p>
<div class="top-buffer1"></div>
<p class="as-seen-forbes">As Featured In <a href="https://www.forbes.com/sites/gingerdean/2017/08/30/4-ways-this-wealth-therapist-whips-her-six-figure-entrepreneurs-into-money-making-machines/#76f6bdbd2c60" target="_blank"><img src="http://www.agneskowalski.com/wp-content/uploads/2017/09/Forbes.png" class="img-responsive" /></a></p>
						</div>
						<div class="tag2">
						</div>



						<div class="tag2">

							<div class="tag2inn">

								<div class="top-buffer2" style="display:inline-block;"></div>

								<div class="top-buffer2"></div>

								<p class="s23"><?php echo $newsletter_heading_1; ?></p>

								<p class="s36"><?php echo $newsletter_heading_2; ?></p>

								<!-- ActiveCampaign Integrated Form Starts -->
								<form method="POST" action="https://agneskowalskiinc.activehosted.com/proc.php" id="_form_5_" class="_form _form_5 _inline-form  _dark" novalidate>
  <input type="hidden" name="u" value="5" />
  <input type="hidden" name="f" value="5" />
  <input type="hidden" name="s" />
  <input type="hidden" name="c" value="0" />
  <input type="hidden" name="m" value="0" />
  <input type="hidden" name="act" value="sub" />
  <input type="hidden" name="v" value="2" />
      									<div class="input1 pull-left">
										<input type="text" name="firstname" placeholder="First Name" required/>
									</div>
									<div class="input1 pull-right">
										<input type="text" name="email" placeholder="Email Address" required/>
									</div>									
									<div class="sub-button">
										<img src="<?php echo get_template_directory_uri(); ?>/images/left.png">
										<input type="submit" value="Abundance Now!" name="subscribe" id="_form_5_submit" class="button mailchimp___submit" />
										<img src="<?php echo get_template_directory_uri(); ?>/images/right.png">
									</div>
									
    <div class="_form-thank-you" style="display:none;">
  </div>
								</form><script type="text/javascript">
window.cfields = [];
window._show_thank_you = function(id, message, trackcmp_url) {
  var form = document.getElementById('_form_' + id + '_'), thank_you = form.querySelector('._form-thank-you');
  form.querySelector('._form-content').style.display = 'none';
  thank_you.innerHTML = message;
  thank_you.style.display = 'block';
  if (typeof(trackcmp_url) != 'undefined' && trackcmp_url) {
    // Site tracking URL to use after inline form submission.
    _load_script(trackcmp_url);
  }
  if (typeof window._form_callback !== 'undefined') window._form_callback(id);
};
window._show_error = function(id, message, html) {
  var form = document.getElementById('_form_' + id + '_'), err = document.createElement('div'), button = form.querySelector('button'), old_error = form.querySelector('._form_error');
  if (old_error) old_error.parentNode.removeChild(old_error);
  err.innerHTML = message;
  err.className = '_error-inner _form_error _no_arrow';
  var wrapper = document.createElement('div');
  wrapper.className = '_form-inner';
  wrapper.appendChild(err);
  button.parentNode.insertBefore(wrapper, button);
  document.querySelector('[id^="_form"][id$="_submit"]').disabled = false;
  if (html) {
    var div = document.createElement('div');
    div.className = '_error-html';
    div.innerHTML = html;
    err.appendChild(div);
  }
};
window._load_script = function(url, callback) {
    var head = document.querySelector('head'), script = document.createElement('script'), r = false;
    script.type = 'text/javascript';
    script.charset = 'utf-8';
    script.src = url;
    if (callback) {
      script.onload = script.onreadystatechange = function() {
      if (!r && (!this.readyState || this.readyState == 'complete')) {
        r = true;
        callback();
        }
      };
    }
    head.appendChild(script);
};
(function() {
  if (window.location.search.search("excludeform") !== -1) return false;
  var getCookie = function(name) {
    var match = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]+)'));
    return match ? match[2] : null;
  }
  var setCookie = function(name, value) {
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 1000 * 60 * 60 * 24 * 365;
    now.setTime(expireTime);
    document.cookie = name + '=' + value + '; expires=' + now + ';path=/';
  }
      var addEvent = function(element, event, func) {
    if (element.addEventListener) {
      element.addEventListener(event, func);
    } else {
      var oldFunc = element['on' + event];
      element['on' + event] = function() {
        oldFunc.apply(this, arguments);
        func.apply(this, arguments);
      };
    }
  }
  var _removed = false;
  var form_to_submit = document.getElementById('_form_5_');
  var allInputs = form_to_submit.querySelectorAll('input, select, textarea'), tooltips = [], submitted = false;

  var getUrlParam = function(name) {
    var regexStr = '[\?&]' + name + '=([^&#]*)';
    var results = new RegExp(regexStr, 'i').exec(window.location.href);
    return results != undefined ? decodeURIComponent(results[1]) : false;
  };

  for (var i = 0; i < allInputs.length; i++) {
    var regexStr = "field\\[(\\d+)\\]";
    var results = new RegExp(regexStr).exec(allInputs[i].name);
    if (results != undefined) {
      allInputs[i].dataset.name = window.cfields[results[1]];
    } else {
      allInputs[i].dataset.name = allInputs[i].name;
    }
    var fieldVal = getUrlParam(allInputs[i].dataset.name);

    if (fieldVal) {
      if (allInputs[i].type == "radio" || allInputs[i].type == "checkbox") {
        if (allInputs[i].value == fieldVal) {
          allInputs[i].checked = true;
        }
      } else {
        allInputs[i].value = fieldVal;
      }
    }
  }

  var remove_tooltips = function() {
    for (var i = 0; i < tooltips.length; i++) {
      tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
    }
      tooltips = [];
  };
  var remove_tooltip = function(elem) {
    for (var i = 0; i < tooltips.length; i++) {
      if (tooltips[i].elem === elem) {
        tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
        tooltips.splice(i, 1);
        return;
      }
    }
  };
  var create_tooltip = function(elem, text) {
    var tooltip = document.createElement('div'), arrow = document.createElement('div'), inner = document.createElement('div'), new_tooltip = {};
    if (elem.type != 'radio' && elem.type != 'checkbox') {
      tooltip.className = '_error';
      arrow.className = '_error-arrow';
      inner.className = '_error-inner';
      inner.innerHTML = text;
      tooltip.appendChild(arrow);
      tooltip.appendChild(inner);
      elem.parentNode.appendChild(tooltip);
    } else {
      tooltip.className = '_error-inner _no_arrow';
      tooltip.innerHTML = text;
      elem.parentNode.insertBefore(tooltip, elem);
      new_tooltip.no_arrow = true;
    }
    new_tooltip.tip = tooltip;
    new_tooltip.elem = elem;
    tooltips.push(new_tooltip);
    return new_tooltip;
  };
  var resize_tooltip = function(tooltip) {
    var rect = tooltip.elem.getBoundingClientRect();
    var doc = document.documentElement, scrollPosition = rect.top - ((window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0));
    if (scrollPosition < 40) {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _below';
    } else {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _above';
    }
  };
  var resize_tooltips = function() {
    if (_removed) return;
    for (var i = 0; i < tooltips.length; i++) {
      if (!tooltips[i].no_arrow) resize_tooltip(tooltips[i]);
    }
  };
  var validate_field = function(elem, remove) {
    var tooltip = null, value = elem.value, no_error = true;
    remove ? remove_tooltip(elem) : false;
    if (elem.type != 'checkbox') elem.className = elem.className.replace(/ ?_has_error ?/g, '');
    if (elem.getAttribute('required') !== null) {
      if (elem.type == 'radio' || (elem.type == 'checkbox' && /any/.test(elem.className))) {
        var elems = form_to_submit.elements[elem.name];
        if (!(elems instanceof NodeList || elems instanceof HTMLCollection) || elems.length <= 1) {
          no_error = elem.checked;
        }
        else {
          no_error = false;
          for (var i = 0; i < elems.length; i++) {
            if (elems[i].checked) no_error = true;
          }
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (elem.type =='checkbox') {
        var elems = form_to_submit.elements[elem.name], found = false, err = [];
        no_error = true;
        for (var i = 0; i < elems.length; i++) {
          if (elems[i].getAttribute('required') === null) continue;
          if (!found && elems[i] !== elem) return true;
          found = true;
          elems[i].className = elems[i].className.replace(/ ?_has_error ?/g, '');
          if (!elems[i].checked) {
            no_error = false;
            elems[i].className = elems[i].className + ' _has_error';
            err.push("Checking %s is required".replace("%s", elems[i].value));
          }
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, err.join('<br/>'));
        }
      } else if (elem.tagName == 'SELECT') {
        var selected = true;
        if (elem.multiple) {
          selected = false;
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected) {
              selected = true;
              break;
            }
          }
        } else {
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected && !elem.options[i].value) {
              selected = false;
            }
          }
        }
        if (!selected) {
          elem.className = elem.className + ' _has_error';
          no_error = false;
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (value === undefined || value === null || value === '') {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "This field is required.");
      }
    }
    if (no_error && elem.name == 'email') {
      if (!value.match(/^[\+_a-z0-9-'&=]+(\.[\+_a-z0-9-']+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid email address.");
      }
    }
    if (no_error && /date_field/.test(elem.className)) {
      if (!value.match(/^\d\d\d\d-\d\d-\d\d$/)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid date.");
      }
    }
    tooltip ? resize_tooltip(tooltip) : false;
    return no_error;
  };
  var needs_validate = function(el) {
    return el.name == 'email' || el.getAttribute('required') !== null;
  };
  var validate_form = function(e) {
    var err = form_to_submit.querySelector('._form_error'), no_error = true;
    if (!submitted) {
      submitted = true;
      for (var i = 0, len = allInputs.length; i < len; i++) {
        var input = allInputs[i];
        if (needs_validate(input)) {
          if (input.type == 'text') {
            addEvent(input, 'blur', function() {
              this.value = this.value.trim();
              validate_field(this, true);
            });
            addEvent(input, 'input', function() {
              validate_field(this, true);
            });
          } else if (input.type == 'radio' || input.type == 'checkbox') {
            (function(el) {
              var radios = form_to_submit.elements[el.name];
              for (var i = 0; i < radios.length; i++) {
                addEvent(radios[i], 'click', function() {
                  validate_field(el, true);
                });
              }
            })(input);
          } else if (input.tagName == 'SELECT') {
            addEvent(input, 'change', function() {
              validate_field(this, true);
            });
          }
        }
      }
    }
    remove_tooltips();
    for (var i = 0, len = allInputs.length; i < len; i++) {
      var elem = allInputs[i];
      if (needs_validate(elem)) {
        if (elem.tagName.toLowerCase() !== "select") {
          elem.value = elem.value.trim();
        }
        validate_field(elem) ? true : no_error = false;
      }
    }
    if (!no_error && e) {
      e.preventDefault();
    }
    resize_tooltips();
    return no_error;
  };
  addEvent(window, 'resize', resize_tooltips);
  addEvent(window, 'scroll', resize_tooltips);
  window._old_serialize = null;
  if (typeof serialize !== 'undefined') window._old_serialize = window.serialize;
  _load_script("//d3rxaij56vjege.cloudfront.net/form-serialize/0.3/serialize.min.js", function() {
    window._form_serialize = window.serialize;
    if (window._old_serialize) window.serialize = window._old_serialize;
  });
  var form_submit = function(e) {
    e.preventDefault();
    if (validate_form()) {
      // use this trick to get the submit button & disable it using plain javascript
      document.querySelector('[id^="_form"][id$="_submit"]').disabled = true;
            var serialized = _form_serialize(document.getElementById('_form_5_'));
      var err = form_to_submit.querySelector('._form_error');
      err ? err.parentNode.removeChild(err) : false;
      _load_script('https://agneskowalskiinc.activehosted.com/proc.php?' + serialized + '&jsonp=true');
    }
    return false;
  };
  addEvent(form_to_submit, 'submit', form_submit);
})();

</script>
								<!-- ActiveCampaign Integrated Form Ends -->
								
								<p class="s18"><?php echo $introduction_information; ?></p>

								<div class="bottom-buffer3"></div>

								<div class="bottom-buffer2" style="display:inline-block;"></div>

							</div>

						</div>



					</div>

					<div class="set1 pull-left">

						<img src="<?php echo $introduction_left_side_image_thumb[0]; ?>"/>

					</div>

				</div>

			</div>

		</section>

		<!-- INTRODUCTION SECTION ENDS -->



		<?php

		/* GET ISSUE SECTION CUSTOM FIELD DETAILS */

		$issue_heading_1 = get_field("issue_heading_1",$the_ID);

		$issue_heading_2 = get_field("issue_heading_2",$the_ID);

		$issue_heading_3 = get_field("issue_heading_3",$the_ID);

		$issue_heading_4 = get_field("issue_heading_4",$the_ID);

		$issue_section_title_1 = get_field("issue_section_title_1",$the_ID);

		$issue_section_title_2 = get_field("issue_section_title_2",$the_ID);

		$issue_section_title_3 = get_field("issue_section_title_3",$the_ID);

		$issues = get_field("issues",$the_ID);

		$issue_highlighter_text = get_field("issue_highlighter_text",$the_ID);

		$issue_background = get_field("issue_background",$the_ID);

		$issue_background_thumb = wp_get_attachment_image_src($issue_background, 'full');

		?>

		<style>

			.seaction3 .sec3 {

				background-image: url(<?php echo $issue_background_thumb[0]; ?>);

			}

		</style>

		<!-- ISSUE SECTION STARTS -->

		<section>

			<div class="seaction3">

				<div class="sec3">

					<div class="container">

						<div class="set1">

							<div class="top-buffer3"></div>

							<p class="s27"><?php echo $issue_heading_1; ?></p>

							<p class="s34"><?php echo $issue_heading_2; ?></p>

							<p class="s27"><?php echo $issue_heading_3; ?></p>

							<p class="s34"><?php echo $issue_heading_4; ?></p>

							<div class="bottom-buffer3"></div>

							<div class="bottom-buffer2"></div>

						</div>

					</div>

					<div class="set2">

						<div class="container">

							<div class="centr">

								<div class="top-buffer3"></div>

								<div class="top-buffer1"></div>

								<p class="s27"><?php echo $issue_section_title_1; ?></p>

								<p class="s68"><i><?php echo $issue_section_title_2; ?></i></p>

								<p class="s27"><?php echo $issue_section_title_3; ?></p>

								<div class="top-buffer1"></div>

								<?php 

								for($w=0;$w<count($issues);$w++) {

									?>

									<p class="p1"><?php echo strip_tags($issues[$w]['issue'], "<strong>"); ?></p>									

									<?php

								} ?>								

								<div class="top-buffer1"></div>

								<p class="p1 lst"><?php echo strip_tags($issue_highlighter_text, "<strong><i>"); ?></p>

								<div class="bottom-buffer3"></div>

								<div class="bottom-buffer1"></div>

							</div>

						</div>

					</div>

					<div class="bottom-buffer3"></div>

					<div class="bottom-buffer2" style="display:inline-block;"></div>

				</div>

			</div>

		</section>

		<!-- ISSUE SECTION ENDS -->





		<?php

		/* GET YOU ARE NOT DESTINED SECTION CUSTOM FIELD DETAILS */

		$section_title = get_field("section_title",$the_ID);

		$section_words = get_field("section_words",$the_ID);

		$section_highlighter_word = get_field("section_highlighter_word",$the_ID);

		$section_description_1 = get_field("section_description_1",$the_ID);

		$section_description_2 = get_field("section_description_2",$the_ID);

		?>

		<!-- YOU ARE NOT DESTINED SECTION STARTS -->

		<section>

			<div class="seaction4">

				<div class="container">

					<div class="top-bufferr3"></div>

					<div class="sec4">

						<h3><?php echo $section_title; ?></h3>

						<div class="menu">

							<?php 

							for($x=0;$x<count($section_words);$x++) {

								?>

								<p><?php echo $section_words[$x]['section_word']; ?></p>									

								<?php

							} ?>

						</div> 

						<h3><?php echo $section_highlighter_word; ?></h3>

						<div class="top-buffer3"></div>

						<p class="s35"><?php echo $section_description_1; ?></p>

						<div class="top-buffer1"></div>

						<p class="s23"><?php echo $section_description_2; ?></p>

						<div class="bottom-buffer2"></div>

					</div>

				</div>

			</div>

		</section>

		<!-- YOU ARE NOT DESTINED SECTION ENDS -->





		<?php

		/* GET THE Mindset Coaching SECTION CUSTOM FIELD DETAILS */

		$mindset_section_title = get_field("mindset_section_title",$the_ID);

		$mindset_word = get_field("mindset_word",$the_ID);

		$mindset_pronounciation = get_field("mindset_pronounciation",$the_ID);

		$mindset_type = get_field("mindset_type",$the_ID);

		$mindset_meaning = get_field("mindset_meaning",$the_ID);

		$mindset_description = get_field("mindset_description",$the_ID);

		?>

		<!-- MINDSET Coaching Section Starts -->

		<section>

			<div class="seaction5">

				<div class="sec5">

					<div class="container">

						<div class="top-buffer2" style="display:inline-block;"></div>

						<p class="s38"><?php echo strip_tags($mindset_section_title, "<strong><i>"); ?></p>

						<div class="top-buffer1"></div>

						<div class="box">

							<p class="s50"><?php echo $mindset_word; ?></p>

							<p class="s20 one"><?php echo $mindset_pronounciation; ?></p>

							<p class="s25"><?php echo $mindset_type; ?></p>

							<p class="s20 ita"><i><?php echo $mindset_meaning; ?></i></p>

						</div>

						<div class="top-buffer2"></div>

						<p class="s23"><?php echo $mindset_description; ?></p>

						<div class="bottom-buffer3"></div>

					</div>

				</div>

			</div>

		</section>

		<!-- Mindset Coaching Section Ends -->





		<?php

		/* GET THE FEATURED IN SECTION CUSTOM FIELD DETAILS */

		$featured_in_section_title = get_field("featured_in_section_title",$the_ID);

		$youtube_video_embed = get_field("youtube_video_embed",$the_ID);

		$youtube_video_caption = get_field("youtube_video_caption",$the_ID);

		$logos_featured = get_field("logos_featured",$the_ID);

		?>

		<!-- Featured In SECTION STARTS -->

		<section>

			<div class="seaction6">

				<div class="container">

					<div class="sec6">

						<div class="set1" style="display:inline-block;">

							<div class="top-buffer3"></div>

							<h1><?php echo $featured_in_section_title; ?></h1>

							<ul class="nav navbar-nav">

								<?php 

								for($y=0;$y<count($logos_featured);$y++) {									

									$logo_thumb = wp_get_attachment_image_src($logos_featured[$y]['logo_featured'], 'full');

									?>

									<li style="width:<?php echo $logos_featured[$y]['logo_width']; ?>%;"><a href="<?php echo $logos_featured[$y]['url_link']; ?>"><img src="<?php echo $logo_thumb[0]; ?>"/></a></li>									

									<?php

								} ?>

							</ul> 

						</div>

						<div class="video">

							<div class="top-buffer3"></div>

							<div class="top-buffer2"></div>

							<object width="100%" height="315" data="<?php echo $youtube_video_embed; ?>"></object>

							<div class="bottom-buffer2"></div>

						</div>

						<p class="s25"><?php echo strip_tags($youtube_video_caption, "<strong><i>"); ?></p>

					</div>

					<div class="bottom-buffer3"></div>

					<div class="bottom-buffer1"></div>

				</div>

			</div>

		</section>

		<!-- Featured In SECTION ENDS --> 





		<?php

		/* GET THE MY CLIENTS SECTION CUSTOM FIELD DETAILS */

		$clients_section_title = get_field("clients_section_title",$the_ID);

		$author_name = get_field("author_name",$the_ID);

		$author_designation = get_field("author_designation",$the_ID);

		/* --- ENDS --- */

		/* GET CLIENT LISTS */

		$clients_args = array(

			'posts_per_page' => -1,

			'post_type' => 'clients',

			'orderby'   => 'menu_order',

			'order'     => 'ASC',

			'post_status' => 'publish',

			'suppress_filters' => true

			);

		$clients_array = get_posts($clients_args);

		?>

		<!-- TESTIMONIAL SECTION STARTS FROM HERE -->

		<section>

			<div class="seaction7">

				<div class="sec7">

					<div class="top-buffer2"></div>

					<p class="s36"><?php echo $clients_section_title; ?></p>

					<div class="top-buffer1"></div>

					<p class="s76"><?php echo $author_name; ?></p>

					<p class="s23"><?php echo $author_designation; ?></p>

					<div class="slider">

						<div class="container">

							<br>

							<div id="myCarousel" class="carousel slide" data-ride="carousel">

								<!-- Wrapper FOR TESTIMONIAL SLIDES -->

								<div class="carousel-inner" role="listbox">

									<?php

									$z = 0;

									foreach ($clients_array as $clients_value) {

										$designation = get_field("designation",$clients_value->ID);

										$attr = array(

											'alt' => trim(strip_tags($clients_value->post_title)),

											'title' => trim(strip_tags($clients_value->post_title)),

											);

										$client_image = get_post_thumbnail_id($clients_value->ID);										

										$client_image_thumb = wp_get_attachment_image_src($client_image, array(150,150));

										?>

										<div class="item <?php if($z==0){echo ' active';} ?>">

											<img src="<?php echo $client_image_thumb[0]; ?>" alt="<?php echo $clients_value->post_title; ?>" width="140px" height="140px" />

											<div class="top-buffer1"></div>

											<div class="discript">

												<p class="s18"><i><?php echo  nl2br($clients_value->post_content); ?></i></p>

												<div class="top-buffer1"></div>

												<p class="s20"><?php echo $clients_value->post_title; ?></p>

												<?php if($designation != '') { echo '<p class="s16">'.$designation.'</p>'; } ?>

											</div>

										</div>

										<?php 

										$z++;

									} ?>



								</div>

								<!-- LEFT AND RIGHT SLIDE CONTROLS -->

								<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">

									<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>

									<span class="sr-only">Previous</span>

								</a>

								<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">

									<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>

									<span class="sr-only">Next</span>

								</a>

							</div>

						</div>  

					</div>

					<div class="bottom-buffer3" style="display:inline-block;"></div>

				</div>

			</div>

		</section>

		<!-- TESTIMONIAL SECTION ENDS HERE -->



		<!-- NEWSLETTER SECTION STARTS -->

		<section>

			<div class="seaction8 mailchimp___black">

				<div class="container">

					<div class="sec8">

						<div class="top-buffer3"></div>

						<div class="top-buffer3"></div>

						<div class="box">

							<div class="tag2inn">

								<div class="top-buffer3" style="display:inline-block;"></div>

								<div class="top-buffer2"></div>

								<p class="s23"><?php echo $newsletter_heading_1; ?></p>

								<div class="top-buffer"></div>

								<p class="s36"><?php echo $newsletter_heading_2; ?></p>								

								<?php  /* echo do_shortcode('[mc4wp_form id="150"]'); */ ?>

								<!-- ActiveCampaign Form Starts -->
								<form method="POST" action="https://agneskowalskiinc.activehosted.com/proc.php" id="_form_5_" class="_form _form_5 _inline-form  _dark" novalidate>
  <input type="hidden" name="u" value="5" />
  <input type="hidden" name="f" value="5" />
  <input type="hidden" name="s" />
  <input type="hidden" name="c" value="0" />
  <input type="hidden" name="m" value="0" />
  <input type="hidden" name="act" value="sub" />
  <input type="hidden" name="v" value="2" />
  <div class="_form-content">
    <div class="_form_element _x72609544 _full_width " >
								<div class="top-buffer2"></div>
									<div class="input1 pull-left">
										<input type="text" name="firstname" placeholder="First Name" required/>
									</div>
									<div class="input1 pull-right">
										 <input type="text" name="email" placeholder="Email Address" required/>
									</div>
									
									<div class="sub-button">
										<img class="left__icon" src="<?php echo get_template_directory_uri(); ?>/images/wleft.png">
										<input type="submit" value="Abundance Now!" name="subscribe" id="mc-embedded-subscribe2" class="button mailchimp___submit" />
										<img class="right__icon" src="<?php echo get_template_directory_uri(); ?>/images/wright.png">
									</div>
									<div class="_form-thank-you" style="display:none;">
  </div>
								</form><script type="text/javascript">
window.cfields = [];
window._show_thank_you = function(id, message, trackcmp_url) {
  var form = document.getElementById('_form_' + id + '_'), thank_you = form.querySelector('._form-thank-you');
  form.querySelector('._form-content').style.display = 'none';
  thank_you.innerHTML = message;
  thank_you.style.display = 'block';
  if (typeof(trackcmp_url) != 'undefined' && trackcmp_url) {
    // Site tracking URL to use after inline form submission.
    _load_script(trackcmp_url);
  }
  if (typeof window._form_callback !== 'undefined') window._form_callback(id);
};
window._show_error = function(id, message, html) {
  var form = document.getElementById('_form_' + id + '_'), err = document.createElement('div'), button = form.querySelector('button'), old_error = form.querySelector('._form_error');
  if (old_error) old_error.parentNode.removeChild(old_error);
  err.innerHTML = message;
  err.className = '_error-inner _form_error _no_arrow';
  var wrapper = document.createElement('div');
  wrapper.className = '_form-inner';
  wrapper.appendChild(err);
  button.parentNode.insertBefore(wrapper, button);
  document.querySelector('[id^="_form"][id$="_submit"]').disabled = false;
  if (html) {
    var div = document.createElement('div');
    div.className = '_error-html';
    div.innerHTML = html;
    err.appendChild(div);
  }
};
window._load_script = function(url, callback) {
    var head = document.querySelector('head'), script = document.createElement('script'), r = false;
    script.type = 'text/javascript';
    script.charset = 'utf-8';
    script.src = url;
    if (callback) {
      script.onload = script.onreadystatechange = function() {
      if (!r && (!this.readyState || this.readyState == 'complete')) {
        r = true;
        callback();
        }
      };
    }
    head.appendChild(script);
};
(function() {
  if (window.location.search.search("excludeform") !== -1) return false;
  var getCookie = function(name) {
    var match = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]+)'));
    return match ? match[2] : null;
  }
  var setCookie = function(name, value) {
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 1000 * 60 * 60 * 24 * 365;
    now.setTime(expireTime);
    document.cookie = name + '=' + value + '; expires=' + now + ';path=/';
  }
      var addEvent = function(element, event, func) {
    if (element.addEventListener) {
      element.addEventListener(event, func);
    } else {
      var oldFunc = element['on' + event];
      element['on' + event] = function() {
        oldFunc.apply(this, arguments);
        func.apply(this, arguments);
      };
    }
  }
  var _removed = false;
  var form_to_submit = document.getElementById('_form_5_');
  var allInputs = form_to_submit.querySelectorAll('input, select, textarea'), tooltips = [], submitted = false;

  var getUrlParam = function(name) {
    var regexStr = '[\?&]' + name + '=([^&#]*)';
    var results = new RegExp(regexStr, 'i').exec(window.location.href);
    return results != undefined ? decodeURIComponent(results[1]) : false;
  };

  for (var i = 0; i < allInputs.length; i++) {
    var regexStr = "field\\[(\\d+)\\]";
    var results = new RegExp(regexStr).exec(allInputs[i].name);
    if (results != undefined) {
      allInputs[i].dataset.name = window.cfields[results[1]];
    } else {
      allInputs[i].dataset.name = allInputs[i].name;
    }
    var fieldVal = getUrlParam(allInputs[i].dataset.name);

    if (fieldVal) {
      if (allInputs[i].type == "radio" || allInputs[i].type == "checkbox") {
        if (allInputs[i].value == fieldVal) {
          allInputs[i].checked = true;
        }
      } else {
        allInputs[i].value = fieldVal;
      }
    }
  }

  var remove_tooltips = function() {
    for (var i = 0; i < tooltips.length; i++) {
      tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
    }
      tooltips = [];
  };
  var remove_tooltip = function(elem) {
    for (var i = 0; i < tooltips.length; i++) {
      if (tooltips[i].elem === elem) {
        tooltips[i].tip.parentNode.removeChild(tooltips[i].tip);
        tooltips.splice(i, 1);
        return;
      }
    }
  };
  var create_tooltip = function(elem, text) {
    var tooltip = document.createElement('div'), arrow = document.createElement('div'), inner = document.createElement('div'), new_tooltip = {};
    if (elem.type != 'radio' && elem.type != 'checkbox') {
      tooltip.className = '_error';
      arrow.className = '_error-arrow';
      inner.className = '_error-inner';
      inner.innerHTML = text;
      tooltip.appendChild(arrow);
      tooltip.appendChild(inner);
      elem.parentNode.appendChild(tooltip);
    } else {
      tooltip.className = '_error-inner _no_arrow';
      tooltip.innerHTML = text;
      elem.parentNode.insertBefore(tooltip, elem);
      new_tooltip.no_arrow = true;
    }
    new_tooltip.tip = tooltip;
    new_tooltip.elem = elem;
    tooltips.push(new_tooltip);
    return new_tooltip;
  };
  var resize_tooltip = function(tooltip) {
    var rect = tooltip.elem.getBoundingClientRect();
    var doc = document.documentElement, scrollPosition = rect.top - ((window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0));
    if (scrollPosition < 40) {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _below';
    } else {
      tooltip.tip.className = tooltip.tip.className.replace(/ ?(_above|_below) ?/g, '') + ' _above';
    }
  };
  var resize_tooltips = function() {
    if (_removed) return;
    for (var i = 0; i < tooltips.length; i++) {
      if (!tooltips[i].no_arrow) resize_tooltip(tooltips[i]);
    }
  };
  var validate_field = function(elem, remove) {
    var tooltip = null, value = elem.value, no_error = true;
    remove ? remove_tooltip(elem) : false;
    if (elem.type != 'checkbox') elem.className = elem.className.replace(/ ?_has_error ?/g, '');
    if (elem.getAttribute('required') !== null) {
      if (elem.type == 'radio' || (elem.type == 'checkbox' && /any/.test(elem.className))) {
        var elems = form_to_submit.elements[elem.name];
        if (!(elems instanceof NodeList || elems instanceof HTMLCollection) || elems.length <= 1) {
          no_error = elem.checked;
        }
        else {
          no_error = false;
          for (var i = 0; i < elems.length; i++) {
            if (elems[i].checked) no_error = true;
          }
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (elem.type =='checkbox') {
        var elems = form_to_submit.elements[elem.name], found = false, err = [];
        no_error = true;
        for (var i = 0; i < elems.length; i++) {
          if (elems[i].getAttribute('required') === null) continue;
          if (!found && elems[i] !== elem) return true;
          found = true;
          elems[i].className = elems[i].className.replace(/ ?_has_error ?/g, '');
          if (!elems[i].checked) {
            no_error = false;
            elems[i].className = elems[i].className + ' _has_error';
            err.push("Checking %s is required".replace("%s", elems[i].value));
          }
        }
        if (!no_error) {
          tooltip = create_tooltip(elem, err.join('<br/>'));
        }
      } else if (elem.tagName == 'SELECT') {
        var selected = true;
        if (elem.multiple) {
          selected = false;
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected) {
              selected = true;
              break;
            }
          }
        } else {
          for (var i = 0; i < elem.options.length; i++) {
            if (elem.options[i].selected && !elem.options[i].value) {
              selected = false;
            }
          }
        }
        if (!selected) {
          elem.className = elem.className + ' _has_error';
          no_error = false;
          tooltip = create_tooltip(elem, "Please select an option.");
        }
      } else if (value === undefined || value === null || value === '') {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "This field is required.");
      }
    }
    if (no_error && elem.name == 'email') {
      if (!value.match(/^[\+_a-z0-9-'&=]+(\.[\+_a-z0-9-']+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid email address.");
      }
    }
    if (no_error && /date_field/.test(elem.className)) {
      if (!value.match(/^\d\d\d\d-\d\d-\d\d$/)) {
        elem.className = elem.className + ' _has_error';
        no_error = false;
        tooltip = create_tooltip(elem, "Enter a valid date.");
      }
    }
    tooltip ? resize_tooltip(tooltip) : false;
    return no_error;
  };
  var needs_validate = function(el) {
    return el.name == 'email' || el.getAttribute('required') !== null;
  };
  var validate_form = function(e) {
    var err = form_to_submit.querySelector('._form_error'), no_error = true;
    if (!submitted) {
      submitted = true;
      for (var i = 0, len = allInputs.length; i < len; i++) {
        var input = allInputs[i];
        if (needs_validate(input)) {
          if (input.type == 'text') {
            addEvent(input, 'blur', function() {
              this.value = this.value.trim();
              validate_field(this, true);
            });
            addEvent(input, 'input', function() {
              validate_field(this, true);
            });
          } else if (input.type == 'radio' || input.type == 'checkbox') {
            (function(el) {
              var radios = form_to_submit.elements[el.name];
              for (var i = 0; i < radios.length; i++) {
                addEvent(radios[i], 'click', function() {
                  validate_field(el, true);
                });
              }
            })(input);
          } else if (input.tagName == 'SELECT') {
            addEvent(input, 'change', function() {
              validate_field(this, true);
            });
          }
        }
      }
    }
    remove_tooltips();
    for (var i = 0, len = allInputs.length; i < len; i++) {
      var elem = allInputs[i];
      if (needs_validate(elem)) {
        if (elem.tagName.toLowerCase() !== "select") {
          elem.value = elem.value.trim();
        }
        validate_field(elem) ? true : no_error = false;
      }
    }
    if (!no_error && e) {
      e.preventDefault();
    }
    resize_tooltips();
    return no_error;
  };
  addEvent(window, 'resize', resize_tooltips);
  addEvent(window, 'scroll', resize_tooltips);
  window._old_serialize = null;
  if (typeof serialize !== 'undefined') window._old_serialize = window.serialize;
  _load_script("//d3rxaij56vjege.cloudfront.net/form-serialize/0.3/serialize.min.js", function() {
    window._form_serialize = window.serialize;
    if (window._old_serialize) window.serialize = window._old_serialize;
  });
  var form_submit = function(e) {
    e.preventDefault();
    if (validate_form()) {
      // use this trick to get the submit button & disable it using plain javascript
      document.querySelector('[id^="_form"][id$="_submit"]').disabled = true;
            var serialized = _form_serialize(document.getElementById('_form_5_'));
      var err = form_to_submit.querySelector('._form_error');
      err ? err.parentNode.removeChild(err) : false;
      _load_script('https://agneskowalskiinc.activehosted.com/proc.php?' + serialized + '&jsonp=true');
    }
    return false;
  };
  addEvent(form_to_submit, 'submit', form_submit);
})();

</script>
								<!-- ActiveCampaign Integrated Form Ends -->

								<div class="bottom-buffer3"></div>

								<div class="bottom-buffer2" style="display:inline-block;"></div>

							</div>

						</div>

						<div class="bottom-buffer2"></div>

						<div class="bottom-buffer3"></div>

					</div>

				</div>

			</div>

		</section>

		<!-- NEWSLETTER SECTION ENDS -->





							<!-- GARBAGE NEWSLETTER CODE 

							

							//// WHITE

								<form>

									<div class="input1 pull-left">

										<input type="text" name="name" id="name" Placeholder="Name"/>

									</div>

									<div class="input1 pull-right">

										<input type="text" name="email" id="email" placeholder="Email"/>

									</div>

									<div class="sub-button">

										<img src="<?php //echo get_template_directory_uri(); ?>/images/left.png"/><button type="submit">Abundance Now!</button><img src="<?php //echo get_template_directory_uri(); ?>/images/right.png"/>

									</div>

								</form>



							//// BLACK

								<form>								

									<div class="top-buffer2"></div>

									<div class="input1 pull-left">

										<input type="text" name="name" id="name" Placeholder="Name"/>

									</div>

									<div class="input1 pull-right">

										<input type="text" name="email" id="email" placeholder="Email"/>

									</div>

									<div class="sub-button">

										<img src="<?php // echo get_template_directory_uri(); ?>/images/wleft.png"/><button type="submit">Abundance Now!</button><img src="<?php // echo get_template_directory_uri(); ?>/images/wright.png"/>

									</div>

								</form>



							-->





						<?php endwhile; ?>

					<?php endif; ?>



					<?php get_footer(); ?>