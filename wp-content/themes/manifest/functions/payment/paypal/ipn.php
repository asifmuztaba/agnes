<?php

if (!defined(ABSPATH)) {
    $pagePath = explode('/wp-content/', dirname(__FILE__));
    include_once(str_replace('wp-content/', '', $pagePath[0] . '/wp-load.php'));
}

// STEP 1: Read POST data
// reading posted data from directly from $_REQUEST causes serialization 
// issues with array data in POST
// reading raw POST data from input stream instead. 
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);

$myfile = fopen("raw_post_data.txt", "w") or die("Unable to open file!");
fwrite($myfile, $raw_post_data);
fclose($myfile);

$myfile = fopen("raw_post_array.txt", "w") or die("Unable to open file!");
fwrite($myfile, $raw_post_array);
fclose($myfile);

$myPost = array();
foreach ($raw_post_array as $keyval) {
    $keyval = explode('=', $keyval);
    if (count($keyval) == 2)
        $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if (function_exists('get_magic_quotes_gpc')) {
    $get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
    if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
        $value = urlencode(stripslashes($value));
    } else {
        $value = urlencode($value);
    }
    $req .= "&$key=$value";
}

$paypal_succ_page = get_option('user_pay_succ_id');
$paypal_succ_page = esc_url(get_permalink($paypal_succ_page));

// STEP 2: Post IPN data back to paypal to validate
if (get_option('paypal_mode') == "live") {
    $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
} else {
    $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
}
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// In wamp like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
// of the certificate as shown below.
curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');


if (!($res = curl_exec($ch))) {
    // error_log("Got " . curl_error($ch) . " when processing IPN data");
    curl_close($ch);
    exit;
}
curl_close($ch);

$myfile = fopen("res.txt", "w") or die("Unable to open file!");
fwrite($myfile, $res);
fclose($myfile);

$myfile = fopen("ch.txt", "w") or die("Unable to open file!");
fwrite($myfile, $ch);
fclose($myfile);

// STEP 3: Inspect IPN validation result and act accordingly

if (strcmp(trim($res), "VERIFIED") == 0) {


// check whether the payment_status is Completed
// check that txn_id has not been previously processed
// check that receiver_email is your Primary PayPal email
// check that payment_amount/payment_currency are correct
// process payment
// assign posted variables to local variables
    $item_number = $_REQUEST['item_number'];
    $payment_status = $_REQUEST['payment_status'];
    $payment_amount = $_REQUEST['mc_gross'];
    $payment_currency = $_REQUEST['mc_currency'];
    $txn_id = $_REQUEST['txn_id'];
    $receiver_email = $_REQUEST['receiver_email'];
    $payer_email = $_REQUEST['payer_email'];
    $payer_id = $_REQUEST['payer_id'];
    $amt = $_REQUEST['amt'];
    $custom_data = $_REQUEST['custom'];

    $program_post = get_post($item_number);
    if (!empty($program_post)) {
        if (($program_post->post_type == "user_program") && ($custom_data == "website")) {
            if ($_REQUEST['payment_status'] == 'Completed') {

                $returnatt = serialize($_REQUEST);

                $myfile = fopen("returnatt.txt", "w") or die("Unable to open file!");
                fwrite($myfile, $returnatt);
                fclose($myfile);

                $payer_id = $_REQUEST['payer_id'];


                $email = $_REQUEST['payer_email'];

                update_post_meta($insert_post, 'first_name', $_REQUEST['first_name']);
                update_post_meta($insert_post, 'last_name', $_REQUEST['last_name']);

                $f_name = $_REQUEST['first_name'];
                $l_name = $_REQUEST['last_name'];

                if (!email_exists($email)) {

                    $password = generateStrongPassword();
                    $create_user = wp_create_user($email, $password, $email);

                    if ($create_user != '') {
                        $userdata = array(
                            'ID' => $create_user,
                            'role' => $acc_type,
                            'first_name' => $f_name,
                            'last_name' => $l_name,
                            'phone' => $mob_num,
                        );

                        wp_update_user($userdata);

                        $user = get_user_by('id', $create_user);


                        /* start of send user payment notification */
                        $from = get_option('admin_email');
                        $to = $user->user_email;
                        $login_pg_url = get_permalink(get_option('login_page_id'));
                        $subject = "Welcome to Permission To Prosper";
                        $message = '
                    <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:96%; float:left; margin:2%;">
                        <tr><td style="font-size:16px;">Hello ' . $_REQUEST['first_name'] . ',</td></tr>
                        <tr>
                            <td style="padding:10px 0; width:150px;"> Welcome and thank you for purchasing "Permission To Prosper" program. Your site login details are given below : </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%; margin:10px 0; font-size:14px; border-collapse:collapse;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Username</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $user->user_email . '</td>
                                    </tr>	
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Password</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $password . '</td>
                                    </tr>	
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Program Name</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $_REQUEST['item_name'] . '</td>
                                    </tr>		
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Payment Given</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $_REQUEST['payment_gross'] . ' ' . $_REQUEST['mc_currency'] . '</td>
                                    </tr>			
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Login Page</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;"> <a href="' . $login_pg_url . '"> ' . $login_pg_url . '</a></td>
                                    </tr>						
                                </table>
                            </td>
                        </tr>
                    </table>';

                        send_my_mail($from, $to, $subject, $message);

                        $myfile = fopen("usermail.txt", "w") or die("Unable to open file!");
                        fwrite($myfile, $message);
                        fclose($myfile);

                        /* End of sending user payment notification */
                    }
                } else {

                    $user = get_user_by('email', $email);

                    /* start of send user payment notification */
                    $from = get_option('admin_email');
                    $to = $user->user_email;
                    $subject = "Program Payment";
                    $message = '
                    <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:96%; float:left; margin:2%;">
                        <tr><td style="font-size:16px;">Hello ' . $_REQUEST['first_name'] . ',</td></tr>
                        <tr>
                            <td style="padding:10px 0; width:150px;"> Thank you for registering of ' . $_REQUEST['item_name'] . ' at Agneskowalski! Your program details are given below : </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%; margin:10px 0; font-size:14px; border-collapse:collapse;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Program Name</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $_REQUEST['item_name'] . '</td>
                                    </tr>		
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Payment Given</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $_REQUEST['payment_gross'] . ' ' . $_REQUEST['mc_currency'] . '</td>
                                    </tr>						
                                </table>
                            </td>
                        </tr>
                    </table>';

                    send_my_mail($from, $to, $subject, $message);

                    $myfile = fopen("usermail.txt", "w") or die("Unable to open file!");
                    fwrite($myfile, $message);
                    fclose($myfile);

                    /* End of sending user payment notification */
                }

                update_post_meta($insert_post, 'first_name', $_REQUEST['first_name']);
                update_post_meta($insert_post, 'last_name', $_REQUEST['last_name']);

                $order_title = $_REQUEST['first_name'] . " " . $_REQUEST['last_name'] . " ( " . $payer_id . " )";

                $my_post = array(
                    'post_title' => $order_title,
                    'post_status' => 'publish',
                    'post_author' => $user->ID,
                    'post_type' => 'payment',
                    'post_content' => $returnatt
                );

// Insert the post into the database
                $insert_post = wp_insert_post($my_post);

                $pro_count = get_user_meta($user->ID, "subscribed_program", $single);
                if (!empty($pro_count)) {
                    $pro_count = $pro_count[0];

                    update_user_meta($user->ID, 'subscribed_program_' . $pro_count . '_payment_detail', $insert_post);
                    update_user_meta($user->ID, '_subscribed_program_' . $pro_count . '_payment_detail', "field_59510b8417e3a");
                    update_user_meta($user->ID, 'subscribed_program_' . $pro_count . '_subscribed_program', $item_number);
                    update_user_meta($user->ID, '_subscribed_program_' . $pro_count . '_subscribed_program', "field_59510b5f17e39");
                    $pro_count++;
                    update_user_meta($user->ID, "subscribed_program", $pro_count);
                    update_user_meta($user->ID, "_subscribed_program", "field_59510b2617e38");
                } else {
                    $pro_count = "0";
                    update_user_meta($user->ID, 'subscribed_program_' . $pro_count . '_payment_detail', $insert_post);
                    update_user_meta($user->ID, '_subscribed_program_' . $pro_count . '_payment_detail', "field_59510b8417e3a");
                    update_user_meta($user->ID, 'subscribed_program_' . $pro_count . '_subscribed_program', $item_number);
                    update_user_meta($user->ID, '_subscribed_program_' . $pro_count . '_subscribed_program', "field_59510b5f17e39");
                    $pro_count++;
                    update_user_meta($user->ID, "subscribed_program", $pro_count);
                    update_user_meta($user->ID, "_subscribed_program", "field_59510b2617e38");
                }
                update_post_meta($insert_post, 'payment_for', $_REQUEST['item_name']);
                update_post_meta($insert_post, 'full_payment_date', $returnatt);

                update_post_meta($insert_post, 'payment_date', $_REQUEST['payment_date']);
                update_post_meta($insert_post, 'mc_currency', $_REQUEST['mc_currency']);
                update_post_meta($insert_post, 'payment_status', $_REQUEST['payment_status']);

                update_post_meta($insert_post, 'mc_fee', $_REQUEST['mc_fee']);
                update_post_meta($insert_post, 'business', $_REQUEST['business']);
                update_post_meta($insert_post, 'verify_sign', $_REQUEST['verify_sign']);
                update_post_meta($insert_post, 'txn_id', $_REQUEST['txn_id']);
                update_post_meta($insert_post, 'receiver_email', $_REQUEST['receiver_email']);
                update_post_meta($insert_post, 'payment_fee', $_REQUEST['payment_fee']);
                update_post_meta($insert_post, 'receiver_id', $_REQUEST['receiver_id']);

                update_post_meta($insert_post, 'payment_given', $_REQUEST['payment_gross']);

//address
                update_post_meta($insert_post, 'address_zip', $_REQUEST['address_zip']);
                update_post_meta($insert_post, 'address_street', $_REQUEST['address_street']);
                update_post_meta($insert_post, 'address_country_code', $_REQUEST['address_country_code']);
                update_post_meta($insert_post, 'address_country', $_REQUEST['address_country']);
                update_post_meta($insert_post, 'address_city', $_REQUEST['address_city']);
                update_post_meta($insert_post, 'address_state', $_REQUEST['address_state']);

//payer detail
                update_post_meta($insert_post, 'payer_id', $_REQUEST['payer_id']);
                update_post_meta($insert_post, 'payer_email', $_REQUEST['payer_email']);
                update_post_meta($insert_post, 'first_name', $_REQUEST['first_name']);
                update_post_meta($insert_post, 'last_name', $_REQUEST['last_name']);

//update_user_meta($user_id, 'user_payment_status', $payment_status);
                //start of send Admin payment notification
                $to = $from;
                $subject = "Program Registration Payment Info";
                $program_payment_info_msg = '
                    <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:96%; float:left; margin:2%">
                        <tr><td style="font-size:16px;">Hello Admin,</td></tr>
                        <tr>
                            <td style="padding:10px 0; width:150px;"> New Payment for the program ' . $_REQUEST['item_name'] . ' has been done. The details are as below :</td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%; margin:10px 0; font-size:14px; border-collapse:collapse;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Username</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $user->user_email . '</td>
                                    </tr>	
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Program Name</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $_REQUEST['item_name'] . '</td>
                                    </tr>		
                                    <tr>
                                        <td style="border:1px solid #90A9F9; padding:5px;">Payment Given</td>
                                        <td style="border:1px solid #90A9F9; padding:5px;">' . $_REQUEST['payment_gross'] . ' ' . $_REQUEST['mc_currency'] . '</td>
                                    </tr>						
                                </table>
                            </td>
                        </tr>
                    </table>';

                send_my_mail($from, $to, $subject, $program_payment_info_msg);

                $myfile = fopen("adminmail.txt", "w") or die("Unable to open file!");
                fwrite($myfile, $program_payment_info_msg);
                fclose($myfile);
            }
        }
    }
    if ($custom_data == "Wealth") {
        $myfile = fopen("wealth.txt", "w") or die("Unable to open file!");
        fwrite($myfile, "wealth");
        fclose($myfile);

        $from = get_option('admin_email');
        $to = $_REQUEST['payer_email'];
        $subject = "Welcome to Wealthy Self Worth Woskshop";
        $message = '
                    <table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:96%; float:left; margin:2%;">
                        <tr><td style="font-size:16px;">Hello ' . $_REQUEST['first_name'] . ',</td></tr>
                        <tr>
                            <td style="padding:10px 0; width:150px;"> Yay! You’re in for a transformational mindset experience. You’ll get details about your workshop shortly.</td>
                        </tr>
                    </table>';

        send_my_mail($from, $to, $subject, $message);

        $myfile = fopen("usermail.txt", "w") or die("Unable to open file!");
        fwrite($myfile, $message);
        fclose($myfile);
    }
} else if (strcmp($res, "INVALID") == 0) {



    $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
    $txt = "Unverified\n";
    fwrite($myfile, $txt);
    fclose($myfile);

    // log for manual investigation
}
?>