<?php
/**
* The template for displaying search results pages.
*/

get_header();
?>
<div class="container">	
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 ">
        	<div class="row p-right">
                <h3 class="text-upper heddings hedding bishop bishop-border"><b><?php printf( __( "Search Results for '%s'", 'twentyfifteen' ), get_search_query() ); ?></b></h3>
            </div>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="row p-right search-f">
                <div class="clearfix top-buffer-30"></div>
                <div class="clearfix top-buffer-10"></div>
                <h3 class="text-upper l-height heddings e-name text-c"><a href="<?php the_permalink(); ?>" class="border-s color"><?php the_title(); ?></a></h3>
                <?php the_excerpt(); ?>
            </div>
            <?php endwhile; ?>
            <nav class="text-center">
                <div class="pagination">
                    <?php if (function_exists('wp_pagenavi')) : ?>
                    <div class="pagenavi"><?php wp_pagenavi(); ?></div>
                    <?php else : ?>
                    <div class="nextPreviousbg">
                        <div class="alignleft"><?php previous_post_link('%link','Previous') ?></div>
                        <div class="alignright"><?php next_post_link('%link','Next') ?></div>
                    </div>
                    <?php endif; wp_reset_query(); ?>
                </div>                
                <div class="clearfix bottom-buffer"></div>
            </nav>            
            <?php else : ?>
                <div class="row p-right search-f">
                    <div class="clearfix top-buffer-30"></div>
                    <div class="clearfix top-buffer-10"></div>
                    <p>No data found.</p>
                </div>
            <?php endif; ?>
        </div>        
        <?php echo do_shortcode('[CommonSidebar]'); ?>
	</div>
</div>

<div class="clearfix top-buffer-30"></div>
<div class="clearfix top-buffer-30"></div>
<?php get_footer(); ?>