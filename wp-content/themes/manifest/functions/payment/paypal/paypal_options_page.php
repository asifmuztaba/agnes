<?php
add_action('admin_init', 'register_paypal_settings');

function register_paypal_settings() {
    register_setting('my-own-theme-options-for-paypal', 'paypal_mode');
    register_setting('my-own-theme-options-for-paypal', 'paypal_merchant_email');
    register_setting('my-own-theme-options-for-paypal', 'paypal_currency');
    register_setting('my-own-theme-options-for-paypal', 'paypal_success_page');
    register_setting('my-own-theme-options-for-paypal', 'paypal_fail_page');

    register_setting('my-own-theme-options-for-paypal', 'app_prmt_prdc_prc');
    register_setting('my-own-theme-options-for-paypal', 'sch_pro_reg_val');
    register_setting('my-own-theme-options-for-paypal', 'sch_pro_reg_expire');

    register_setting('my-own-theme-options-for-paypal', 'paypal_sch_sign_succ');
    register_setting('my-own-theme-options-for-paypal', 'paypal_sch_sign_fail');

    register_setting('my-own-theme-options-for-paypal', 'paypal_book_succ');
    register_setting('my-own-theme-options-for-paypal', 'paypal_book_fail');

    register_setting('my-own-theme-options-for-paypal', 'paypal_produce_fail');
    register_setting('my-own-theme-options-for-paypal', 'paypal_produce_succ');
}

function paypal_options_page() {
    ?>

    <div class="wrap">
        <h2>Paypal Setting</h2>
        <?php settings_errors(); ?> 
        <form method="post" action="options.php">
            <?php settings_fields('my-own-theme-options-for-paypal'); ?>
            <?php do_settings_sections('my-own-theme-options-for-paypal'); ?>
            <?php include(get_template_directory() . '/functions/theme_option_page/bootstrap_theme_includes.php'); ?>
            <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
            <br />
            <style type="text/css">
                a{
                    outline: none !important;
                }
                a:focus{
                    box-shadow: none !important;
                }
                #wpfooter {
                    position: relative;
                }
            </style>
            <div class="row">

                <div class="col-md-8">


                    <ul class="nav nav-tabs panel-title" id="myTab">
                        <li class="active" ><a data-toggle="tab" href="#paypal_a">General</a></li>
                        <li><a data-toggle="tab" href="#paypal_b">School Program Sign Up</a></li>
                        <li><a data-toggle="tab" href="#paypal_book">Buy Book</a></li>
                        <li><a data-toggle="tab" href="#paypal_produce">Promote Produce</a></li>
                    </ul>


                    <div class="tab-content">
                        <div id="paypal_a" class="tab-pane fade in active">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Paypal Details</h3>
                                </div>
                                <div class="panel-body">

                                    <label for="paypal_mode">Payment Mode</label>

                                    <br />

                                    <label for="demo_mode"> 
                                        <input id="demo_mode" type="radio" value="sandbox" name="paypal_mode" class="form-control" <?php if (get_option('paypal_mode') == "sandbox") { ?> checked="checked" <?Php } ?> /> Sandbox
                                    </label>
                                    &nbsp;&nbsp;&nbsp;
                                    <label for="live_mode"> 
                                        <input id="live_mode" type="radio" value="live" name="paypal_mode" class="form-control" <?php if (get_option('paypal_mode') == "live") { ?> checked="checked" <?Php } ?> /> Live
                                    </label>

                                    <br />
                                    <br />

                                    <div class="paypal_merchant_email_div" <?php if (get_option('paypal_mode') == "sandbox") { ?> style="display: none;" <?Php } ?> >

                                        <label for="paypal_merchant_email">Merchant Email</label>
                                        <input id="paypal_merchant_email" type="text" name="paypal_merchant_email" value="<?php echo get_option('paypal_merchant_email'); ?>" class="form-control" />

                                        <br />
                                    </div>


                                    <div class="paypal_currency_div" <?php if (get_option('paypal_mode') == "sandbox") { ?> style="display: none;" <?Php } ?> >

                                        <label for="paypal_currency">Currency Code</label>
                                        <input id="paypal_currency" type="text" name="paypal_currency" value="<?php echo get_option('paypal_currency'); ?>" class="form-control" />

                                        <br />
                                    </div>

                                    <?php
                                    $args = array(
                                        'post_type' => 'page',
                                        'post_status' => 'publish',
                                        'sort_order' => 'asc',
                                        'sort_column' => 'post_title',
                                        'hierarchical' => 1,
                                    );
                                    $pages = get_pages($args);
                                    ?>
                                    <?php $paypal_success_page = get_option('paypal_success_page'); ?>

                                    <label for="paypal_success_page">Success Page URL</label>
                                    <select class="form-control" name="paypal_success_page" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_success_page) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />

                                    <label for="paypal_fail_page">Fail Page URL</label>
    <?php $paypal_fail_page = get_option('paypal_fail_page'); ?>
                                    <select class="form-control" name="paypal_fail_page" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_fail_page) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />

                                    <label for="app_prmt_prdc_prc">Promote Produce Price</label>
                                    <input id="app_prmt_prdc_prc" type="text" name="app_prmt_prdc_prc" value="<?php echo get_option('app_prmt_prdc_prc'); ?>" class="form-control" /> <br />

                                    <script type="text/javascript">
                                        jQuery(document).ready(function ($) {
                                            $("input[name=paypal_mode]:radio").change(function () {
                                                if ($("#demo_mode").attr("checked")) {
                                                    $(".paypal_merchant_email_div").slideUp("slow");
                                                    $(".paypal_currency_div").slideUp("slow");
                                                }
                                                else {
                                                    $(".paypal_merchant_email_div").slideDown("slow");
                                                    $(".paypal_currency_div").slideDown("slow");
                                                }
                                            });
                                        });
                                    </script>

                                </div>
                            </div>
                        </div>

                        <div id="paypal_b" class="tab-pane fade in">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">School Program Sign Up</h3>
                                </div>
                                <div class="panel-body">

    <?php $paypal_success_page = get_option('paypal_sch_sign_succ'); ?>

                                    <label for="paypal_sch_sign_succ">Payment Success Page</label>
                                    <select class="form-control" name="paypal_sch_sign_succ" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_success_page) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />

                                    <label for="paypal_sch_sign_fail">Payment Fail Page</label>
    <?php $paypal_fail_page = get_option('paypal_sch_sign_fail'); ?>
                                    <select class="form-control" name="paypal_sch_sign_fail" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_fail_page) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="sch_pro_reg_val">School Program Registration Price</label>
                                            <p>Enter the registration price for the school program</p>
                                            <input id="sch_pro_reg_val" type="text" name="sch_pro_reg_val" value="<?php echo get_option('sch_pro_reg_val'); ?>" class="form-control" />
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="sch_pro_reg_expire">School Program Registration Expire Months</label>
                                            <p>Enter the registration expire time in months</p>
                                            <input id="sch_pro_reg_expire" type="number" name="sch_pro_reg_expire" value="<?php echo get_option('sch_pro_reg_expire'); ?>" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="paypal_book" class="tab-pane fade in">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Book Setting</h3>
                                </div>
                                <div class="panel-body">

    <?php $paypal_book_succ_page = get_option('paypal_book_succ'); ?>

                                    <label for="paypal_book_succ">Payment Success Page</label>
                                    <select class="form-control" name="paypal_book_succ" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_book_succ_page) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />

                                    <label for="paypal_book_fail">Payment Fail Page</label>
    <?php $paypal_boook_fail_page = get_option('paypal_book_fail'); ?>
                                    <select class="form-control" name="paypal_book_fail" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_boook_fail_page) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />
                                </div>
                            </div>
                        </div>
                        <div id="paypal_produce" class="tab-pane fade in">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Produce Page</h3>
                                </div>
                                <div class="panel-body">
    <?php $paypal_produce_succ_page = get_option('paypal_produce_succ'); ?>

                                    <label for="paypal_produce_succ">Payment Success Page</label>
                                    <select class="form-control" name="paypal_produce_succ" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_produce_succ_page) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />

                                    <label for="paypal_produce_fail">Payment Fail Page</label>
    <?php $paypal_produce_fail = get_option('paypal_produce_fail'); ?>
                                    <select class="form-control" name="paypal_produce_fail" >
                                        <option>Select</option>
    <?php for ($i = 0; $i < count($pages); $i++) { ?>
                                            <option value="<?php echo $pages[$i]->ID; ?>" <?php if ($pages[$i]->ID == $paypal_produce_fail) { ?> selected="selected" <?php } ?>  ><?php echo $pages[$i]->post_title; ?></option>
                                        <?php } ?>
                                    </select>

                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

    <?php include get_template_directory() . '/functions/theme_option_page/option_page_sidebar.php'; ?>

            </div>

    <?php submit_button(); ?>

        </form>
    </div>
    <?php
}

/* My Theme Option */
?>
