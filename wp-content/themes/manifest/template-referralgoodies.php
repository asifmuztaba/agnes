<?php
/*
  Template Name: Referral Goodies
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-site-verification" content="JeancQdLxoMjIxfnD1sQYcQ7XAMJ8T2bkL9Asc9s8gY" />
    <title>Agnes Kowalski</title>
    <!-- Bootstrap core CSS -->
    <link href="http://www.agneskowalski.com/wp-content/themes/manifest/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.agneskowalski.com/wp-content/themes/manifest/css/style-juicy-resources.css" rel="stylesheet">


</head>
<body class="jucy_resource">
		      <section>
         <div class="header">
            <div class="navbar-header">
               <p class="s76 responsive-title" style="color:#c8485f;">Agnes Kowalski</p>
               <button type="button" class="navbar-toggle toggleMenu collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">                            <span class="icon-bar"></span>                            <span class="icon-bar"></span>                            <span class="icon-bar"></span>                        </button>                    
            </div>
            <ul id="menu-header-menu" class="nav"><li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-17"><a href="http://www.agneskowalski.com/">HOME</a></li>
<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="http://www.agneskowalski.com/about/">ABOUT</a></li>
<li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="http://www.agneskowalski.com/work-with-me/">WORK WITH ME</a></li>
<li id="menu-item-297" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-297"><a href="http://www.agneskowalski.com/blog/">BLOG</a></li>
<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="http://www.agneskowalski.com/client-love/">CLIENT LOVE</a></li>
<li id="menu-item-977" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-977"><a href="http://www.agneskowalski.com/juicy-resources/">JUICY RESOURCES</a></li>
<li id="menu-item-978" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-978"><a href="http://www.agneskowalski.com/referral-goodies/">REFERRAL GOODIES</a></li>
<li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="http://www.agneskowalski.com/contact/">CONTACT</a></li>
</ul>                
         </div>
      </section>
<section>
    <div class="goodies_ref">
		<div class="container">
			 <div class="top-buffer5"></div>
			 <div class="title_m text-center">
				  <h2>Referral Goodies</h2>
			 </div>
		    <div class="top-buffer5"></div>
		    <div class="top-buffer1"></div>
			<div class="col-md-7">
				<div class="goodies_box">
					<div class="inner_box">
						 <p>I love to love you. Especially when you refer new clients to me.My clients return year after year because the work, works, if you work it.Want to tell your Family? Friends? Colleagues? Clients? Crossfit buddies?</p>
						 <div class="top-buffer2"></div>
						 <p>Great.</p>
						 <div class="top-buffer2"></div>
						 <p>There’s incentive for you.</p>
						 <div class="top-buffer2"></div>
						 <p>For every client referral you can redeem:</p>
						 <div class="top-buffer2"></div>
						 <ul>
							 <li><p>√ A subscription box (3 months)</p></li>
							 <li><p>√ A 30 min. Intensive session with me (good for 6 months)</p></li>
							 <li><p>√ A $50 gift card of your choice</p></li>
						 </ul>
						 <div class="top-buffer2"></div>
						 <p>Click contact here to email me, please include:</p>
						 <div class="top-buffer2"></div>
						 <p>Your name</p>
						 <p>Referrals name</p>
						 <p>What goodie you want coming your way!</p>
						 <div class="top-buffer2"></div>
						 <p>XO</p>
						 <div class="top-buffer1"></div>
						 <h2>Agnes</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="top-buffer5"></div>
    </div>
</section>

<footer id="page-footer">

	<div class="container">

		<div class="footer">

			<p style="color: #ffffff;">© 2018 ALL RIGHTS RESERVED</p>

		</div>

	</div>

</footer>


<script src="http://www.agneskowalski.com/wp-content/themes/manifest/js/jquery.min.js"></script>
<script src="http://www.agneskowalski.com/wp-content/themes/manifest/js/bootstrap.min.js"></script>
</body>
</html>