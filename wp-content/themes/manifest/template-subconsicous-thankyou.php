<?php
/*
  Template Name: Subconsicous Smackdown Thank You
 */
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Subconscious Smackdown - Thank You</title>
        <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/fonts/stylesheet.css">
        <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/css/font-awesome.css">
        <link rel="stylesheet" href="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/css/t_style.css">
    </head>
    <body>
        <section>
            <div class="image">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="content text-center">
                                <span class="line1 d-inline-block w-100 mt-5">THANK You </span>
                                <span class="line2 d-inline-block w-100"><p>for joining the Training!</p></span>
                                <span class="line3 d-inline-block w-100"><p>Want to make your money mindset breakthrough</p></span>
                                <span class="line4 d-inline-block w-100"><p>Succeed by more than 95%?</p></span>
                                <span class="line5 d-inline-block w-100"><p>Get an accountability partner, do it with a friend!</p></span>
                                <div class="share mb-2 mt-4">
                                    <span class="symbol"><a target="_blank" href="https://www.facebook.com/sharer.php?u=http://www.agneskowalski.com/subconscious-smackdown/"><i class="fa fa-facebook fb" aria-hidden="true"></i></a></span>&nbsp
                                    <span class="symbol"><a target="_blank" href="https://twitter.com/share?url=http://www.agneskowalski.com/subconscious-smackdown/"><i class="fa fa-twitter fb" aria-hidden="true"></i></a></span>&nbsp
                                    <span class="symbol"><a target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?is_video=false&url=http://www.agneskowalski.com/subconscious-smackdown/&media=&description=Subconscious%20Smackdown"><i class="fa fa-pinterest-p fb" aria-hidden="true"></i></a></span>&nbsp
                                    <span class="symbol"><a target="_blank" href="mailto:?Subject=Subconscious%20Smackdown&Body=http://www.agneskowalski.com/subconscious-smackdown/"> <i class="fa fa-envelope fb" aria-hidden="true"></i></a></span>
                                </div>
                                <span class="line6"><p class="mb-0">SHARE YOUR 5 DAY TRAINING EXPERIENCE</p></span>
                                <span class="line7"><p>Using hashtag #subconsioussmackdown #agneskowalski #moneymindsetcoach</p></span>
                                <span class="line8"><p>Every month I'll give away a 30 minute mindset session ($250 value!) to the person who has the most hashtags.</p></span>
                                <span class="line9 mt-4"><p>May the force of the subconscious be with you!</p></span>
                                <div class="xoxo mt-4">
                                    <span class="sign d-inline-block mw-100 pr-3">XOXO</span> 
                                    <span class="sign-o">
                                        <span class="agnes d-inline-block">Agnes K</span>
                                        <span class="mindset"> Mind$et Mentor </span>                                        
                                    </span>
                                </div>
                                <span class="featured"><p class="mt-4 mb-0">AS FEATURED IN</p></span>
                                <div class="logo-img">
                                    <img class="elephant" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/l1.png" alt="">
                                    <img class="elephant" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/l2.png" alt="">
                                    <img class="elephant" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/l3.png" alt="">
                                    <img class="elephant" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/l7.png" alt="">
                                    <img class="elephant" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/l4.png" alt="">
                                    <img class="elephant" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/l5.png" alt="">
                                    <img class="elephant" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/l6.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div>
                <img class="logo" src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/image/logo_icon.png" alt="">
            </div>
        </section>
        <div class="footer">
            <div class="container-fluid">
                <div class="footer-text text-center">
                    <i class="fa fa-copyright" aria-hidden="true"></i> 2018 All rights reserved
                </div>
            </div>
        </div>
        <script src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/js/bootstrap.min.js"></script>
        <script src="http://www.agneskowalski.com/wp-content/themes/subconscious-smackdown/js/jquery-3.3.1.slim.min.js"></script>
    </body>
</html>