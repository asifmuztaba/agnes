<?php

$result = add_role(
        'client', __('Client'), array(
    'read' => true,
    'edit_posts' => true,
    'delete_posts' => false,
    'switch_themes' => false,
    'edit_themes' => false,
    'edit_theme_options' => true,
    'activate_plugins' => false,
    'edit_plugins' => false,
    'install_plugins' => false,
    'edit_users' => false,
    'manage_options' => true,
    'moderate_comments' => true,
    'manage_categories' => true,
    'manage_links' => true,
    'upload_files' => true,
    'edit_others_posts' => true,
    'edit_published_posts' => true,
    'publish_posts' => true,
    'edit_pages' => true,
    'publish_pages' => true,
    'edit_others_pages' => true,
    'edit_published_pages' => true,
    'delete_pages' => false,
    'delete_others_pages' => false,
    'delete_published_pages' => false,
    'delete_posts' => false,
    'delete_others_posts' => false,
    'delete_published_posts' => false,
    'delete_private_posts' => false,
    'edit_private_posts' => true,
    'read_private_posts' => true,
    'delete_private_pages' => false,
    'edit_private_pages' => true,
    'read_private_pages' => true,
    'delete_users' => false,
    'create_users' => false,
    'unfiltered_upload' => true,
    'edit_dashboard' => true,
    'update_plugins' => false,
    'delete_plugins' => false,
    'update_themes' => false,
    'update_core' => false,
    'list_users' => true,
    'remove_users' => false,
    'add_users' => false,
    'promote_users' => true,
    'delete_themes' => false,
    'export' => false,
    'edit_comment' => true,
    'manage_network' => false,
    'manage_sites' => false,
    'manage_network_users' => false,
    'manage_network_themes' => false,
    'manage_network_options' => false
        )
);

//$client = get_role( 'client' );
//$client->add_cap( 'administrator' ); 

remove_role('client');
remove_role('normal');

$result = add_role(
        'company', __('Company'), array(
    'read' => true,
    'edit_posts' => true,
    'upload_files' => true,
    'create_posts' => true,
    'publish_posts' => false,
    'delete_posts' => true,
    'delete_attachments' => true,
    'edit_themes' => false,
    'install_plugins' => false,
    'update_plugin' => false,
    'update_core' => false,
    'edit_published_posts' => true,
    'delete_published_posts' => true
        )
);
if (current_user_can('company') && !current_user_can('upload_files'))
    add_action('admin_init', 'allow_contributor_uploads');

function allow_contributor_uploads() {
    $contributor = get_role('company');
    $contributor->add_cap('upload_files');

    $contributor->add_cap('delete_attachments');
}

/* Start of user role Showroom */

$result = add_role(
        'showroom', __('Show Room'), array(
    'read' => true,
    'edit_posts' => true,
    'upload_files' => true,
    'create_posts' => true,
    'publish_posts' => false,
    'delete_posts' => true,
    'delete_attachments' => true,
    'edit_themes' => false,
    'install_plugins' => false,
    'update_plugin' => false,
    'update_core' => false,
    'edit_published_posts' => true,
    'delete_published_posts' => true
        )
);
if (current_user_can('showroom') && !current_user_can('upload_files'))
    add_action('admin_init', 'allow_contributor_uploads_showroom');

function allow_contributor_uploads_showroom() {
    $contributor = get_role('showroom');
    $contributor->add_cap('upload_files');
    $contributor->add_cap('delete_attachments');
}
/* End of user role Showroom */



/* Start of user role Supplier */
$result = add_role(
        'supplier', __('Supplier'), array(
    'read' => true,
    'edit_posts' => true,
    'upload_files' => true,
    'create_posts' => true,
    'publish_posts' => false,
    'delete_posts' => true,
    'delete_attachments' => true,
    'edit_themes' => false,
    'install_plugins' => false,
    'update_plugin' => false,
    'update_core' => false,
    'edit_published_posts' => true,
    'delete_published_posts' => true
        )
);
if (current_user_can('supplier') && !current_user_can('upload_files'))
    add_action('admin_init', 'allow_contributor_uploads_supplier');

function allow_contributor_uploads_supplier() {
    $contributor = get_role('supplier');
    $contributor->add_cap('upload_files');
    $contributor->add_cap('delete_attachments');
}
/* End of user role Supplier */


/* Start of limit user to upload images */
add_filter('wp_handle_upload_prefilter', 'limit_uploads_for_user_roles');

function limit_uploads_for_user_roles($file) {
    $user = wp_get_current_user();
    // add the role you want to limit in the array
    $limit_roles = array('company');
    $filtered = apply_filters('limit_uploads_for_roles', $limit_roles, $user);
    if (array_intersect($limit_roles, $user->roles)) {
        $upload_count = get_user_meta($user->ID, 'upload_count', true) ?: 0;
        $limit = 2;

        // get filesize of upload
        $size = $file['size'];
        $size = $size / 1024; // Calculate down to KB
        // get imagetype of upload
        $type = $file['type'];
        $is_image = strpos($type, 'image');

        // set sizelimit
        $limit = 500; // Your Filesize in KB
        // set imagelimit
        $imagelimit = 10;

        // set allowed imagetype
        $imagetype = 'image/jpeg';

        // query how many images the current user already uploaded
        global $current_user;
        $args = array(
            'orderby' => 'post_date',
            'order' => 'DESC',
            'numberposts' => -1,
            'post_type' => 'attachment',
            'author' => $current_user->ID,
        );
        $attachmentsbyuser = get_posts($args);

        if (( $size > $limit ) && ($is_image !== false)) { // check if the image is small enough
            $file['error'] = 'Image files must be smaller than ' . $limit . 'KB';
        } elseif ($type != $imagetype) { // check if image type is allowed
            $file['error'] = 'Image must be ' . $imagetype . '.';
        } elseif (count($attachmentsbyuser) >= $imagelimit) { // check if the user has exceeded the image limit
            $file['error'] = 'Image limit of ' . $imagelimit . ' is exceeded for this user.';
        }

        if (( $upload_count + 1 ) > $limit) {
            $file['error'] = __('Upload limit has been reached for this account! Contact us on Info@ceramicmarketing for more information', 'yourtxtdomain');
        } else {
            update_user_meta($user->ID, 'upload_count', $upload_count + 1);
        }
    }
    return $file;
}

add_action('delete_attachment', 'decrease_limit_uploads_for_user');

function decrease_limit_uploads_for_user($id) {
    $user = wp_get_current_user();
    // add the role you want to limit in the array
    $limit_roles = array('company');
    $filtered = apply_filters('limit_uploads_for_roles', $limit_roles, $user);
    if (array_intersect($limit_roles, $user->roles)) {
        $post = get_post($id);
        if ($post->post_author != $user->ID)
            return;
        $count = get_user_meta($user->ID, 'upload_count', true) ?: 0;
        if ($count)
            update_user_meta($user->ID, 'upload_count', $count - 1);
    }
}

/* End of limit user to upload images */
?>