<?php
add_action( 'admin_init', 'register_mycontact_settings' );
function register_mycontact_settings() 
{ 
    register_setting( 'my-own-theme-options-for-contact', 'footer_contact_label_val' );
    register_setting( 'my-own-theme-options-for-contact', 'address_val' );
    register_setting( 'my-own-theme-options-for-contact', 'address_url_val' );
    register_setting( 'my-own-theme-options-for-contact', 'email_val' );
    register_setting( 'my-own-theme-options-for-contact', 'phone_val' );
    register_setting( 'my-own-theme-options-for-contact', 'fax_val' );
}
function contact_options_page() {
?>

<div class="wrap">
    <h2>Social Links</h2>
    <?php settings_errors(); ?> 
    <form method="post" action="options.php">
        <?php settings_fields( 'my-own-theme-options-for-contact' ); ?>
        <?php do_settings_sections( 'my-own-theme-options-for-contact' ); ?>
        <?php include('bootstrap_theme_includes.php'); ?>
        <br />
        <div class="row">
            
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Contact Detail</h3>
                    </div>
                    <div class="panel-body">
                        
                        <label for="footer_contact_label_val">Contact Label</label>
                        <input id="footer_contact_label_val" type="text" name="footer_contact_label_val" value="<?php echo get_option('footer_contact_label_val'); ?>" class="form-control" />
                        
                        <br />
                        
                        <label for="address_val">Address</label>
                        <input id="address_val" type="text" name="address_val" value="<?php echo get_option('address_val'); ?>" class="form-control" />
                        
                        <br />
                        <label for="address_url_val">Address Url</label>
                        <input id="address_url_val" type="text" name="address_url_val" value="<?php echo get_option('address_url_val'); ?>" class="form-control" />
                        
                        <br />
                        
                        <label for="email_val">Email</label>
                        <input id="email_val" type="text" name="email_val" value="<?php echo get_option('email_val'); ?>" class="form-control" />
                        
                        <br />
                        
                        <label for="phone_val">Phone Number</label>
                        <input id="phone_val" type="text" name="phone_val" value="<?php echo get_option('phone_val'); ?>" class="form-control" />
                        
                        <br />
                        
                        <label for="fax_val">Fax Number</label>
                        <input id="fax_val" type="text" name="fax_val" value="<?php echo get_option('fax_val'); ?>" class="form-control" />
                        
                    </div>
                </div>
            </div>
            
            <?php include 'option_page_sidebar.php'; ?>
            
        </div>
   
        <?php submit_button(); ?>
        
    </form>
</div>
<?php } 
/*My Theme Option*/
?>
