<?php
/*
  Template Name: Login
 */

if (!session_id()) {
    session_start();
}

if (!is_user_logged_in()) {

    if (isset($_POST['login_btn'])) {

        $user_name = $_POST['email_id'];
        $user_password = $_POST['password'];
        $rememberme = $_POST['rememberme'];

        if ($user_name != '') {
            $creds = array();
            $creds['user_login'] = $user_name;
            $creds['user_password'] = $user_password;
            if ($rememberme) {
                $creds['remember'] = $rememberme;
            }
            $user = wp_signon($creds, false);
            if (!is_wp_error($user)) {
                if (is_super_admin()) {
                    $dashboard_url = admin_url();
                    wp_safe_redirect($dashboard_url);
                    die();
                } else {
                    wp_safe_redirect(get_permalink(get_option('dash_page_id')));
                    die();
                }
            } else {
                /* Setting up Forgot Password Page */
                $_SESSION['login_error_msg'] = $user->get_error_message();
                $string = $user->get_error_message();
                $newurl = get_permalink(get_option('f_pass_id'));
                $pattern = "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/";
                $_SESSION['login_error_msg'] = preg_replace($pattern, $newurl, $string);
            }
        } else {
            $_SESSION['login_error_msg'] = "Please add your Email address or Password";
        }
    }
    ?>
    <?php
    get_header();
    ?>

    <?php if (have_posts()) : ?>  
        <?php while (have_posts()) : the_post(); ?>


            <!---login---->
            <section class="login-page">
                <div class="top-buffer100"></div>
                <div class="top-buffer100"></div>
                <div class="container">
                    <div class="login">
                        <div class="title sec4">
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <div class="login-form">
                            <form name="login_form" method="post">
                                <?php if ($_SESSION['login_error_msg']) { ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <?php
                                        echo $_SESSION['login_error_msg'];
                                        $_SESSION['login_error_msg'] = "";
                                        ?>
                                    </div>
                                <?php } ?>
                                <?php if ($_SESSION['mail_check']) { ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <?php
                                        echo $_SESSION['mail_check'];
                                        $_SESSION['mail_check'] = "";
                                        ?>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <input type="email" id="username" placeholder="Username" class="text-input" required="" name="email_id" >
                                </div>
                                <div class="form-group">
                                    <input type="password" class="text-input"  required="" name="password" id="password" placeholder="Password">
                                </div>
                                <div class="form-group text-right">
                                    <a href="<?php echo get_permalink(get_option('f_pass_id')); ?>"><u>Forgot Password?</u></a>
                                </div>
                                <div class="sub-button">
                                    <button class="log-submit" type="submit" name="login_btn" value="login">LOG IN</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="top-buffer100"></div>
                <div class="top-buffer100" style="margin-top:93px;"></div>
            </section>
            <!--- end Login--->


        <?php endwhile; ?>
    <?php else: ?> 
        <div class="error"><?php _e('Oops ! Page not Found !'); ?></div>
    <?php endif; ?>

    <?php
    get_footer();
}
else {
    wp_safe_redirect(get_permalink(get_option('dash_page_id'))); /* Redirect To Dashboard page */
    die();
}?>