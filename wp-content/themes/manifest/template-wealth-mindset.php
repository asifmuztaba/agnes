<?php

/*

  Template Name: Wealth Mindset Workbook

 */

?>

<html lang="en">

	<head>

		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="10 Cash Flow Commandments">

		<meta name="author" content="Agnes Kowalski">

        <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />

		<title>Wealth Mindset Workbook</title>

		<!-- Bootstrap core CSS -->

		<link href="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/css/bootstrap.min.css" rel="stylesheet">

		<link href="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/css/bootstrap-theme.min.css" rel="stylesheet">

		<link href="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/css/style.css" rel="stylesheet">

		<script src="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/js/jquery.min.js"></script>

		<script src="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/js/bootstrap.min.js"></script>
		<style>
		._submit.button {background-image: url(http://www.agneskowalski.com/mindsetpack/images/button.png);
    background-position: 58% 69%;
    background-color: #C8485F;
    border-radius: 20px;
    padding: 7px 30px;
    letter-spacing: 1px;
    margin-top: 20px;
    margin-bottom: 0px;
    border: none;
		color: #fff;
		    font-family: Bebas-Neue-Bold;
    font-size: 35px;
    text-transform: uppercase;}
	
	a, a:hover {
	text-decoration:none;}
		</style>

	</head>

	<body>

		<div class="section">

			<div class="main-bg">

				<div class="container">

					<div class="details">

					    <div class="top-buffer140"></div>

						<p class="s38">Wealth Mindset</p>

						<h1>Workbook</h1>
						
						<p style="text-align:center;font-size:24px;margin-bottom:40px;">31 Prosperity Increasing Prompts (I personally do) To Immediately Start Shifting you out of Scarcity and into Prosperity</p>

						<div class="top-buffer1"></div>

						<p class="s27">With</p>

						<h2>Agnes Kowalski</h2>

						<div class="top-buffer5"></div>

						<div class="top-buffer3"></div>

						<div class="col-sm-10 col-sm-offset-1">

							<div style="text-align: center;">
  
      <div class="buttons sub-button">
	  <img src="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/images/left.png"/>
        <a href="
https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GHU7QG2SJ44ZE" target="_blank"class="_submit button" type="submit">BUY NOW
        </a>
		<img src="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/images/right.png"/>
      </div>
      

							</div>

						</div>

					</div>

					<div class="bottom-buffer"></div>

				</div>

				

			</div>

			<footer>

				<div class="container">

					<div class="footer">

						<p>© 2017 All rights reserved</p>

					</div>

				</div>

			</footer>

			

		</div>


		<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

<!-- Global Site Tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments)};

  gtag('js', new Date());



  gtag('config', 'UA-106731453-1');

</script>

	</body>

</html>