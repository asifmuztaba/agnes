<?php 
register_post_type( 'user_program',
    array(
    'labels'             => array(
    'name'               => __('Program'            		),
    'singular_name'      => __('Program'                    ),
    'add_new'            => __('Add Program'                ),
    'all_items'          => __('All Program'                ),
    'add_new_item'       => __('Add New Program'            ),
    'edit_item'          => __('Edit Program'               ),
    'new_item'           => __('New Program'                ),
    'view_item'          => __('View Program'               ),
    'search_items'       => __('Search Program'             ),
    'not_found'          => __('No Program found'           ),
    'not_found_in_trash' => __('No Program found in Trash'  )
    ),
    'public'       => true,
    'has_archive'  => true,
    'menu_icon'    => 'dashicons-networking',
    'rewrite'      => array('slug'=>'user-program'),
    'supports'     => array( 'title','editor','thumbnail','custom-fields','revisions','page-attributes')
    )
); 
//include 'user_program_columns.php';
?>