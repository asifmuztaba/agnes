<?php
/*
  Template Name: Free High End Stuff
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-site-verification" content="JeancQdLxoMjIxfnD1sQYcQ7XAMJ8T2bkL9Asc9s8gY" />
    <title>Agnes Kowalski</title>
    <!-- Bootstrap core CSS -->
    <link href="http://www.agneskowalski.com/wp-content/themes/manifest/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.agneskowalski.com/wp-content/themes/manifest/css/style-juicy-resources.css" rel="stylesheet">


</head>

<body class="jucy_resource">
	      <section>
         <div class="header">
            <div class="navbar-header">
               <p class="s76 responsive-title" style="color:#c8485f;">Agnes Kowalski</p>
               <button type="button" class="navbar-toggle toggleMenu collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">                            <span class="icon-bar"></span>                            <span class="icon-bar"></span>                            <span class="icon-bar"></span>                        </button>                    
            </div>
            <ul id="menu-header-menu" class="nav"><li id="menu-item-17" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item menu-item-17"><a href="http://www.agneskowalski.com/">HOME</a></li>
<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18"><a href="http://www.agneskowalski.com/about/">ABOUT</a></li>
<li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="http://www.agneskowalski.com/work-with-me/">WORK WITH ME</a></li>
<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="http://www.agneskowalski.com/client-love/">CLIENT LOVE</a></li>
<li id="menu-item-977" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-977"><a href="http://www.agneskowalski.com/free-high-end-stuff/">FREE HIGH END STUFF</a></li>
<li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-20"><a href="http://www.agneskowalski.com/contact/">CONTACT</a></li>
</ul>                
         </div>
      </section>

<section>

    <div class="container">
	     <div class="top-buffer5"></div>
		 <div class="title_m text-center">
		      <h2>Free High End Stuff</h2>
		 </div>
		 <div class="top-buffer2"></div>
		 
		 
		 <div class="row">
		     <div class="col-sm-6">
    		     <a href="http://www.agneskowalski.com/10-cash-flow-commandments/" target="_blank">
			        <div class="box text-center">
						 <div class="box_title">
							   <h1>10</h1>
							   <div class="top-buffer1"></div>
							   <h2><i>Cash Flow</i></h2>
							   <h3>commandments</h3>
						 </div>
				    </div>
				 </a>
                  <div class="top-buffer3"></div>				  
			 </div>
			 <div class="col-sm-6">
				<a href="http://www.agneskowalski.com/prosperitytraining/" target="_blank">
			        <div class="box box1 text-center">
						  <div class="box_title">
							   <h1>5 Laws</h1>
							   <div class="top-buffer1"></div>
							   <h3>of</h3>
							   <h4>Prosperity <span>Training</span></h4>
						  </div>
				    </div>
			    </a>
                  <div class="top-buffer3"></div>				  
			 </div>
		 </div>
		 
		 <div class="clearfix"></div>
		 
		 <div class="sebscribe_sec embedd">
		      <div class="subsc_video">
				  <div class="top-buffer2"></div>
				  <div class="title text-center">
					  <h2>My Youtube Channel</h2>
				  </div>
				  <div class="top-buffer2"></div>
				  <iframe src='https://www.youtube.com/embed/7jgHTNbWv7I' frameborder='0' width='100%' height='757'></iframe>
                  
			  </div>
		      <div class="clearfix"></div>
   	          <div class="top-buffer5"></div>
			  <div class="resource">
                   <div class="title text-center">
					  <h2>My Little Black Book of Wealth Amplifying Resources</h2>
				   </div>
				   <div class="top-buffer3"></div>
				   <div class="col-sm-6 right-border">
				          <div class="top-buffer2"></div>
				          <a href="https://17hats.com/card/xfbwgvwcrg" target="_blank"><div class="title text-center">
							  <h2>17 Hats</h2>
							  <p>The platform I use for managing <br> my customers accounts</p>
						  </div></a>
						  <div class="top-buffer3"></div>
						  
						  <a href="https://laurenohayon.com/" target="_blank"><div class="title text-center">
							  <h2>Healthy Wealthy Movement</h2>
							  <p>with my BFF and movement guru <br>Lauren Ohayon</p>
							  </div></a>
					     <div class="top-buffer3"></div>
						  <a href="https://www.facebook.com/groups/thesatisfiedmama/" target="_blank"><div class="title text-center">
							  <h2>MOJO</h2>
							  <p>Dana Myers the self-love coach for Mamas</p>
							  </div></a>					   
				   </div>
				   <div class="col-sm-6">
				          <div class="top-buffer2"></div>
				          <a href="https://gumroad.com/a/685945971" target="_blank"><div class="title text-center">
							  <h2>Tapping</h2>
							  <p>The only person I trust with my tapping gold mine <br>Keisha Dixon The Tapping Queen</p>
							  </div></a>
						  <div class="top-buffer3"></div>
						  
						  <a href="https://www.activecampaign.com/?_r=VQ2F42NM" target="_blank"><div class="title text-center">
							  <h2>Active Campaign</h2>
							  <p>My go-to email provider</p>
							  </div></a>
						  <div class="top-buffer3"></div>
						  
						  <a href="https://sarahnegus.com/" target="_blank"><div class="title text-center">
							  <h2>Sarah Negus</h2>
							  <p>Celebrity Shaman</p>
							  </div></a>
				   </div>
			  </div>
		 </div>
	</div>

</section>
<div class="clearfix"></div>
<div class="top-buffer5"></div>
<footer id="page-footer">

	<div class="container">

		<div class="footer">

			<p style="color: #ffffff;">© 2018 ALL RIGHTS RESERVED</p>

		</div>

	</div>

</footer>
<style>.header ul {
    padding-left: 0px;
    width: 130%;
    margin: auto;
}</style>
<script src="http://www.agneskowalski.com/wp-content/themes/manifest/js/jquery.min.js"></script>
<script src="http://www.agneskowalski.com/wp-content/themes/manifest/js/bootstrap.min.js"></script>
</body>
</html>