<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0; user-scalable=0;"/>
      <meta name="apple-mobile-web-app-capable" content="yes"/>
      <meta name="handheldfriendly" content="true"/>
      <meta name="MobileOptimized" content="width"/>
      <link rel="icon" href="<?php  echo get_template_directory_uri(); ?>/images/favicon.ico">
      <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <base href="<?php
         bloginfo('url'); ?>" />
      <meta name="description" content="Mindset Mentor & Wealth Therapist">
      <meta name="author" content="Agnes Kowalski">
      <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />
      <?php
         if (is_single())
         	{
         	global $post;
         	$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
         	$pimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID) , 'single-post-thumbnail'); ?>            
      <meta property="og:url" content="<?php
         echo $url; ?>" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="<?php
         echo $post->post_title; ?>" />
      <meta property="og:description" content="<?php
         echo $post->post_excerpt; ?>" />
      <?php
         if ($pimage[0] != "")
         	{ ?>                
      <meta property="og:image" content="<?php
         echo $pimage[0]; ?>" />
      <?php
         }
          else
         { ?>                
      <meta property="og:image" content="<?php
         echo get_template_directory_uri(); ?>/images/Logo/logo.png" />
      <?php
         }
         } ?>        
      <title><?php
         global $page, $paged;
         wp_title(' | ', true, 'right');
         bloginfo('name');
         $site_description = get_bloginfo('description', 'display');
         
         if ($site_description && (is_home() || is_front_page())) echo " | $site_description";
         
         if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'mytheme') , max($paged, $page)); ?></title>
      <!-- Bootstrap core CSS -->        
      <link type="text/css" href="<?php
         echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
      <link type="text/css" href="<?php
         echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css" rel="stylesheet">
      <link href="<?php
         echo get_template_directory_uri(); ?>/css/font-awesome.css" rel="stylesheet">
      <link href="<?php
         echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
      <link type="text/css" href="<?php
         echo get_template_directory_uri(); ?>/css/style1.css" rel="stylesheet">
      <link type="text/css" href="<?php
         echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
      <link type="text/css" href="<?php
         echo get_template_directory_uri(); ?>/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
      <!-- Add fancyBox -->        
      <link rel="stylesheet" href="<?php
         echo get_template_directory_uri(); ?>/js/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
      <!-- Optionally add helpers - button, thumbnail and/or media -->        
      <link rel="stylesheet" href="<?php
         echo get_template_directory_uri(); ?>/js/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
      <link rel="stylesheet" href="<?php
         echo get_template_directory_uri(); ?>/js/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->        <!--[if lt IE 9]>        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>        <![endif]-->        <?php
         wp_head(); ?>    
   </head>
   <body <?php  body_class(); ?>>
      <div class="main_div <?php echo get_the_ID(); ?>">
      <section>
         <div class="header">
            <div class="navbar-header">
               <p class="s76 responsive-title" style="color:#c8485f;">Agnes Kowalski</p>
               <button type="button" class="navbar-toggle toggleMenu collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">                            <span class="icon-bar"></span>                            <span class="icon-bar"></span>                            <span class="icon-bar"></span>                        </button>                    
            </div>
            <?php
               wp_nav_menu(array(
               	'container' => false,
               	'menu' => 'Header Menu',
               	'menu_class' => 'nav',
               	'fallback_cb' => false
               )); ?>                
         </div>
      </section>

