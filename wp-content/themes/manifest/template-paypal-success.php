<?php
/*
  Template Name: PayPal Success
 */
?>
<?php get_header(); ?>
<?php
$pp_hostname = "www.paypal.com"; 

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-synch';
$tx_token = $_GET['tx'];
$auth_token = "lHF2N13GBuyScnNIyRkdNv7C-doq9mzBuyasY6YPL5tvMC13gDg3XfQxnSW";
$req .= "&tx=$tx_token&at=$auth_token";
 
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://$pp_hostname/cgi-bin/webscr");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
//set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
//if your server does not bundled with default verisign certificates.
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $pp_hostname"));
$res = curl_exec($ch);
curl_close($ch);

if(!$res){
echo 'error';
    //HTTP ERROR
}else{
//echo $res;
     // parse the data
    $lines = explode("\n", trim($res));
    $keyarray = array();
    if (strcmp ($lines[0], "SUCCESS") == 0) {
        for ($i = 1; $i < count($lines); $i++) {
            $temp = explode("=", $lines[$i],2);
            $keyarray[urldecode($temp[0])] = urldecode($temp[1]);
        }

    // process payment
    $firstname = $keyarray['first_name'];
    $lastname = $keyarray['last_name'];
    $cemail=$keyarray['payer_email'];
    $item_name=$keyarray['item_name1'];
  
     
    //echo ("<center><p><h3>Thank you for your purchase!</h3></p></center>");
      echo ("<div style='font-size:45px;color:#67A9A5;text-transform:UPPERCASE;border:1px solid #000;margin: 10%;padding: 2%;font-family: BebasNeuewebfont; letter-spacing: 2px’><center><p><h3>Thank yourself for Expanding your Consciousness with this purchase!</h3></p></center></div>");

  //echo ("<b>Payment Details</b><br>\n");
   // echo ("<li>Name: $firstname $lastname</li>\n");
   // echo ("<li>Email: $cemail</li>\n");
    
    if($item_name == 'Testing'){
    $apikey = '5a15d2b41a73bab026629bf36eea33af-us10';
            $auth = base64_encode( 'user:'.$apikey );

            $data = array(
                'apikey'        => $apikey,
                'email_address' => $cemail,
                'status'        => 'subscribed',
                'merge_fields'  => array(
                    'FNAME' => $firstname,
                    'LNAME' =>$lastname
                )
            );
            $json_data = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://us10.api.mailchimp.com/3.0/lists/e8ec94f758/members/');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                        'Authorization: Basic '.$auth));
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);                                                                                                                  

            $result = curl_exec($ch);

            //var_dump($result);
            die('Subscription Done');
   }
   else{
   // no list found
   }
    }
    else if (strcmp ($lines[0], "FAIL") == 0) {
        // log for manual investigation
    }
}
?>
<?php get_footer(); ?>