<?php
/*

  Template Name: Blog Page

 */

  get_header();
  ?>

  <div class="top-buffer3"></div>
  <div class="top-buffer1"></div>

  <section>
    <div class="container">

        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

            <?php if (have_posts()) : ?>

                <?php
                while (have_posts()) : the_post();
                $pimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                if ($pimage[0] != "") {
                    ?>
                    <img src="<?php echo $pimage[0]; ?>" class="img-responsive"/>
                    <?php } ?>
                    <div class="top-buffer2"></div>
                    <p class="s35"><?php echo $post->post_title; ?></p>
                    <div class="top-buffer2"></div>
                    <div class="b22 metasinglemeta">
                        <span class="s22 a22">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            &nbsp; <?php the_time(get_option('date_format')); ?> I
                        </span>
                    </div>
                    <div class="b24 metasinglemeta">
                        <span class="a22">
                            SHARE ON &nbsp;                             
                            <a class="fb-xfbml-parse-ignore" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>&amp;src=sdkpreparse" onclick="javascript:window.open(this.href,
                                '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" /></a>
                            <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>&amp;tw_p=tweetbutton&amp;url=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
                                '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a>
                            <a href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $pimage[0]; ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,
                                '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/pinterest.png" /></a>
                            <a href="mailto:?&subject=<?php echo urlencode(get_the_title()); ?>&body=<?php echo urlencode(get_permalink($post->ID)); ?>" onclick="javascript:window.open(this.href,
                                '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo get_template_directory_uri(); ?>/images/email.png" /></a>
                            </span>
                        </div>
                        <div class="top-buffer2"></div>                        
                        <?php
                        $excerpt = explode(' ', $post->post_content, 50);
                        if (count($excerpt) >= 50) {
                            array_pop($excerpt);
                            $excerpt = implode(" ", $excerpt) . '...';
                        } else {
                            $excerpt = implode(" ", $excerpt);
                        }
                        $et = preg_replace('`\[[^\]]*\]`', '', $excerpt);
                        echo '<p class="s23 a23">'.$et.'</p>';
                        ?>
                        <div class="top-buffer2"></div>
                        <div class="button5">
                            <a href="<?php echo get_permalink($post->ID) ?>"><input type="button" value="Read More"></a>
                        </div>

                        <div class="top-buffer3"></div>
                        <div class="top-buffer2"></div>


                    <?php endwhile; ?>

                <?php endif; ?>

                <?php
                global $wp_query;

            $big = 999999999; // need an unlikely integer

            $mypagei = paginate_links(array(                
                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'total' => $wp_query->max_num_pages,
                'type' => 'list',
                'prev_text' => __('&laquo;'),
                'next_text' => __('&raquo;'),
                ));
            if ($mypagei != '') {
                ?>
                <nav aria-label="Page navigation" class="text-center">
                    <?php echo $mypagei; ?>
                </nav>
                <?php } ?>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 textsatting">
                <?php if (is_active_sidebar('blog_sidebar')) { ?>
                <?php dynamic_sidebar('blog_sidebar'); ?>
                <?php } ?>
            </div>
        </div>
    </section>



    <?php get_footer(); ?>