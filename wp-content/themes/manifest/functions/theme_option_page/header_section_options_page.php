<?php
add_action('admin_init', 'register_myheader_settings');

function register_myheader_settings() {
    register_setting('my-own-theme-options-for-header', 'my_favicon_icon');
    register_setting('my-own-theme-options-for-header', 'logo_image');
    register_setting('my-own-theme-options-for-header', 'header_reg_menu');
    register_setting('my-own-theme-options-for-header', 'header_acc_menu');
}

function header_section_options_page() {
    ?>

    <style type="text/css">

        .repatertab
        {
            border:1px solid #CCC;
            border-collapse:collapse;
        }
        .repatertab td, .repatertab th
        {
            border:1px solid #CCC;
            padding:5px;
        }
        .repatertab tr
        {
            border:1px solid #CCC;
        }
        .repatertab th
        {
            background:#DDDDDD;
            text-align: center;
        }
        .repatertab tr:nth-child(even) {background: #FFF}
        .repatertab tr:nth-child(odd) {background: #EEE}
        .repatertab .sort
        {
            cursor:move;
        }

    </style>
    <div class="wrap">
        <h2>Header Section</h2>
        <?php settings_errors(); ?> 
        <form method="post" action="options.php">
            <?php settings_fields('my-own-theme-options-for-header'); ?>
            <?php do_settings_sections('my-own-theme-options-for-header'); ?>
            <?php include('bootstrap_theme_includes.php'); ?>
            <br />
            <div class="row">

                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Header Options</h3>
                        </div>

                        <div class="panel-body">



                            <label style="width: 100%;" for="upload_image">Logo Image</label>
                            <?php echo my_image_uploader('logo_image', 'Upload Image'); ?>

                            <br />
                            <br />

                            <label style="width: 100%;" for="upload_image">Favicon Icon</label>                            
                            <?php echo my_image_uploader('my_favicon_icon', 'Upload Image'); ?>

                            <br />
                            <br />


                            <script type="text/javascript">
                                jQuery(document).ready(function ($) {
                                    $("#addnewfield").live("click", function () {
                                        $("#sortable tr").find('input[type=button]').removeAttr('disabled');
                                        var clo = $("#sortable tr:last-child").clone();

                                        clo.find('.sort').text(($(".repatertab tr").size()));
                                        clo.find('input[type=text]').val('').end().insertAfter("#sortable tr:last-child");
                                    });

                                    $(".removethis").live("click", function () {
                                        if ($(".repatertab tr").size() > 2)
                                        {
                                            $(this).parent().parent().remove();
                                        } else
                                        {
                                            $(".removethis").attr('disabled', 'disabled');
                                        }
                                    });

                                    //jQuery( "#sortable" ).sortable({cancel: 'input,button,textarea'});
                                });
                            </script>





                            <table class="repatertab" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Icon</th>
                                        <th>Page</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody id="sortable">
                                    <?php
                                    $outputdata = get_option('header_reg_menu');

                                    if (empty($outputdata)) {

                                        $countrec = 1;
                                    } else {
                                        $countrec = count($outputdata['name']);
                                    }
                                    ?>
                                    <?php for ($i = 0; $i < $countrec; $i++) { ?>
                                        <?php
                                        if (!empty($outputdata)) {
                                            if ($outputdata['name'][$i] != '') {
                                                $menu_name = $outputdata['name'][$i];
                                            }
                                            if ($outputdata['icon'][$i] != '') {
                                                $menu_icon = $outputdata['icon'][$i];
                                            }
                                            if ($outputdata['page'][$i] != '') {
                                                $menu_page = $outputdata['page'][$i];
                                            }
                                        }
                                        ?>
                                        <tr id="myrow<?php echo $i ?>">
                                            <td class="sort" align="center" valign="middle" width="50px">
                                                <?php echo $i + 1; ?>
                                            </td>

                                            <td align="center" valign="middle">
                                                <input type="text" class="form-control" placeholder="page" value="<?php echo $menu_name; ?>" name="header_reg_menu[name][]" />
                                            </td> 

                                            <td align="center" valign="middle">
                                                <input type="text" class="form-control" placeholder="icon" value="<?php echo $menu_icon; ?>" name="header_reg_menu[icon][]" />
                                            </td> 
                                            
                                            <td align="center" valign="middle">
                                                <input type="text" class="form-control" placeholder="page" value="<?php echo $menu_page; ?>" name="header_reg_menu[page][]" />
                                            </td> 

                                            <td align="center" valign="middle">
                                                <input class="button removethis" type="button" value="Remove" />
                                            </td> 
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>	
                            <br />
                            <input class="button addnewbutton" type="button" id="addnewfield" value="add new" />


                        </div>


                    </div>
                </div>

                <?php include 'option_page_sidebar.php'; ?>

            </div>

            <?php submit_button(); ?>

        </form>
    </div>
    <?php
}

/* My Theme Option */
?>
