<?php

/*

  Template Name: Mindset Starter - Thank You

 */

?>

<html lang="en">

	<head>

		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="Mindset Starter Pack Thank You page">

		<meta name="author" content="Agnes Kowalski">

        <meta name="google-site-verification" content="PrEECgmgZijmxFtQnJlaz6cp7u5_tNCYkBXi6qWWozo" />

		<title>Mindset Starter Pack - Thank You</title>

		<!-- Bootstrap core CSS -->

		<link href="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/css/bootstrap.min.css" rel="stylesheet">

		<link href="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/css/bootstrap-theme.min.css" rel="stylesheet">

		<link href="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/css/style.css" rel="stylesheet">

		<script src="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/js/jquery.min.js"></script>

		<script src="http://www.agneskowalski.com/wp-content/themes/prosperitytraining/js/bootstrap.min.js"></script>

<!-- Facebook Pixel Code -->

<script>

  !function(f,b,e,v,n,t,s)

  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?

  n.callMethod.apply(n,arguments):n.queue.push(arguments)};

  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

  n.queue=[];t=b.createElement(e);t.async=!0;

  t.src=v;s=b.getElementsByTagName(e)[0];

  s.parentNode.insertBefore(t,s)}(window, document,'script',

  'https://connect.facebook.net/en_US/fbevents.js');

  fbq('init', '242423366230929');

  fbq('track', 'PageView');

</script>

<noscript><img height="1" width="1" style="display:none"

  src="https://www.facebook.com/tr?id=242423366230929&ev=PageView&noscript=1"

/></noscript>

<!-- End Facebook Pixel Code -->

	</head>

	<body>

		<div class="section">

			<div class="main-bg">

				<div class="container">

					<div class="details">

					    <div class="top-buffer140"></div>

						<p class="s38">Thank you for signing up to receive the</p>

						<!--<p class="s27">of</p>-->

						<h1>Mindset</h1><p class="s38"></p><h1><span>Starter Pack!</span></h1>

						<div class="top-buffer1"></div>

						<p class="s27">Your content is currently en route to your email inbox; 

be sure to check your spam and promotions folders.</p>

						<!--<h2>Agnes Kowalski</h2>-->

                        <p class="s38">See you soon!</p>

						<div class="top-buffer5"></div>

						<div class="top-buffer3"></div>

                        <!--<div class="top-buffer5"></div>-->

						<div class="signup-form col-sm-10 col-sm-offset-1">

						</div>

					</div>

					<div class="bottom-buffer"></div>

				</div>

				

			</div>

			<footer>

				<div class="container">

					<div class="footer">

						<p>© 2017 All rights reserved</p>

					</div>

				</div>

			</footer>

			

		</div>

		<script type='text/javascript' src='http://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>

		<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

<!-- Global Site Tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106731453-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments)};

  gtag('js', new Date());



  gtag('config', 'UA-106731453-1');

</script>

	</body>

</html>